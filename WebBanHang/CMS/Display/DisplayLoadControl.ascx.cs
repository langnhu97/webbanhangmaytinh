﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.CMS.Admin;

namespace WebBanHang.CMS.Display
{
    public partial class DisplayLoadControl : System.Web.UI.UserControl
    {
        string queryString = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["uc"] != null)
            {
                queryString = Request.QueryString["uc"];
            }

            switch (queryString)
            {
                case "prd":
                    plDisplayLoadControl.Controls.Add(LoadControl("SanPham/SanPhamLoadControl.ascx"));
                    break;
                case "mem":
                    plDisplayLoadControl.Controls.Add(LoadControl("ThanhVien/ThanhVienLoadControl.ascx"));
                    break;
                case "new":
                    plDisplayLoadControl.Controls.Add(LoadControl("TinTuc/TinTucLoadControl.ascx"));
                    break;              
                default:
                    plDisplayLoadControl.Controls.Add(LoadControl("TrangChu/TrangChuLoadControl.ascx"));
                    break;
            }
        }
    }
}