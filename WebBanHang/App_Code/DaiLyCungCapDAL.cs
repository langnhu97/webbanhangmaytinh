﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class DaiLyCungCapDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Update một DaiLyCungCap

        public void DaiLyCungCap_Update(int MaDL,
                                    string TenDL,
                                    string DiaChi,
                                    string SDT)
        {
            SqlCommand cmd = new SqlCommand("sp_DaiLyCungCapUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDL", MaDL);
            cmd.Parameters.AddWithValue("@TenDL", TenDL);
            cmd.Parameters.AddWithValue("@DiaChi", DiaChi);
            cmd.Parameters.AddWithValue("@SDT", SDT);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một DaiLyCungCap

        public void DaiLyCungCap_Insert(string TenDL,
                                        string DiaChi,
                                        string SDT)
        {
            SqlCommand cmd = new SqlCommand("sp_DaiLyCungCapInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenDL", TenDL);
            cmd.Parameters.AddWithValue("@DiaChi", DiaChi);
            cmd.Parameters.AddWithValue("@SDT", SDT);
            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 DaiLyCungCap

        public void DaiLyCungCap_Delete(int MaDL)
        {
            SqlCommand cmd = new SqlCommand("sp_DaiLyCungCapDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDL", MaDL);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách DaiLyCungCap
        public DataTable DaiLyCungCap_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM DaiLyCungCap";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một DaiLyCungCap theo MaDL
        public DataTable DaiLyCungCap_SelectById(int MaDL)
        {
            SqlCommand cmd = new SqlCommand("sp_DaiLyCungCapSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDL", MaDL);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}