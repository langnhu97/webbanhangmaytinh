﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class DonDatHangDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Update một DonDatHang

        public void DonDatHang_Update(int MaHD,
                                        DateTime NgayDatHang,
                                        float TongTien,
                                        string TinhTrang,
                                        int MaKH,
                                        string TenKH,
                                        string sdtKH,
                                        string emailKH)
        {
            SqlCommand cmd = new SqlCommand("sp_DonDatHangUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaHD", MaHD);
            cmd.Parameters.AddWithValue("@NgayDatHang", NgayDatHang);
            cmd.Parameters.AddWithValue("@TongTien", TongTien);
            cmd.Parameters.AddWithValue("@TinhTrang", TinhTrang);
            cmd.Parameters.AddWithValue("@MaKH", MaKH);
            cmd.Parameters.AddWithValue("@TenKH", TenKH);
            cmd.Parameters.AddWithValue("@sdtKH", sdtKH);
            cmd.Parameters.AddWithValue("@emailKH", emailKH);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một DonDatHang

        public void DonDatHang_Insert(DateTime NgayDatHang,
                                        float TongTien,
                                        string TinhTrang,
                                        int MaKH,
                                        string TenKH,
                                        string sdtKH,
                                        string emailKH)
        {
            SqlCommand cmd = new SqlCommand("sp_DonDatHangInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@NgayDatHang", NgayDatHang);
            cmd.Parameters.AddWithValue("@TongTien", TongTien);
            cmd.Parameters.AddWithValue("@TinhTrang", TinhTrang);
            cmd.Parameters.AddWithValue("@MaKH", MaKH);
            cmd.Parameters.AddWithValue("@TenKH", TenKH);
            cmd.Parameters.AddWithValue("@sdtKH", sdtKH);
            cmd.Parameters.AddWithValue("@emailKH", emailKH);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 DonDatHang

        public void DonDatHang_Delete(int MaHD)
        {
            SqlCommand cmd = new SqlCommand("sp_DonDatHangDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaHD", MaHD);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách DonDatHang
        public DataTable DonDatHang_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM DonDatHang";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị danh sách DonDatHang theo thứ tự mới đến cũ (giảm dần theo MaHD)
        public DataTable DonDatHang_SelectDesc()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM DonDatHang Order By MaHD desc";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một DonDatHang theo MaHD
        public DataTable DonDatHang_SelectById(int MaHD)
        {
            SqlCommand cmd = new SqlCommand("sp_DonDatHangSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaHD", MaHD);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

        #region Hiển thị một DonDatHang theo MaHD
        public DataTable DonDatHang_SelectByMaThanhToan(string MaThanhToanTrucTuyen)
        {
            SqlCommand cmd = new SqlCommand("sp_DonDatHang_SelectByMaThanhToan");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaThanhToanTrucTuyen", MaThanhToanTrucTuyen);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

    }
}