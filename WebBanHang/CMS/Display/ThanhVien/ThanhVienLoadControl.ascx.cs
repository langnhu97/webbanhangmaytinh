﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Display.ThanhVien
{
    public partial class ThanhVienLoadControl : System.Web.UI.UserControl
    {
        string queryString = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["suc"] != null)
            {
                queryString = Request.QueryString["suc"];
            }

            switch (queryString)
            {
                case "log":
                    plControl.Controls.Add(LoadControl("DangNhap.ascx"));
                    break;
                case "reg":
                    plControl.Controls.Add(LoadControl("DangKy.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("DangNhap.ascx"));
                    break;
            }
        }
    }
}