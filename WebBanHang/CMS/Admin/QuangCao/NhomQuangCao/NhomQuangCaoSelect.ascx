﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NhomQuangCaoSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.QuangCao.NhomQuangCao.NhomQuangCaoSelect" %>
<div class="head">
    Danh sách nhóm quảng cáo
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=adv&suc=grp&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colName">Tên Nhóm Quảng Cáo</th>
            <th class="colAdd">Vị trí</th>
            <th class="colOrd">Thứ tự</th>
            <th class="colTool">Công cụ</th>            
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>
         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc muốn xóa danh mục này?")) {
           
            $.post("CMS/Admin/QuangCao/NhomQuangCao/AJAX/NhomQuangCao.aspx",
                {
                    "man": "del",
                    "id": id
                },
                function (data, status) {
                    if (data == "1") //Nếu thực hiện thành công data -> 1, ngược lại data ->0
                    {
                        $("#rowId_" + id).slideUp();
                    } else {
                        alert("Xóa thất bại");
                    }
                }
            );
        }        
    }
</script>