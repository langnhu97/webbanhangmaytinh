﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.KhachHang
{
    public partial class KhachHangSelect : System.Web.UI.UserControl
    {
        KhachHangDAL dal = new KhachHangDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhSach();
            }
        }

        private void LayDanhSach()
        {
            DataTable dt = new DataTable();
            dt = dal.KhachHang_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaKH"] + @"'>
                     <td class='colId'>" + dt.Rows[i]["MaKH"] + @"</td>   
                     <td class='colName'>" + dt.Rows[i]["TenKH"] + @"</td>       
                     <td class='colAdd'>" + dt.Rows[i]["DiaChi"] + @"</td>       
                     <td class='colPhone'>" + dt.Rows[i]["SDT"] + @"</td>       
                     <td class='colEmail'>" + dt.Rows[i]["Email"] + @"</td>       
                     <td class='colTool'>
                         <a href = 'Admin.aspx?uc=cus&suc=lst&man=upd&id=" + dt.Rows[i]["MaKH"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaKH"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>                
                </tr>
            ";

            }
        }
    }
}