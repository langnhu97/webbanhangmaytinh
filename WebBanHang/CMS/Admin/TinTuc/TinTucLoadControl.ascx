﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TinTucLoadControl.ascx.cs" Inherits="WebBanHang.CMS.Admin.TinTuc.TinTucLoadControl" %>

<div class="container">
     <div class="colLeft">  
        <div class="head">
            Quản lý
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("new","grp","") %>" href="Admin.aspx?uc=new&suc=grp">Danh mục tin tức</a></li>
            <li><a class="<%=DanhDauThaoTac("new","lst","") %>" href="Admin.aspx?uc=new&suc=lst">Danh sách tin tức</a></li>
        </ul>
        <div class="head">
            Thêm mới
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("new","grp","add") %>" href="Admin.aspx?uc=new&suc=grp&man=add" title="Thêm mới nhóm quảng cáo">Danh mục tin tức</a></li>
            <li><a class="<%=DanhDauThaoTac("new","lst","add") %>" href="Admin.aspx?uc=new&suc=lst&man=add" title="Thêm mới quảng cáo">Danh sách tin tức</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="colRight">
        <asp:PlaceHolder ID="plTinTucControl" runat="server"></asp:PlaceHolder>
    </div>
</div>