﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class QuangCaoDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Thuc hien them mot QuangCao

        public void QuangCao_Insert(int MaNhomQC,
                                    string TenQC,
                                    string AnhDaiDien,
                                    string LienKet,
                                    int ThuTu)
        {
            SqlCommand cmd = new SqlCommand("sp_QuangCaoInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNhomQC", MaNhomQC);
            cmd.Parameters.AddWithValue("@TenQC", TenQC);
            cmd.Parameters.AddWithValue("@AnhDaiDien", AnhDaiDien);
            cmd.Parameters.AddWithValue("@LienKet", LienKet);
            cmd.Parameters.AddWithValue("@ThuTu", ThuTu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thuc hien chinh sua mot QuangCao

        public void QuangCao_Update(int MaQC,
                                    int MaNhomQC,
                                    string TenQC,
                                    string AnhDaiDien,
                                    string LienKet,
                                    int ThuTu)
        {
            SqlCommand cmd = new SqlCommand("sp_QuangCaoUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaQC", MaQC);
            cmd.Parameters.AddWithValue("@MaNhomQC", MaNhomQC);
            cmd.Parameters.AddWithValue("@TenQC", TenQC);
            cmd.Parameters.AddWithValue("@AnhDaiDien", AnhDaiDien);
            cmd.Parameters.AddWithValue("@LienKet", LienKet);
            cmd.Parameters.AddWithValue("@ThuTu", ThuTu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thuc hien xoa 1 QuangCao

        public void QuangCao_Delete(int MaQC)
        {
            SqlCommand cmd = new SqlCommand("sp_QuangCaoDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaQC", MaQC);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách QuangCao
        public DataTable QuangCao_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM QuangCao";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một QuangCao theo MaQC
        public DataTable QuangCao_SelectById(int MaQC)
        {
            SqlCommand cmd = new SqlCommand("sp_QuangCaoSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaQC", MaQC);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

        #region Hiển thị QuangCao theo MaNhomQC
        public DataTable QuangCao_SelectByNhomQC(int MaNhomQC)
        {
            SqlCommand cmd = new SqlCommand("sp_QuangCaoSelectByNhomQC");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNhomQC", MaNhomQC);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}