﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.NhaSanXuat
{
    public partial class NhaSanXuatUpdate : System.Web.UI.UserControl
    {
        NhaSanXuatDAL dal = new NhaSanXuatDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
            HienThiThongTin(id);
        }

        private void HienThiThongTin(int id)
        {
            DataTable dt = dal.NhaSanXuat_SelectById(id);
            txtTenNSX.Text = dt.Rows[0]["TenNSX"].ToString();
            txtGhiChu.Text = dt.Rows[0]["GhiChu"].ToString();            
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.NhaSanXuat_Update(id, txtTenNSX.Text.Trim(), txtGhiChu.Text.Trim());
                Response.Redirect("Admin.aspx?uc=prd&suc=man");

            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa sửa được danh mục</div>";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenNSX.Text = "";
            txtGhiChu.Text = "";
        }
    }
}