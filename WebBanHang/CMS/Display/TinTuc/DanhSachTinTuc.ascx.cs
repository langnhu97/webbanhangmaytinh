﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.TinTuc
{
    public partial class DanhSachTinTuc : System.Web.UI.UserControl
    {
        DanhMucTinTucDAL danhMucTinTucDAL = new DanhMucTinTucDAL();
        TinTucDAL tinTucDAL = new TinTucDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }
            if (!IsPostBack)
            {
                ltrDanhMucTinTuc.Text = LayDanhMucTinTuc();
            }
        }

        private string LayDanhMucTinTuc()
        {
            string s = "";
            DataTable dt = danhMucTinTucDAL.DanhMucTinTuc_SelectById(id);

            s += @"
                    <div class='news-list mx-1 my-3 px-3'>                    
                    <div class='news-list-head row mb-3'>
                       <a class='news-list-head' href='Default.aspx?uc=new&suc=lst&id=" + dt.Rows[0]["MaDMTT"] + @"' title='" + dt.Rows[0]["TenDMTT"] + @"'>
                            <i class='fa-solid fa-rss mr-2'></i>" + dt.Rows[0]["TenDMTT"] + @"
                       </a>
                   </div>                                 
                ";

            s += LayDanhSachTin(Convert.ToInt32(dt.Rows[0]["MaDMTT"].ToString()));
            s += "</div>";

            return s;
        }

        private string LayDanhSachTin(int MaDM)
        {
            string s = "";
            DataTable dt = tinTucDAL.TinTuc_SelectByDanhMuc(MaDM);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                s += @"
            <div class='row news-list-first mb-4'>
                <div class='news-list-img col-md-4 px-i' >
                    <a href='Default.aspx?uc=new&suc=dtl&id=" + dt.Rows[i]["MaTT"] + @"' title ='" + dt.Rows[i]["TieuDe"] + @"' class='w-1ii h-1ii d-inline-block mb-3'>
                        <img src = 'IMG/TinTuc/" + dt.Rows[i]["AnhDaiDien"] + @"' class='img-fluid w-1ii h-1ii'/>
                    </a>
                </div>               
            
             <div class='col-md-8 pl-5'>
                    <div class='news-list-name'>
                        <a href='Default.aspx?uc=new&suc=dtl&id=" + dt.Rows[i]["MaTT"] + @"' title='" + dt.Rows[i]["TieuDe"] + @"'>
                            " + dt.Rows[i]["TieuDe"] + @"
                        </a >
                    </div>
                    <div class='news-list-view'>                       
                        <a href='Default.aspx?uc=new&suc=dtl&id=" + dt.Rows[i]["MaTT"] + @"' title='" + dt.Rows[i]["TieuDe"] + @"'>
                            <span><i class='fa-solid fa-eye px-2'></i>" + dt.Rows[i]["LuotXem"] + @"</span>
                            <span><i class='fa-regular fa-calendar-days px-2'></i>(" + Convert.ToDateTime(dt.Rows[i]["NgayDang"]).ToString("dd/MM/yyyy") + @")</span>
                        </a>
                    </div>
                    <div>
                        <a class='overflow-hidden' style='max-height:40px' href='Default.aspx?uc=new&suc=dtl&id=" + dt.Rows[i]["MaTT"] + @"' title='" + dt.Rows[i]["TieuDe"] + @"'>
                            " + dt.Rows[i]["MoTa"] + @"
                        </a>
                    </div>
                </div>
            </div>
                ";
            }
            return s;
        }
    }
}