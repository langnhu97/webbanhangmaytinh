﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.ThanhVien
{
    public partial class DangKy : System.Web.UI.UserControl
    {
        KhachHangDAL khachHangDAL = new KhachHangDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnDangKy_Click(object sender, EventArgs e)
        {
            //Kiểm tra email đăng ký đã tồn tại hay chưa
            if (DaTonTaiEmail(txtEmail.Text.Trim()))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('Email đăng ký đã tồn tại');", true);
            }
            else
            {
                //Thực hiện đăng ký tài khoản khách hàng
                string matkhau = MaHoa.GetMD5(txtMatKhau.Text);
                khachHangDAL.KhachHang_Insert(txtTenKH.Text, txtDiaChi.Text, txtSDT.Text, txtEmail.Text, matkhau, "");

                ScriptManager.RegisterStartupScript(this, this.GetType(), "","alert('Đăng ký tài khoản khách hàng thành công. " +
                    "Bạn có thể đăng nhập với email và mật khẩu vừa tạo');location.href='/Default.aspx?uc=mem&suc=log'", true);

            }
        }

        private bool DaTonTaiEmail(string email)
        {
            DataTable dt = khachHangDAL.KhachHang_SelectByEmail(email);
            if(dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}