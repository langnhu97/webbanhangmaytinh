﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TinTuc.DanhMucTinTuc
{
    public partial class DanhMucTinTucSelect : System.Web.UI.UserControl
    {
        DanhMucTinTucDAL dal = new DanhMucTinTucDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = dal.DanhMucTinTuc_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaDMTT"] + @"'>
                    <td class='colId'>" + dt.Rows[i]["MaDMTT"] + @"</td>
                    <td class='colName'>" + dt.Rows[i]["TenDMTT"] + @"</td>
                    <td class='colOrd'>" + dt.Rows[i]["ThuTu"] + @"</td>
                    <td class='colTool'>
                        <a href = 'Admin.aspx?uc=new&suc=grp&man=upd&id=" + dt.Rows[i]["MaDMTT"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaDMTT"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>                         
                </tr>
                ";
            }
        }
    }
}