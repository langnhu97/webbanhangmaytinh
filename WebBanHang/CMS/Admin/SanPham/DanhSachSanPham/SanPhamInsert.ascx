﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SanPhamInsert.ascx.cs" Inherits="WebBanHang.CMS.Admin.SanPham.DanhSachSanPham.SanPhamInsert" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET"  TagPrefix="CKEditor" %>

<div class="head">
    Thêm mới sản phẩm
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">        
        <div class="attr">Tên sản phẩm</div>
        <div class="val">
            <asp:TextBox ID="txtTenSP" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="***" SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenSP"></asp:RequiredFieldValidator>
        </div>        
    </div>
    <div class="field">
        <div class="attr">Danh mục</div>
        <div class="val"><asp:DropDownList CSSclass="w250" ID="ddlDM" runat="server"></asp:DropDownList></div>
    </div>
    <div class="field">
        <div class="attr">Loại sản phẩm</div>
        <div class="val"><asp:DropDownList CssClass="w250" ID="ddlLoaiSP" runat="server"></asp:DropDownList></div>
    </div>
    <div class="field">
        <div class="attr">Nhà sản xuất</div>
        <div class="val"><asp:DropDownList CssClass="w250" ID="ddlNSX" runat="server"></asp:DropDownList></div>
    </div>    
     <div class="field">
        <div class="attr">Khuyến mãi</div>
        <div class="val"><asp:DropDownList CSSclass="w250" ID="ddlKhuyenMai" runat="server"></asp:DropDownList></div>
    </div>
    <div class="field">
        <div class="attr">Bảo hành</div>
        <div class="val"><asp:TextBox ID="txtBaoHanh" runat="server"></asp:TextBox></div>
    </div>
    <div class="field">
        <div class="attr">Số lượng</div>
        <div class="val">
            <asp:TextBox ID="txtSoLuong" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSoLuong" 
                SetFocusOnError="true" Display="Dynamic" ForeColor="red" ValidationExpression="(\d)*" ErrorMessage="Phải nhập số"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="field">
        <div class="attr">Thông số</div>
        <div class="val">
            <asp:TextBox ID="txtThongSo" TextMode="MultiLine" CssClass="w250" runat="server"></asp:TextBox>            
        </div>
    </div>
    <div class="field">
        <div class="attr">Giá bán</div>
        <div class="val">
            <asp:TextBox ID="txtGiaBan" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtGiaBan" 
                SetFocusOnError="true" Display="Dynamic" ForeColor="red" ValidationExpression="^[\d\.,]+$" ErrorMessage="Phải nhập số"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="field">
        <div class="attr">Hình ảnh</div>
        <div class="val"><asp:FileUpload ID="flHinhAnh" runat="server" /></div>
    </div>
    <div class="field">
        <div class="attr">Ghi chú</div>
        <div class="val">
            <%--<asp:TextBox ID="txtGhiChu" runat="server"></asp:TextBox>--%>
            <CKEditor:CKEditorControl  ID="txtGhiChu" runat="server" Text='<%# Bind("content") %>' ></CKEditor:CKEditorControl>
        </div>
    </div>
   
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:CheckBox ID="ckTiepTuc" Text="Tiếp tục thêm mới sau khi tạo sản phẩm này" runat="server" />
        </div>
    </div>
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn btnThemMoi" ID="btnThemMoi" runat="server" Text="Thêm mới" OnClick="btnThemMoi_Click" />
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click"/>
        </div>
    </div>
</div>