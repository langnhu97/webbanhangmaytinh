﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang
{
    public partial class Login : System.Web.UI.Page
    {
        TaiKhoanDAL dal = new TaiKhoanDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void btnDangNhap_Click(object sender, EventArgs e)
        {
            DataTable dt = dal.TaiKhoan_SelectLogin(txtTenDangNhap.Text.Trim(), MaHoa.GetMD5(txtMatKhau.Text.Trim()));
            if(dt.Rows.Count > 0)
            {
                Session["DangNhap"] = "1";
                Session["TenDangNhap"] = dt.Rows[0]["TenDangNhap"].ToString();
                Response.Redirect("/Admin.aspx");
            }
            else
            {
                lblThongBao.Text = "Thông tin đăng nhập chưa chính xác!";
            }
        }
    }
}