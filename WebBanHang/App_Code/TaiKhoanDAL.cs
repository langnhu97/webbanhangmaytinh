﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class TaiKhoanDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Thuc hien chinh sua mot TaiKhoan

        public void TaiKhoan_Update(string TenDangNhap,
                                    string MatKhau,
                                    string EmailDK,
                                    string DiaChiDK,
                                    string HoTen,
                                    DateTime NgaySinh,
                                    int MaQuyen)
        {
            SqlCommand cmd = new SqlCommand("sp_TaiKhoanUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenDangNhap", TenDangNhap);
            cmd.Parameters.AddWithValue("@MatKhau", MatKhau);
            cmd.Parameters.AddWithValue("@EmailDK", EmailDK);
            cmd.Parameters.AddWithValue("@DiaChiDK", DiaChiDK);
            cmd.Parameters.AddWithValue("@HoTen", HoTen);
            cmd.Parameters.AddWithValue("@NgaySinh", NgaySinh);
            cmd.Parameters.AddWithValue("@MaQuyen", MaQuyen);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion


        #region Thuc hien them mot TaiKhoan

        public void TaiKhoan_Insert(string TenDangNhap,
                                    string MatKhau,
                                    string EmailDK,
                                    string DiaChiDK,
                                    string HoTen,
                                    DateTime NgaySinh,
                                    int MaQuyen)
        {
            SqlCommand cmd = new SqlCommand("sp_TaiKhoanInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenDangNhap", TenDangNhap);
            cmd.Parameters.AddWithValue("@MatKhau", MatKhau);
            cmd.Parameters.AddWithValue("@EmailDK", EmailDK);
            cmd.Parameters.AddWithValue("@DiaChiDK", DiaChiDK);
            cmd.Parameters.AddWithValue("@HoTen", HoTen);
            cmd.Parameters.AddWithValue("@NgaySinh", NgaySinh);
            cmd.Parameters.AddWithValue("@MaQuyen", MaQuyen);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion


        #region Thuc hien xoa 1 TaiKhoan

        public void TaiKhoan_Delete(string TenDangNhap)
        {
            SqlCommand cmd = new SqlCommand("sp_TaiKhoanDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenDangNhap", TenDangNhap);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách TaiKhoan
        public DataTable TaiKhoan_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM TaiKhoan";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một TaiKhoan theo TenDangNhap
        public DataTable TaiKhoan_SelectById(string TenDangNhap)
        {
            string sql = $"SELECT * FROM TaiKhoan WHERE TenDangNhap LIKE N'%{TenDangNhap}%'";
            return _sqldb.GetDataTable(sql);
        }
        #endregion

        #region Hiển thị một TaiKhoan theo TenDangNhap va MatKhau
        public DataTable TaiKhoan_SelectLogin(string TenDangNhap, string MatKhau)
        {
            SqlCommand cmd = new SqlCommand("sp_TaiKhoanSelectLogin");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenDangNhap", TenDangNhap);
            cmd.Parameters.AddWithValue("@MatKhau", MatKhau);

            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}