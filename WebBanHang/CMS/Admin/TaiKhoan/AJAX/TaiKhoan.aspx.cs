﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TaiKhoan.AJAX
{
    public partial class TaiKhoan : System.Web.UI.Page
    {
        TaiKhoanDAL dal = new TaiKhoanDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["DangNhap"].ToString() != "1")
            {
                Response.Redirect("Login.aspx");
            }
            string man = ""; //Đánh dấu thao tác (manipulate) được gửi thông qua tham số "man" của XMLHttpRequest
            if (Request.Params["man"] != null)
            {
                man = Request.Params["man"];
            }

            switch (man)
            {
                case "del":
                    XoaDanhMuc();
                    break;

                default:
                    break;
            }
        }

        private void XoaDanhMuc()
        {
            string id = "";
            if (Request.Params["id"] != null)
            {
                id = Request.Params["id"];
            }

            //Thực hiện code xóa, trả về "1"- thành công, "0"-thất bại
            try
            {
                dal.TaiKhoan_Delete(id);
                Response.Write("1");
            }
            catch (Exception)
            {
                Response.Write("0");
            }
        }
    }
}