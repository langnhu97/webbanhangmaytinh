﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class KhachHangDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Thuc hien them mot khach hang
        
        public void KhachHang_Insert(string TenKH,
                                        string DiaChi,
                                        string SDT,
                                        string Email,
                                        string MatKhau,
                                        string ret)
        {
            SqlCommand cmd = new SqlCommand("sp_KhachHangInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenKH", TenKH);
            cmd.Parameters.AddWithValue("@DiaChi", DiaChi);
            cmd.Parameters.AddWithValue("@SDT", SDT);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@MatKhau", MatKhau);
            cmd.Parameters.AddWithValue("@ret", ret);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thuc hien chinh sua mot khach hang
        
        public void KhachHang_Update(int MaKH,
                                    string TenKH,
                                        string DiaChi,
                                        string SDT,
                                        string Email,
                                        string MatKhau)
        {
            SqlCommand cmd = new SqlCommand("sp_KhachHangUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaKH", MaKH);
            cmd.Parameters.AddWithValue("@TenKH", TenKH);
            cmd.Parameters.AddWithValue("@DiaChi", DiaChi);
            cmd.Parameters.AddWithValue("@SDT", SDT);
            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@MatKhau", MatKhau);


            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thuc hien xoa 1 khach hang
        
        public void KhachHang_Delete(int MaKH)
        {
            SqlCommand cmd = new SqlCommand("sp_KhachHangDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaKH", MaKH);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị một KhachHang theo MaKH
        public DataTable KhachHang_SelectById(int MaKH)
        {
            SqlCommand cmd = new SqlCommand("sp_KhachHangSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaKH", MaKH);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách KhachHang
        public DataTable KhachHang_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM KhachHang";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một KhachHang theo Email
        public DataTable KhachHang_SelectByEmail(string Email)
        {
            SqlCommand cmd = new SqlCommand("sp_KhachHangSelectByEmail");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Email", Email);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

        #region Hiển thị một KhachHang theo Email và MatKhau
        public DataTable KhachHang_SelectByEmail_MatKhau(string Email, string MatKhau)
        {
            SqlCommand cmd = new SqlCommand("sp_KhachHangSelectByEmail_MatKhau");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@Email", Email);
            cmd.Parameters.AddWithValue("@MatKhau", MatKhau);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}