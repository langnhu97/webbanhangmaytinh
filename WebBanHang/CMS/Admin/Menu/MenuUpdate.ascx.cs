﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.Menu
{
    public partial class MenuUpdate : System.Web.UI.UserControl
    {
        MenuDAL dal = new MenuDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }

            HienThiThongTin(id);
        }

        private void HienThiThongTin(int id)
        {
            DataTable dt = dal.Menu_SelectById(id);
            if(dt.Rows.Count > 0)
            {
                txtTenMenu.Text = dt.Rows[0]["TenMenu"].ToString();
                txtLienKet.Text = dt.Rows[0]["LienKet"].ToString();
                txtThuTu.Text = dt.Rows[0]["ThuTu"].ToString();
            }
        }

        protected void btnChinhSua_Click(object sender, EventArgs e)
        {
            try
            {
                dal.Menu_Update(id, txtTenMenu.Text.Trim(), txtLienKet.Text.Trim(), Convert.ToInt32(txtThuTu.Text.Trim()));
                Response.Redirect("Admin.aspx?uc=men&suc=lst");
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa sửa được danh mục</div>";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenMenu.Text = "";
            txtLienKet.Text = "";
            txtThuTu.Text = "";
        }
    }
}