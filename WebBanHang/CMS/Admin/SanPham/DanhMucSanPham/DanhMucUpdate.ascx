﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DanhMucUpdate.ascx.cs" Inherits="WebBanHang.CMS.Admin.SanPham.DanhMucSanPham.DanhMucUpdate" %>
<div class="head">
    Chỉnh sửa danh mục
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">        
        <div class="attr">Tên danh mục</div>
        <div class="val">
            <asp:TextBox ID="txtTenDM" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="***" 
                SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenDM"></asp:RequiredFieldValidator>
        </div>        
    </div> 
    <div class="field">        
        <div class="attr">Số sản phẩm hiển thị</div>
        <div class="val">
            <asp:TextBox ID="txtSoSPHienThi" class="txt" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSoSPHienThi" 
                SetFocusOnError="true" Display="Dynamic" ForeColor="red" ValidationExpression="(\d)*" ErrorMessage="Phải nhập số"></asp:RegularExpressionValidator>
        </div>        
    </div>
    <div class="field">
        <div class="attr">Ghi chú</div>
        <div class="val">
            <asp:TextBox ID="txtGhiChu" class="txt" runat="server"></asp:TextBox>            
        </div>        
    </div>        
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn" ID="btnThemMoi" runat="server" Text="Chỉnh sửa" OnClick="btnThemMoi_Click" style="width: 89px"/>
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click"/>
        </div>
    </div>
</div>