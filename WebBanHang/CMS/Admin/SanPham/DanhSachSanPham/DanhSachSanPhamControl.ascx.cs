﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin.SanPham.DanhSachSanPham
{
    public partial class DanhSachSanPhamControl : System.Web.UI.UserControl
    {
        string queryString = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["man"] != null)
            {
                queryString = Request.QueryString["man"];
            }

            switch (queryString)
            {
                case "add":
                    plControl.Controls.Add(LoadControl("SanPhamInsert.ascx"));
                    break;
                case "upd":
                    plControl.Controls.Add(LoadControl("SanPhamUpdate.ascx"));
                    break;
                case "del":
                    plControl.Controls.Add(LoadControl("SanPhamDelete.ascx"));
                    break;
                case "dis":
                    plControl.Controls.Add(LoadControl("SanPhamSelect.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("SanPhamSelect.ascx"));
                    break;
            }
        }
    }
}