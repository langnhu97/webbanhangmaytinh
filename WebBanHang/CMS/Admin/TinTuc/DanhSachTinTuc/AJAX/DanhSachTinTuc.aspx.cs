﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TinTuc.DanhSachTinTuc.AJAX
{
    public partial class DanhSachTinTuc : System.Web.UI.Page
    {
        TinTucDAL dal = new TinTucDAL();
        public string man { get; set; }
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["DangNhap"].ToString() != null && Session["DangNhap"].ToString() == "1")
            {
                
            }
            else
            {
                Response.Redirect("Login.aspx");
            }

            if (Request.Params["man"] != null)
            {
                man = Request.Params["man"].ToString();
            }

            switch (man)
            {
                case "del":
                    XoaTinTuc();
                    break;
                default:
                    break;
            }
        }

        private void XoaTinTuc()
        {
            if (Request.Params["id"] != null)
            {
                id = Convert.ToInt32(Request.Params["id"]);
            }


            //Code xóa -> trả về "1" - thành công,  "0" - thất bại
            try
            {
                dal.TinTuc_Delete(id);
                Response.Write("1");
            }
            catch (Exception)
            {
                Response.Write("0");
            }
        }
    }
}