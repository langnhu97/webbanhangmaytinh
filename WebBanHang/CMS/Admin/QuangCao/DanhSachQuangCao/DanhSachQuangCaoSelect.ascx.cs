﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.QuangCao.DanhSachQuangCao
{
    public partial class DanhSachQuangCaoSelect : System.Web.UI.UserControl
    {
        NhomQuangCaoDAL nhomQuangCaoDAL = new NhomQuangCaoDAL();
        QuangCaoDAL quangCaoDAL = new QuangCaoDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = quangCaoDAL.QuangCao_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string tenNhomQC = nhomQuangCaoDAL.NhomQuangCao_SelectById(Convert.ToInt32(dt.Rows[i]["MaNhomQC"])).Rows[0]["TenNhomQC"].ToString();
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaQC"] + @"'>
                    <td class='colId'>" + dt.Rows[i]["MaQC"] + @"</td>
                    <td class='colGroup'>" + tenNhomQC + @"</td>   
                    <td class='colName'>" + dt.Rows[i]["TenQC"] + @"</td>                   
                    <td class='colImg'>
                        <img class='img-display' src='/IMG/QuangCao/" + dt.Rows[i]["AnhDaiDien"] + @"'/>
                        <img class='img-hover' src = '/IMG/QuangCao/" + dt.Rows[i]["AnhDaiDien"] + @"'/>
                    </td>            
                    <td class='colLink'>" + dt.Rows[i]["LienKet"] + @"</td>  
                    <td class='colOrd'>" + dt.Rows[i]["ThuTu"] + @"</td>            
                    <td class='colTool'>
                        <a href='Admin.aspx?uc=adv&suc=lst&man=upd&id=" + dt.Rows[i]["MaQC"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'/>
                        <a href='javascript:XoaDanhMuc(" + dt.Rows[i]["MaQC"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'/>
                </tr>
                ";
            }
        }
    }
}