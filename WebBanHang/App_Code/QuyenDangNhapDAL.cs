﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class QuyenDangNhapDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Update một QuyenDangNhap

        public void QuyenDangNhap_Update(int MaQuyen,
                                string TenQuyen)
        {
            SqlCommand cmd = new SqlCommand("sp_QuyenDangNhapUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaQuyen", MaQuyen);
            cmd.Parameters.AddWithValue("@TenQuyen", TenQuyen);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một QuyenDangNhap

        public void QuyenDangNhap_Insert(string TenQuyen)
        {
            SqlCommand cmd = new SqlCommand("sp_QuyenDangNhapInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenDM", TenQuyen);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 QuyenDangNhap

        public void QuyenDangNhap_Delete(int MaQuyen)
        {
            SqlCommand cmd = new SqlCommand("sp_QuyenDangNhapDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDM", MaQuyen);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách QuyenDangNhap
        public DataTable QuyenDangNhap_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM QuyenDangNhap";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một QuyenDangNhap theo MaDM
        public DataTable QuyenDangNhap_SelectById(int MaQuyen)
        {
            SqlCommand cmd = new SqlCommand("sp_QuyenDangNhapSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaQuyen", MaQuyen);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}