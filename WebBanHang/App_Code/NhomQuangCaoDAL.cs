﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Data.SqlTypes;
using Microsoft.SqlServer.Server;

namespace WebBanHang.App_Code
{
    public class NhomQuangCaoDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();
        #region Update một NhomQuangCao

        public void NhomQuangCao_Update(int MaNhomQC,
                                        string TenNhomQC,
                                        string ViTri,
                                        int ThuTu)
        {
            SqlCommand cmd = new SqlCommand("sp_NhomQuangCaoUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNhomQC", MaNhomQC);
            cmd.Parameters.AddWithValue("@TenNhomQC", TenNhomQC);
            cmd.Parameters.AddWithValue("@ViTri", ViTri);
            cmd.Parameters.AddWithValue("@ThuTu", ThuTu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một NhomQuangCao

        public void NhomQuangCao_Insert(string TenNhomQC,
                                        string ViTri,
                                        int ThuTu)
        {
            SqlCommand cmd = new SqlCommand("sp_NhomQuangCaoInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenNhomQC", TenNhomQC);
            cmd.Parameters.AddWithValue("@ViTri", ViTri);
            cmd.Parameters.AddWithValue("@ThuTu", ThuTu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 NhomQuangCao

        public void NhomQuangCao_Delete(int MaNhomQC)
        {
            SqlCommand cmd = new SqlCommand("sp_NhomQuangCaoDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNhomQC", MaNhomQC);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách NhomQuangCao
        public DataTable NhomQuangCao_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM NhomQuangCao";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một NhomQuangCao theo MaNSX
        public DataTable NhomQuangCao_SelectById(int MaNhomQC)
        {
            SqlCommand cmd = new SqlCommand("sp_NhomQuangCaoSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNhomQC", MaNhomQC);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
        #region Hiển thị một NhomQuangCao theo ViTri
        public DataTable NhomQuangCao_SelectByViTri(string ViTri)
        {
            SqlCommand cmd = new SqlCommand("sp_NhomQuangCaoSelectByViTri");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@ViTri", ViTri);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}