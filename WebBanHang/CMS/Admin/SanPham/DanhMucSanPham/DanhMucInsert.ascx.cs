﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.DanhMucSanPham
{
    public partial class DanhMucInsert : System.Web.UI.UserControl
    {
        DanhMucDAL dal = new DanhMucDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.DanhMuc_Insert(txtTenDM.Text.Trim(), Convert.ToInt32(txtSoSPHienThi.Text), txtGhiChu.Text.Trim());
                lblThongBao.Text = "<div class='successNoti'>Đã tạo danh mục " + txtTenDM.Text + "</div>";
                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=prd&suc=cat");
                }
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa thêm được danh mục</div>";
            }

        }

        private void ResetForm()
        {
            txtTenDM.Text = String.Empty;
            txtGhiChu.Text = String.Empty;
            txtSoSPHienThi.Text = String.Empty;
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}