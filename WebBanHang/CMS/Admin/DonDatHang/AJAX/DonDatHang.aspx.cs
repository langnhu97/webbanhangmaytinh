﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.DonDatHang.AJAX
{
    public partial class DonDatHang : System.Web.UI.Page
    {
        DonDatHangDAL dondathangDAL = new DonDatHangDAL();
        ChiTietDonHangDAL chitietdonhangDAL = new ChiTietDonHangDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            //Code kiểm tra đăng nhập
            if (Session["DangNhap"] != null && Session["DangNhap"].ToString() == "1")
            {

            }
            else
            {
                Response.Redirect("Login.aspx");
            }

            string man = ""; //Đánh dấu thao tác (manipulate) được gửi thông qua tham số "man" của XMLHttpRequest
            if (Request.Params["man"] != null)
            {
                man = Request.Params["man"];
            }

            switch (man)
            {
                case "del":
                    XoaDanhMuc();
                    break;

                default:
                    break;
            }
        }

        private void XoaDanhMuc()
        {
            string id = "";
            if (Request.Params["id"] != null)
            {
                id = Request.Params["id"];
            }

            //Thực hiện code xóa, trả về "1"- thành công, "0"-thất bại
            try
            {
                chitietdonhangDAL.ChiTietDonHang_DeleteByMaHD(Convert.ToInt32(id));
                dondathangDAL.DonDatHang_Delete(Convert.ToInt32(id));
                Response.Write("1");
            }
            catch (Exception)
            {
                Response.Write("0");
            }
        }
    }
}