﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DonDatHangLoadControl.ascx.cs" Inherits="WebBanHang.CMS.Admin.DonDatHang.DonDatHangLoadControl" %>
<div class="container">
     <div class="colLeft">  
        <div class="head">
            Quản lý
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("ord","lst","") %>" href="Admin.aspx?uc=ord&suc=lst"title="Danh sách khách hàng">Danh sách đơn đặt hàng</a></li>
        </ul>
        <div class="head">
            Thêm mới
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("ord","lst","add") %>" href="Admin.aspx?uc=ord&suc=lst&man=add" title="Thêm mới đơn đặt hàng">Đơn đặt hàng</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="colRight">
        <asp:PlaceHolder ID="plControl" runat="server"></asp:PlaceHolder>
    </div>
</div>
