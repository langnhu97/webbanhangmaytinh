﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin.KhuyenMai
{
    public partial class KhuyenMaiLoadControl : System.Web.UI.UserControl
    {
        string queryString = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["man"] != null)
            {
                queryString = Request.QueryString["man"];
            }

            switch (queryString)
            {
                case "add":
                    plControl.Controls.Add(LoadControl("KhuyenMaiInsert.ascx"));
                    break;
                case "upd":
                    plControl.Controls.Add(LoadControl("KhuyenMaiUpdate.ascx"));
                    break;
                case "del":
                    plControl.Controls.Add(LoadControl("KhuyenMaiDelete.ascx"));
                    break;               
                default:
                    plControl.Controls.Add(LoadControl("KhuyenMaiSelect.ascx"));
                    break;
            }
        }
    }
}