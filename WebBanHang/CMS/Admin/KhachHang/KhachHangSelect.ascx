﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KhachHangSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.KhachHang.KhachHangSelect" %>
<div class="head">
    Danh sách khách hàng đã đăng ký thông tin
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=cus&suc=lst&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colName">Tên Khách Hàng</th>
            <th class="colAdd">Địa chỉ</th>
            <th class="colPhone">SĐT</th>
            <th class="colPhone">Email</th>
            <th class="colTool">Công cụ</th>            
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>
         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc muốn xóa danh mục này?")) {
            $.post("CMS/Admin/KhachHang/AJAX/KhachHang.aspx",
                {
                    "man": "del",
                    "id": id
                },
                function (data, status) {
                    if (data == "1") //data = 1: thực hiện thành công
                    {
                        $("#rowId_" + id).slideUp();
                    } else {
                        alert("Xóa thất bại");
                    }
                }
            );
        }
        
    }
</script>