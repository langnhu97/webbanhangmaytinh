﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.DaiLyCungCap
{
    public partial class DaiLyInsert : System.Web.UI.UserControl
    {
        DaiLyCungCapDAL _dalDaiLyCungCap = new DaiLyCungCapDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                _dalDaiLyCungCap.DaiLyCungCap_Insert(txtTenDL.Text.Trim(), txtDiaChi.Text.Trim(), txtSDT.Text.Trim());
                lblThongBao.Text = "<div class='successNoti'>Đã tạo đại lý cung cấp " + txtTenDL.Text.Trim() + "</div>";

                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=prd&suc=sup");                
                }

            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa tạo được đại lý cung cấp </div>";
                throw;
            }
        }

        private void ResetForm()
        {
            txtTenDL.Text = String.Empty;
            txtDiaChi.Text = String.Empty;
            txtSDT.Text = String.Empty;
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}