﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin.QuangCao.DanhSachQuangCao
{
    public partial class DanhSachQuangCaoLoadControl : System.Web.UI.UserControl
    {
        string man = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["man"] != null)
            {
                man = Request.QueryString["man"];
            }

            switch (man)
            {
                case "add":
                    plControl.Controls.Add(LoadControl("DanhSachQuangCaoInsert.ascx"));
                    break;
                case "upd":
                    plControl.Controls.Add(LoadControl("DanhSachQuangCaoUpdate.ascx"));
                    break;
                case "del":
                    plControl.Controls.Add(LoadControl("DanhSachQuangCaoDelete.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("DanhSachQuangCaoSelect.ascx"));
                    break;
            }
        }
    }
}