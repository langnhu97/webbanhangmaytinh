﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.KhachHang
{
    public partial class KhachHangInsert : System.Web.UI.UserControl
    {
        KhachHangDAL dal = new KhachHangDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.KhachHang_Insert(txtTenKH.Text.Trim(), txtDiaChi.Text.Trim(), txtSDT.Text.Trim(), 
                    txtEmail.Text.Trim(), MaHoa.GetMD5(txtMatKhau.Text.Trim()),"");
                lblThongBao.Text = "<div class='successNoti'Tạo thành công khách hàng " + txtTenKH.Text.Trim() + "</div>";

                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=cus&suc=lst");
                }
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Thêm mới thất bại</div>";
            }
        }

        private void ResetForm()
        {
            txtTenKH.Text = "";
            txtDiaChi.Text = "";
            txtEmail.Text = "";
            txtSDT.Text = "";
            txtMatKhau.Text = "";
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}