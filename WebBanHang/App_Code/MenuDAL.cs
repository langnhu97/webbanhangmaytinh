﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class MenuDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();
        #region Update một Menu

        public void Menu_Update(int MaMenu,
                                string TenMenu,
                                string LienKet,
                                int ThuTu)
        {
            SqlCommand cmd = new SqlCommand("sp_MenuUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaMenu", MaMenu);
            cmd.Parameters.AddWithValue("@TenMenu", TenMenu);
            cmd.Parameters.AddWithValue("@LienKet", LienKet);
            cmd.Parameters.AddWithValue("@ThuTu", ThuTu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một Menu

        public void Menu_Insert(string TenMenu,
                                string LienKet,
                                int ThuTu)
        {
            SqlCommand cmd = new SqlCommand("sp_MenuInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenMenu", TenMenu);
            cmd.Parameters.AddWithValue("@LienKet", LienKet);
            cmd.Parameters.AddWithValue("@ThuTu", ThuTu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 Menu

        public void Menu_Delete(int MaMenu)
        {
            SqlCommand cmd = new SqlCommand("sp_MenuDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaMenu", MaMenu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách Menu
        public DataTable Menu_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM Menu Order by ThuTu";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một Menu theo MaMenu
        public DataTable Menu_SelectById(int MaMenu)
        {
            SqlCommand cmd = new SqlCommand("sp_MenuSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaMenu", MaMenu);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
        
    }
}