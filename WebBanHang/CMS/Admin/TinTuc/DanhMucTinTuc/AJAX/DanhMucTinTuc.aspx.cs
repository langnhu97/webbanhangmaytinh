﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TinTuc.DanhMucTinTuc.AJAX
{
    public partial class DanhMucTinTuc : System.Web.UI.Page
    {
        DanhMucTinTucDAL dal = new DanhMucTinTucDAL();
        public string man { get; set; }
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //Code kiểm tra đăng nhập
            if (Session["DangNhap"].ToString() != "1")
            {
                Response.Redirect("Login.aspx");
            }

            if (Request.Params["man"] != null)
            {
                man = Request.Params["man"].ToString();
            }

            switch (man)
            {
                case "del":
                    XoaDanhMuc();
                    break;
                default:
                    break;
            }
        }

        private void XoaDanhMuc()
        {
            if (Request.Params["id"] != null)
            {
                id = Convert.ToInt32(Request.Params["id"]);
            }

            //Thực hiện code xóa, trả về "1"- thành công, "0" - thất bại
            try
            {
                dal.DanhMucTinTuc_Delete(id);
                Response.Write("1");
            }
            catch (Exception)
            {
                Response.Write("0");
            }
        }
    }
}