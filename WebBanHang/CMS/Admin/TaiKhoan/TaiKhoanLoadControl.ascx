﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaiKhoanLoadControl.ascx.cs" Inherits="WebBanHang.CMS.Admin.TaiKhoan.TaiKhoanLoadControl" %>
<div class="container">
     <div class="colLeft">  
        <div class="head">
            Quản lý
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("acc","lst","") %>" href="Admin.aspx?uc=acc&suc=lst"title="Danh sách tài khoản">Danh sách tài khoản</a></li>
        </ul>
        <div class="head">
            Thêm mới
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("acc","lst","add") %>" href="Admin.aspx?uc=acc&suc=lst&man=add" title="Thêm mới tài khoản">Tài khoản</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="colRight">
        <asp:PlaceHolder ID="plControl" runat="server"></asp:PlaceHolder>
    </div>
</div>
