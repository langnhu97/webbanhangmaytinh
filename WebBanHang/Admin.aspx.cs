﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Kiểm tra trạng thái đăng nhập -> nếu chưa đẩy về trang login
            if (Session["DangNhap"] != null && Session["DangNhap"].ToString() == "1")
            {
                ltrXinChao.Text = "Xin chào " + Session["TenDangNhap"].ToString() + "   |   ";
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected string DanhDau(string ucName)
        {
            string s = "";

            //Lấy giá trị queryString trong modul
            string uc = ""; 
            if (Request.QueryString["uc"] != null)
            {
                uc = Request.QueryString["uc"];
            }

            //So sánh nếu queryString bằng tên uc (modul) truyền vào thì trả về current --> Đánh dấu là menu hiện tại
            if (uc == ucName)
                s = "current";
            return s;
        }

        protected void btnDangXuat_Click(object sender, EventArgs e)
        {
            Session["TenDangNhap"] = null;
            Session["DangNhap"] = null;
            Response.Redirect("/Login.aspx");
        }
    }
}