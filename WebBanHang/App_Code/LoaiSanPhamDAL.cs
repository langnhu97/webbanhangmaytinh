﻿using System.Data.SqlClient;
using System.Data;

namespace WebBanHang.App_Code
{
    public class LoaiSanPhamDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();
        #region Update một LoaiSanPham

        public void LoaiSanPham_Update(int MaLoaiSP,
                                        string TenLoaiSP,
                                        string GhiChu)
        {
            SqlCommand cmd = new SqlCommand("sp_LoaiSanPhamUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaLoaiSP", MaLoaiSP);
            cmd.Parameters.AddWithValue("@TenLoaiSP", TenLoaiSP);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một LoaiSanPham

        public void LoaiSanPham_Insert(string TenLoaiSP,
                                        string GhiChu)
        {
            SqlCommand cmd = new SqlCommand("sp_LoaiSanPhamInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenLoaiSP", TenLoaiSP);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 LoaiSanPham

        public void LoaiSanPham_Delete(int MaLoaiSP)
        {
            SqlCommand cmd = new SqlCommand("sp_LoaiSanPhamDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaLoaiSP", MaLoaiSP);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách LoaiSanPham
        public DataTable LoaiSanPham_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM LoaiSanPham";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một LoaiSanPham theo MaLoaiSP
        public DataTable LoaiSanPham_SelectById(int MaLoaiSP)
        {
            SqlCommand cmd = new SqlCommand("sp_LoaiSanPhamSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaLoaiSP", MaLoaiSP);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}