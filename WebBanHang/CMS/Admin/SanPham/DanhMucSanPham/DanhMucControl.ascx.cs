﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin.SanPham.DanhMucSanPham
{
    public partial class DanhMucControl : System.Web.UI.UserControl
    {
        string queryString = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["man"] != null)
            {
                queryString = Request.QueryString["man"];
            }

            switch (queryString)
            {
                case "add":
                    plControl.Controls.Add(LoadControl("DanhMucInsert.ascx"));
                    break;
                case "upd":
                    plControl.Controls.Add(LoadControl("DanhMucUpdate.ascx"));
                    break;
                case "del":
                    plControl.Controls.Add(LoadControl("DanhMucDelete.ascx"));
                    break;
                case "dis":
                    plControl.Controls.Add(LoadControl("DanhMucSelect.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("DanhMucSelect.ascx"));
                    break;
            }
        }
    }
}