﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.LoaiSanPham
{
    public partial class LoaiSanPhamUpdate : System.Web.UI.UserControl
    {
        LoaiSanPhamDAL dal = new LoaiSanPhamDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
            HienThiThongTin();
        }

        private void HienThiThongTin()
        {
            DataTable dt = dal.LoaiSanPham_SelectById(id);

            txtTenLoaiSP.Text = dt.Rows[0]["TenLoaiSP"].ToString();
            txtGhiChu.Text = dt.Rows[0]["GhiChu"].ToString();
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.LoaiSanPham_Update(id, txtTenLoaiSP.Text, txtGhiChu.Text);
                Response.Redirect("Admin.aspx?uc=prd&suc=typ");

            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa sửa được danh mục</div>";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenLoaiSP.Text = "";
            txtGhiChu.Text = "";
        }
    }
}