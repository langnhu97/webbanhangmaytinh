﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DanhMucSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.SanPham.DanhMucSanPham.DanhMucSelect" %>

<div class="head">
    Các danh mục sản phẩm đã tạo
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=prd&suc=cat&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colName">Tên Danh Mục</th>
            <th class="colQua">Số sản phẩm hiển thị</th>
            <th class="colNote">Ghi Chú</th>
            <th class="colTool">Công cụ</th>            
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>   
    </table>
</div>

<script>
    function XoaDanhMuc(MaDM) {
        if (confirm("Bạn chắc chắn xóa danh mục này?")) {
            //Code xóa danh mục
            $.post("CMS/Admin/SanPham/DanhMucSanPham/Ajax/DanhMuc.aspx",
                {
                    "man": "del",
                    "MaDM": MaDM
                },
                function (data, status) {
                    if (data == "1") { //Nếu thực hiện xóa thành công <==> response trả data = 1; ngược lại data =2 
                        $("#rowId_" + MaDM).slideUp();
                    } else {
                        alert("Xóa thất bại")
                    }
                });
        }
    }
</script>