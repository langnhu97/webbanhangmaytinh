﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DanhSachQuangCaoUpdate.ascx.cs" Inherits="WebBanHang.CMS.Admin.QuangCao.DanhSachQuangCao.DanhSachQuangCaoUpdate" %>

<div class="head">
    Chỉnh sửa quảng cáo
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
     <div class="field">
        <div class="attr">Tên nhóm quảng cáo</div>
        <div class="val"><asp:DropDownList CssClass="w250" ID="ddlNhomQC" runat="server"></asp:DropDownList></div>
    </div>  
    <div class="field">        
        <div class="attr">Tên quảng cáo</div>
        <div class="val">
            <asp:TextBox ID="txtTenQC" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="***" SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenQC"></asp:RequiredFieldValidator>
        </div>        
    </div>
       
    <div class="field">
        <div class="attr">Hình ảnh</div>
        <div class="val">
            <div>
                <asp:HiddenField ID="hdfHinhAnh" runat="server" />
                <asp:Literal ID="ltrHinhAnh" runat="server"></asp:Literal>
            </div>
            <asp:FileUpload ID="flHinhAnh" runat="server" />
        </div>
       
    </div>
    <div class="field">
        <div class="attr">Liên kết</div>
        <div class="val"><asp:TextBox ID="txtLienKet" runat="server"></asp:TextBox></div>
    </div>
   <div class="field">
        <div class="attr">Thứ tự</div>
        <div class="val">
            <asp:TextBox ID="txtThuTu" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtThuTu" 
                SetFocusOnError="true" Display="Dynamic" ForeColor="red" ValidationExpression="(\d)*" ErrorMessage="Phải nhập số"></asp:RegularExpressionValidator>
        </div>
    </div>     
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn " ID="btnThemMoi" runat="server" Text="Chỉnh sửa" OnClick="btnThemMoi_Click"/>
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click"/>
        </div>
    </div>
</div>