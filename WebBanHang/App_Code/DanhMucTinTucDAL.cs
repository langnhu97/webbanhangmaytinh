﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class DanhMucTinTucDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();
        #region Update một DanhMucTinTuc

        public void DanhMucTinTuc_Update(int MaDMTT,
                                            string TenDMTT,
                                            int ThuTu)
        {
            SqlCommand cmd = new SqlCommand("sp_DanhMucTinTucUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDMTT", MaDMTT);
            cmd.Parameters.AddWithValue("@TenDMTT", TenDMTT);
            cmd.Parameters.AddWithValue("@ThuTu", ThuTu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một DanhMucTinTuc

        public void DanhMucTinTuc_Insert(string TenDMTT,
                                            int ThuTu)
        {
            SqlCommand cmd = new SqlCommand("sp_DanhMucTinTucInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenDMTT", TenDMTT);
            cmd.Parameters.AddWithValue("@ThuTu", ThuTu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 DanhMucTinTuc

        public void DanhMucTinTuc_Delete(int MaDMTT)
        {
            SqlCommand cmd = new SqlCommand("sp_DanhMucTinTucDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDMTT", MaDMTT);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách DanhMucTinTuc
        public DataTable DanhMucTinTuc_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM DanhMucTinTuc";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một DanhMucTinTuc theo MaNSX
        public DataTable DanhMucTinTuc_SelectById(int MaDMTT)
        {
            SqlCommand cmd = new SqlCommand("sp_DanhMucTinTucSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDMTT", MaDMTT);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}