﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SanPhamLoadControl.ascx.cs" Inherits="WebBanHang.CMS.SanPham.SanPhamLoadControl" %>


<div class="container">    
    <div class="colLeft">  
        <div class="head">
            Quản lý
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("prd","cat","") %>" href="Admin.aspx?uc=prd&suc=cat"title="Danh mục Sản Phẩm">Danh mục Sản Phẩm</a></li>
            <li><a class="<%=DanhDauThaoTac("prd","typ","") %>" href="Admin.aspx?uc=prd&suc=typ" title="Loại Sản Phẩm">Loại Sản Phẩm</a></li>
            <li><a class="<%=DanhDauThaoTac("prd","lst","") %>" href="Admin.aspx?uc=prd&suc=lst"title="Danh sách Sản Phẩm">Danh sách Sản Phẩm</a></li>
            <li><a class="<%=DanhDauThaoTac("prd","man","") %>" href="Admin.aspx?uc=prd&suc=man" title="Nhà sản xuất">Nhà sản xuất</a></li>
            <%--<li><a class="<%=DanhDauThaoTac("prd","dis","") %>" href="Admin.aspx?uc=prd&suc=dis" title="Khuyến mãi">Khuyến mãi</a></li>--%>
        </ul>
        <div class="head">
            Thêm mới
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("prd","cat","add") %>" href="Admin.aspx?uc=prd&suc=cat&man=add" title="Thêm danh mục sản Phẩm">Danh mục Sản Phẩm</a></li>
            <li><a class="<%=DanhDauThaoTac("prd","typ","add") %>" href="Admin.aspx?uc=prd&suc=typ&man=add" title="Thêm loại sản phẩm">Loại Sản Phẩm</a></li>
            <li><a class="<%=DanhDauThaoTac("prd","lst","add") %>" href="Admin.aspx?uc=prd&suc=lst&man=add" title="Thêm sản phẩm">Sản Phẩm</a></li>
            <li><a class="<%=DanhDauThaoTac("prd","man","add") %>" href="Admin.aspx?uc=prd&suc=man&man=add" title="Thêm nhà sản xuất">Nhà sản xuất</a></li>
            <%--<li><a class="<%=DanhDauThaoTac("prd","dis","add") %>" href="Admin.aspx?uc=prd&suc=dis&man=add" title="Thêm khuyến mãi">Khuyến mãi</a></li>--%>
        </ul>
    </div>
</div>
<div class="container">
    <div class="colRight">
        <asp:PlaceHolder ID="plSanPhamLoadControl" runat="server"></asp:PlaceHolder>
    </div>
</div>
<div class="cb"></div>