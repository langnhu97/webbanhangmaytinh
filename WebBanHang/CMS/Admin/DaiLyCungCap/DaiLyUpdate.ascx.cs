﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.DaiLyCungCap
{
    public partial class DaiLyUpdate : System.Web.UI.UserControl
    {
        DaiLyCungCapDAL dal = new DaiLyCungCapDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
            HienThiThongTin(id);
        }

        private void HienThiThongTin(int id)
        {
            DataTable dt = dal.DaiLyCungCap_SelectById(id);
            txtTenDL.Text = dt.Rows[0]["TenDL"].ToString();
            txtDiaChi.Text = dt.Rows[0]["DiaChi"].ToString();
            txtSDT.Text = dt.Rows[0]["SDT"].ToString();
        }

        protected void btnChinhSua_Click(object sender, EventArgs e)
        {
            try
            {
                dal.DaiLyCungCap_Update(id, txtTenDL.Text, txtDiaChi.Text, txtSDT.Text);
                Response.Redirect("Admin.aspx?uc=sup&suc=lst");
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa sửa được danh mục</div>";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenDL.Text = "";
            txtDiaChi.Text = "";
            txtSDT.Text = "";
        }
    }
}