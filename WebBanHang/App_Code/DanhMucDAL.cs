﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{

    public class DanhMucDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Update một DanhMuc

        public void DanhMuc_Update(int MaDM,
                                string TenDM,
                                int SoSPHienThi,
                                string GhiChu)
        {
            SqlCommand cmd = new SqlCommand("sp_DanhMucUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDM", MaDM);
            cmd.Parameters.AddWithValue("@TenDM", TenDM);
            cmd.Parameters.AddWithValue("@SoSPHienThi", SoSPHienThi);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một DanhMuc

        public void DanhMuc_Insert(string TenDM,
                                    int SoSPHienThi,
                                    string GhiChu)
        {
            SqlCommand cmd = new SqlCommand("sp_DanhMucInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenDM", TenDM);
            cmd.Parameters.AddWithValue("@SoSPHienThi", SoSPHienThi);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 DanhMuc

        public void DanhMuc_Delete(int MaDM)
        {
            SqlCommand cmd = new SqlCommand("sp_DanhMucDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDM", MaDM);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách DanhMuc
        public DataTable DanhMuc_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM DanhMuc";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một DanhMuc theo MaDM
        public DataTable DanhMuc_SelectById(int MaDM)
        {
            SqlCommand cmd = new SqlCommand("sp_DanhMucSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDM", MaDM);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

    }
}