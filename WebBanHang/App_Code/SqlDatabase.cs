﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Windows.Input;

namespace WebBanHang.App_Code
{
    public class SqlDatabase
    {
        private string _connectionString { get; set; }
        private SqlConnection _connection { get; set; }
        private DataSet _dataSet { get; set; }
        private SqlCommand _command { get; set; }
        private SqlDataAdapter _dataAdapter { get; set; }

        public SqlDatabase()
        {
            _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["WebMayTinh_conn"].ToString();
        }

        //Thuc hien ket noi den DB
        public bool Connect()
        {
            bool functionReturnValue = false;
            functionReturnValue = true;
            try
            {
                _connection = new SqlConnection(_connectionString);
                _connection.Open();
            }
            catch (Exception Ex)
            {
                functionReturnValue = false;
                throw (Ex);
            }
            return functionReturnValue;
        }

        //Dong 1 ket noi toi DB
        public void Close()
        {
            try
            {
                if ((_connection.State == ConnectionState.Open))
                {
                    _connection.Close();
                    _connection.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw (ex);

            }            
        }        

        //Thuc hien 1 cau query tra ve 1 datatable
        //Input: cau lenh Sql
        //Output: datatable
        public DataTable GetDataTable(string cmd)
        {
            DataTable table = new DataTable();
            if ((Connect() == false))
                return null;

            _dataAdapter = new SqlDataAdapter();
            _dataAdapter.SelectCommand = new SqlCommand(cmd, _connection);

            try
            {
                _dataAdapter.Fill(table);
            }
            catch (Exception ex)
            {
                table = null;
                throw (ex);

            }
            finally
            {
                Close();
            }
            return table;
        }


        /// <summary>
        /// Thực thi 1 SqlCommand và trả về 1 dataTable
        /// </summary>
        /// <param name="spname"></param>
        /// <returns></returns>
        public DataTable GetDataTableCmd(SqlCommand cmd)
        {
            DataTable dt = new DataTable();
            if ((Connect() == false))
                return null;           

            try
            {
                cmd.Connection = _connection;
                _dataAdapter = new SqlDataAdapter(cmd);
                _dataAdapter.Fill(dt);
            }
            catch (Exception ex)
            {
                dt = null;
                throw (ex);

            }
            finally
            {
                Close();
            }
            return dt;
        }


        //Mo 1 dataset moi bang cach thuc hien 1 cau lenh Sql
        //Input: 1.cau lenh Sql
        //       2.ten table (bi danh) de truy xuat du lieu tu table moi duoc tao tu cau lenh Sql 1 cach tu nhien hon
        //Ouput: dataset
        public DataSet GetDataSet(string sql, string nametable)
        {
            DataSet functionReturnValue = null;
            if ((Connect() == false))
                return null;
            DataSet ds = new DataSet();
            try
            {
                _dataAdapter = new SqlDataAdapter(sql, _connection);
                _dataAdapter.Fill(ds, nametable);
                functionReturnValue = ds;
            }
            catch (Exception ex)
            {
                functionReturnValue = null;
                throw (ex);

            }
            finally
            {
                Close();
            }
            return functionReturnValue;
        }

        //Thuc hien 1 cau lenh Insert, Update, Delete, hay 1 procedure 
        //Input: cau lenh 
        //Output: Boolean cho biet thuc hien thanh cong hay khong ?
        public bool ExecuteNonQuery(string sql)
        {
            bool functionReturnValue = false;
            if ((Connect() == false))
                return false;

            SqlCommand cmd = new SqlCommand(sql, _connection);
            cmd.CommandType = CommandType.Text;
            try
            {
                cmd.ExecuteNonQuery();
                functionReturnValue = true;
            }
            catch (Exception e)
            {
                functionReturnValue = false;
                throw (e);

            }
            Close();
            return functionReturnValue;
        }
       
        //Thuc hien 1 cau lenh Insert, Update, Delete, hay 1 procedure = cach viet cau lenh "exec sp_fgsd"
        //Input: cau lenh 
        //Output: Boolean cho biet thuc hien thanh cong hay khong ?
        public bool Execute(string sql)
        {
            bool returnValue = false;
            if ((Connect() == false))
                return false;

            _command = new SqlCommand(sql, _connection);
            _command.CommandType = CommandType.Text;
            try
            {
                _command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                returnValue = false;
                throw (e);

            }
            Close();
            returnValue = true;
            return returnValue;
        }

        /// <summary>
        /// Thực thi 1 store procedure Insert, Update, Delete
        /// </summary>
        /// <param name="spName"></param>
        /// <returns></returns>
        public bool ExecuteCmd(SqlCommand cmd)
        {
            bool returnValue = false;
            //int i = 0;
            if ((Connect() == false))
                return false;
            returnValue = true;
            cmd.Connection = _connection;
            cmd.CommandType = CommandType.StoredProcedure;

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                returnValue = false;
                throw (ex);

            }
            finally
            {
                Close();
            }
            return returnValue;
        }       
    }
}