﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.WebSockets;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.QuangCao.DanhSachQuangCao
{
    public partial class DanhSachQuangCaoUpdate : System.Web.UI.UserControl
    {
        NhomQuangCaoDAL nhomQuangCaoDAL = new NhomQuangCaoDAL();
        QuangCaoDAL quangCaoDAL = new QuangCaoDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhSachNhomQuangCao();
            }

            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }
            HienThiThongTin(id);
        }

        private void HienThiThongTin(int id)
        {
            DataTable dt = quangCaoDAL.QuangCao_SelectById(id);
            if(dt.Rows.Count > 0)
            {
                ddlNhomQC.SelectedValue = dt.Rows[0]["MaNhomQC"].ToString();
                txtTenQC.Text = dt.Rows[0]["TenQC"].ToString();
                ltrHinhAnh.Text += @"<img class='img-load' src='/IMG/QuangCao/" + dt.Rows[0]["AnhDaiDien"] + @"' >";
                hdfHinhAnh.Value = dt.Rows[0]["AnhDaiDien"].ToString();
                txtLienKet.Text = dt.Rows[0]["LienKet"].ToString();
                txtThuTu.Text = dt.Rows[0]["ThuTu"].ToString();
            }
        }

        private void LayDanhSachNhomQuangCao()
        {
            DataTable dt = nhomQuangCaoDAL.NhomQuangCao_Select();
            ddlNhomQC.DataSource = dt;
            ddlNhomQC.DataValueField = "MaNhomQC";
            ddlNhomQC.DataTextField = "TenNhomQC";
            ddlNhomQC.DataBind();
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            string tenAnh = "";
            try
            {
                if (flHinhAnh.FileContent.Length > 0)
                {
                    if (flHinhAnh.FileName.EndsWith(".jpg") || flHinhAnh.FileName.EndsWith(".png")
                               || flHinhAnh.FileName.EndsWith(".gif") || flHinhAnh.FileName.EndsWith(".jpeg"))
                    {
                        flHinhAnh.SaveAs(Server.MapPath("/IMG/QuangCao/" + flHinhAnh.FileName));
                    }
                    tenAnh = flHinhAnh.FileName;
                }
                else
                {
                    tenAnh = hdfHinhAnh.Value;
                }

                quangCaoDAL.QuangCao_Update(id,Convert.ToInt32(ddlNhomQC.SelectedValue), txtTenQC.Text.Trim(),
                    tenAnh, txtLienKet.Text.Trim(), Convert.ToInt32(txtThuTu.Text.Trim()));
                Response.Redirect("Admin.aspx?uc=adv&suc=lst");
            }
            catch (Exception)
            {
                lblThongBao.Text = "Chỉnh sửa thất bại";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {

        }
    }
}