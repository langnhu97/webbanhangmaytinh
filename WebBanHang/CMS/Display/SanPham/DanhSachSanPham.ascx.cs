﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.SanPham
{
    public partial class DanhSachSanPham : System.Web.UI.UserControl
    {
        DanhMucDAL danhMucDAL = new DanhMucDAL();
        SanPhamDAL sanPhamDAL = new SanPhamDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }
            if (!IsPostBack)
            {
                ltrDanhMucSanPham.Text = LayDanhMucSanPham();
            }
        }

        private string LayDanhMucSanPham()
        {
            string s = "";
            DataTable dt = danhMucDAL.DanhMuc_SelectById(id);

            s += "<div>";
            s += @"
                    <div class='head-mid mt-5 mb-3' style='color:#FF5E22'>
                         " + dt.Rows[0]["TenDM"] + @"                       
                    </div>                    
                ";

            s += "<div class='row pb-3'>";
            s += LaySanPham(Convert.ToInt32(dt.Rows[0]["MaDM"].ToString()));
            s += "</div>";
            s += "<div style='clear:both'><div>";
            s += "</div>";

            return s;
        }

        private string LaySanPham(int MaDM)
        {
            string s = "";
            DataTable dt = sanPhamDAL.SanPham_SelectByDanhMuc(MaDM);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["MaSP"] == null) break;
                string link = "Default.aspx?uc=prd&suc=dtl&id=" + dt.Rows[i]["MaSP"];

                s += @"
                        <figure class='product-item col-md-3 float-left p-3' style='height:225px'>
                            <a href='" + link + @"' class='w-100 h-75 d-inline-block mb-3'>
                                <img src='IMG/SanPham/" + dt.Rows[i]["HinhAnh"] + @"' class='img-fluid w-100 h-100'>
                            </a>
                            <div class='text-center'>
                                <a href='" + link + @"' style='color:#000000' class='product-item-name w-100 h-15 text-center font-weight-bold'>
                                   " + dt.Rows[i]["TenSP"] + @"
                                </a>
                            </div>
                            <div class='text-center'>                            
                                <a href='" + link + @"' style='color:#FF5E22'  class='product-item-name w-100 h-10 text-center font-weight-bold'>
                                    " + Convert.ToDouble(dt.Rows[i]["GiaBan"]).ToString("N").Replace(".00", "") + " " + @"
                                    VNĐ
                                </a>
                            </div>
                        </figure>
                ";
            }

            return s;
        }
    }
}