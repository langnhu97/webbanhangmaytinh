﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DaiLyInsert.ascx.cs" Inherits="WebBanHang.CMS.Admin.DaiLyCungCap.DaiLyInsert" %>
<div class="head">
    Thêm mới Đại lý cung cấp
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">        
        <div class="attr">Tên đại lý</div>
        <div class="val">
            <asp:TextBox ID="txtTenDL" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="***" SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenDL"></asp:RequiredFieldValidator>
        </div>        
    </div>     
   <div class="field">        
        <div class="attr">Địa chỉ</div>
        <div class="val">
            <asp:TextBox ID="txtDiaChi" class="txt" runat="server"></asp:TextBox>
        </div>        
    </div>
    <div class="field">        
        <div class="attr">Số điện thoại</div>
        <div class="val">
            <asp:TextBox ID="txtSDT" class="txt" runat="server"></asp:TextBox>
        </div>        
    </div>
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:CheckBox ID="ckTiepTuc" Text="Tiếp tục thêm mới sau khi tạo sản phẩm này" runat="server" />
        </div>
    </div>
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn btnThemMoi" ID="btnThemMoi" runat="server" Text="Thêm mới" OnClick="btnThemMoi_Click"/>
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click"/>
        </div>
    </div>
</div>