﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TinTuc
{
    public partial class TinTucInsert : System.Web.UI.UserControl
    {
        DanhMucTinTucDAL danhMucTinTucDAL = new DanhMucTinTucDAL();
        TinTucDAL tinTucDAL = new TinTucDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhMucTinTuc();
            }
        }

        private void LayDanhMucTinTuc()
        {
            DataTable dt = danhMucTinTucDAL.DanhMucTinTuc_Select();
            ddlDMTT.DataSource = dt;
            ddlDMTT.DataValueField = "MaDMTT";
            ddlDMTT.DataTextField = "TenDMTT";
            ddlDMTT.DataBind();
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                if (flHinhAnh.FileContent.Length > 0)
                {
                    if (flHinhAnh.FileName.EndsWith(".jpg") || flHinhAnh.FileName.EndsWith(".png")
                        || flHinhAnh.FileName.EndsWith(".gif") || flHinhAnh.FileName.EndsWith(".jpeg")
                        || flHinhAnh.FileName.EndsWith(".psd") || flHinhAnh.FileName.EndsWith(".pdf"))
                    {
                        flHinhAnh.SaveAs(Server.MapPath("IMG/TinTuc/") + flHinhAnh.FileName);
                    }
                }
                int maDMTT = Convert.ToInt32(ddlDMTT.SelectedValue);
                string tieuDe = txtTieuDe.Text.Trim();
                string mota = txtMoTa.Text.Trim();
                int luotxem = txtLuotXem.Text != "" ? Convert.ToInt32(txtLuotXem.Text.Trim()) : 0;
                DateTime ngaySinh = txtNgayDang.Text != "" ? Convert.ToDateTime(txtNgayDang.Text) : DateTime.Today;
                string tenAnh = flHinhAnh.FileName;
                string chiTiet = txtChiTiet.Text;

                tinTucDAL.TinTuc_Insert(maDMTT, tieuDe, mota, luotxem, ngaySinh, tenAnh, chiTiet);
                lblThongBao.Text = "<div class='successNoti'>Đã tạo thành công</div>";
                if (ckTiepTuc.Checked)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=new&suc=lst");
                }

            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Tạo thất bại</div>";
            }
        }

        private void ResetForm()
        {
            ddlDMTT.SelectedIndex = -1;
            txtTieuDe.Text = "";
            txtMoTa.Text = "";
            txtLuotXem.Text = "";
            txtNgayDang.Text = "";
            txtChiTiet.Text = "";
            flHinhAnh.FileContent.Flush();
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}