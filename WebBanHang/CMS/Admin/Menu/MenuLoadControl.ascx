﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuLoadControl.ascx.cs" Inherits="WebBanHang.CMS.Admin.Menu.MenuLoadControl" %>
<div class="container">
     <div class="colLeft">  
        <div class="head">
            Quản lý
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("menu","lst","") %>" href="Admin.aspx?uc=men&suc=lst"title="Danh sách quảng cáo">Danh sách menu</a></li>
        </ul>
        <div class="head">
            Thêm mới
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("menu","lst","add") %>" href="Admin.aspx?uc=men&suc=lst&man=add" title="Thêm mới menu">Thêm mới menu</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="colRight">
        <asp:PlaceHolder ID="plControl" runat="server"></asp:PlaceHolder>
    </div>
</div>
