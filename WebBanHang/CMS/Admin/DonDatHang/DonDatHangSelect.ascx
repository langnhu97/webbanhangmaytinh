﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DonDatHangSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.DonDatHang.DonDatHangSelect" %>
<div class="head">
    Các đơn đặt hàng đã tạo
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=ord&suc=lst&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colDate">Ngày đặt hàng</th>
            <th class="colState">Tình trạng</th>
            <th class="colName">Tên Khách Hàng</th>
            <th class="colPhone">SĐT</th>
            <th class="colPhone">Email</th>
            <th class="colMoney">Tổng tiền</th>
            <th class="colTool">Công cụ</th>
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc muốn xóa danh mục này?")) {
            $.post("CMS/Admin/DonDatHang/AJAX/DonDatHang.aspx",
                {
                    "man": "del",
                    "id": id
                },
                function (data, status) {
                    if (data == "1") //data = 1: thực hiện thành công
                    {
                        $("#rowId_" + id).slideUp();
                    } else {
                        alert("Xóa thất bại");
                    }
                }
            );
        }
        
    }
</script>