﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QuangCaoLoadControl.ascx.cs" Inherits="WebBanHang.CMS.Admin.QuangCao.QuangCaoLoadControl" %>
<div class="container">
     <div class="colLeft">  
        <div class="head">
            Quản lý
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("adv","grp","") %>" href="Admin.aspx?uc=adv&suc=grp"title="Nhóm quảng cáo">Nhóm quảng cáo</a></li>
            <li><a class="<%=DanhDauThaoTac("adv","lst","") %>" href="Admin.aspx?uc=adv&suc=lst"title="Danh sách quảng cáo">Danh sách quảng cáo</a></li>
        </ul>
        <div class="head">
            Thêm mới
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("adv","grp","add") %>" href="Admin.aspx?uc=adv&suc=grp&man=add" title="Thêm mới nhóm quảng cáo">Nhóm quảng cáo</a></li>
            <li><a class="<%=DanhDauThaoTac("adv","lst","add") %>" href="Admin.aspx?uc=adv&suc=lst&man=add" title="Thêm mới quảng cáo">Danh sách quảng cáo</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="colRight">
        <asp:PlaceHolder ID="plQuangCaoControl" runat="server"></asp:PlaceHolder>
    </div>
</div>
