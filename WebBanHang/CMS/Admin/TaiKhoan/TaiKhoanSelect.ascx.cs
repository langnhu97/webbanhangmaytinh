﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TaiKhoan
{
    public partial class TaiKhoanSelect : System.Web.UI.UserControl
    {
        TaiKhoanDAL _taiKhoanDAL = new TaiKhoanDAL();
        QuyenDangNhapDAL _quyenDangNhapDAL = new QuyenDangNhapDAL();
        protected void Page_Load(object sender, EventArgs e)        
        {
            if (!IsPostBack)
            {
                LayDanhSach();
            }
        }

        private void LayDanhSach()
        {
            DataTable dt = _taiKhoanDAL.TaiKhoan_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataTable dt2 = _quyenDangNhapDAL.QuyenDangNhap_SelectById(Convert.ToInt32(dt.Rows[i]["MaQuyen"]));

                string tenQuyen = dt2.Rows[0]["TenQuyen"].ToString();
                ltrDanhMuc.Text += @"
                    <tr id=''>
                        <td class='colLoginName w150'>" + dt.Rows[i]["TenDangNhap"] + @"</td>
                        <td class='colEmail'>" + dt.Rows[i]["EmailDK"] + @"</td>
                        <td class='colAdd'>" + dt.Rows[i]["DiaChiDK"] + @"</td>
                        <td class='colName'>" + dt.Rows[i]["HoTen"] + @"</td>
                        <td class='colBirtdDay'>" + dt.Rows[i]["NgaySinh"] + @"</td>
                        <td class='colRole'>" + tenQuyen + @"</td>
                        <td class='colTool'>
                            <a href = 'Admin.aspx?uc=acc&suc=lst&man=upd&id=" + dt.Rows[i]["TenDangNhap"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                            <a href = 'Admin.aspx?uc=acc&suc=lst&man=del&id=" + dt.Rows[i]["TenDangNhap"] + @"' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                        </td>          
                     </tr>";            
            }
        }
    }   
}