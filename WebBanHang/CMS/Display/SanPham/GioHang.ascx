﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="GioHang.ascx.cs" Inherits="WebBanHang.CMS.Display.SanPham.GioHang" %>
<script src="Libs/jquery-3.6.4.min.js"></script>
<div class="cart px-3">
    <div class="cart-head row d-flex justify-content-center py-2 text-body text-uppercase">
        Bạn có &nbsp<span id="TongSoLuongSanPham"> 0 </span>&nbsp sản phẩm trong giỏ hàng
    </div>
    <div class="cart-detail row p-3" id="BangThongTinGioHang">
    </div>
    <hr>
    <div class="cart-note row d-flex p-3">
        Quý khách vui lòng điền đầy đủ thông tin theo form phía dưới và nhấn nút Đặt hàng.
        Hệ thống sẽ kiểm tra và tạo cho quý khách một tài khoản thành viên với tên đăng nhập và mật khẩu là email của quý khách
    </div>
    <div class="cart-info row d-flex justify-content-center py-2">
        <div class="cart-info-row row w-100 py-1">
            <div class="col-md-4">Họ tên</div>
            <div class="col-md-8">
                <input id="txtTenKH" type="text" class="w-100" value="<%=hoTen%>" />
            </div>
        </div>
        <div class="cart-info-row row w-100 py-1">
            <div class="col-md-4">Địa chỉ</div>
            <div class="col-md-8">
                <input id="txtDiaChi" type="text" class="w-100" value="<%=diaChi%>" />
            </div>
        </div>
        <div class="cart-info-row row w-100 py-1">
            <div class="col-md-4">Số điện thoại</div>
            <div class="col-md-8">
                <input id="txtSDT" type="text" class="w-100" value="<%=email%>" />
                <%--<asp:TextBox ID="txtSDT" runat="server" class="w-100" value></asp:TextBox>--%>
            </div>
        </div>
        <div class="cart-info-row row w-100 py-1">
            <div class="col-md-4">Email</div>
            <div class="col-md-8">
                <input id="txtEmailKH" type="text" class="w-100" value="<%=sdt%>" />
            </div>
        </div>
    </div>
    <div class="cart-total-money row d-flex justify-content-end py-3">
        <div class="col-md-3">Tổng cộng</div>
        <div class="col-md-3"><span id="TongTienGioHang">0</span><sup>VNĐ</sup></div>
    </div>
    <div class="cart-pay row py-3 px-3 border border-info rounded">
        <div class="w-100 py-1">
            PHƯƠNG THỨC THANH TOÁN
            <br />
        </div>
        <div class="w-100">
            <input id="rbChuyenKhoan" type="radio" name="rbHinhThucThanhToan" />
            <span>
                <img src="IMG/Logo/nhanhang.png" class="img-fluid" style="height: 20px; width: auto" />
            </span>
            <label for="rbChuyenKhoan">THANH TOÁN KHI NHẬN HÀNG (COD)</label>
        </div>

        <div class="w-100">
            <input id="rbOnepay" type="radio" name="rbHinhThucThanhToan" />
            <span>
                <img src="IMG/Logo/chuyenkhoan.png" class="img-fluid" style="height: 20px; width: auto" />
            </span>
            <label for="rbOnepay">CHUYỂN KHOẢN QUA NGÂN HÀNG</label>
            <div class="paymentInfo">
            </div>
        </div>

        <div class="w-100">
            <input id="rbOnepayQuocTe" type="radio" name="rbHinhThucThanhToan" checked="checked" />
            <span>
                <img src="IMG/Logo/chuyenkhoan.png" class="img-fluid" style="height: 20px; width: auto" />
            </span>
            <label for="rbOnepayQuocTe">THANH TOÁN TRỰC TUYẾN QUA THẺ VISA, MASTER,...</label>
            <div class="paymentInfo">
                <%--<script type="text/javascript"src="http://202.9.84.88/documents/payment/logoscript.jsp?logos=v,m,a,j,u,at&lang=en"></script>--%>
                <div class="clearfix">
                    <!---->
                </div>
            </div>
        </div>
        <br />
    </div>
    <div class="cart-test-card-info">
        <span>
            <br />
            ***Thông tin thẻ dùng cho cổng test của onepay***<br />
            Loại tài khoản: Visa<br />
            Số thẻ: 4000000000000002 or 5313581000123430<br />
            Date Exp: 05/23<br />
            CVV/CSC: 123<br />
            <%--Street: Tran Quang Khai<br />
                City/Town: Hanoi<br />
                State/Province: North<br />
                Postcode(zip code): 1234<br />
                Country: VietNam<br />--%>
        </span>
    </div>
    <div class="cart-order row d-flex justify-content-around p-3">
        <div class="col-md-3">
            <asp:LinkButton ID="btnQuayLai" runat="server" href="Default.aspx?uc=prd">
                <i class="fa-solid fa-arrow-left"></i>
                Tiếp tục mua hàng
            </asp:LinkButton>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-3">
            <asp:LinkButton ID="btnDatHang" CssClass="btn btn-primary w-100" href="javascript:GuiDonHang()" runat="server">
                Đặt hàng
                <i class="fa-solid fa-arrow-right"></i>
            </asp:LinkButton>
        </div>
    </div>
</div>
<script type="text/javascript">
    //Gửi thông tin đơn hàng
    function GuiDonHang() {

        if ($("#txtTenKH").val() !== "" && $("#txtSDT").val() !== "") {

            var email = $("#txtEmailKH").val();
            var phone = $("#txtSDT").val();
            var emailRegex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/g;
            var phoneRegex = /((09|03|07|08|05)+([0-9]{8})\b)/g;
            if (!emailRegex.test(email) || !phoneRegex.test(phone)) {
                alert("Email hoặc SĐT không đúng định dạng");
            } else {
                var phuongthucthanhtoan = "";
                if ($("#rbChuyenKhoan").is(":checked")) phuongthucthanhtoan = "ChuyenKhoan";
                if ($("#rbOnepay").is(":checked")) phuongthucthanhtoan = "Onepay";
                if ($("#rbOnepayQuocTe").is(":checked")) phuongthucthanhtoan = "OnepayQuocTe";

                $.post("CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
                    {
                        "ThaoTac": "GuiDonHang",
                        "HoTen": $("#txtTenKH").val(),
                        "DiaChi": $("#txtDiaChi").val(),
                        "SoDienThoai": $("#txtSDT").val(),
                        "EmailKH": $("#txtEmailKH").val(),
                        "phuongThucThanhToan": phuongthucthanhtoan
                    },
                    function (data, status) {
                        //Nếu không có lỗi (tức là xóa thành công) --> thông báo đặt hàng thành công --> đẩy về trang chủ
                        if (phuongthucthanhtoan === "ChuyenKhoan") {
                            if (data === "") {
                                alert("Bạn đã gửi đơn hàng thành công");
                            }
                        } else {//Trường hợp thanh toán online --> đẩy tới trang thanh toán của đơn vị tích hợp
                            location.href = data;
                        }
                    }
                );
            }
        } else {
            alert("Bạn vui lòng điền đủ họ tên và số điện thoại để đặt hàng");
        }
    }


    //Lấy thông tin các sản phẩm có trong giỏ
    function LayThongTinGioHang() {
        $.post("CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
            {
                "ThaoTac": "LayThongTinGioHang"
            },
            function (data, status) {
                if (data != null) {
                    $("#BangThongTinGioHang").html(data);
                }
            }
        );
    }

    //Lấy tổng số lượng các sản phẩm trong giỏ
    function LayTongSoLuongSP() {
        $.post("CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
            {
                "ThaoTac": "LayTongSoLuongSP",
            },
            function (data, status) {
                if (data != null) {
                    $("#TongSoLuongSanPham").html(data);
                }
            }
        );
    }

    //Lấy tổng tiền sản phẩm trong giỏ
    function LayTongTien() {
        $.post("CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
            {
                "ThaoTac": "LayTongTien",
            },
            function (data, status) {
                if (data != null) {
                    $("#TongTienGioHang").html(data);
                }
            }
        );
    }

    //Xóa sản phẩm trong giỏ
    function XoaSPTrongGioHang(id) {
        if (confirm("Bạn có chắc xóa sản phẩm này?")) {
            $.post("CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
                {
                    "ThaoTac": "XoaSanPhamTrongGio",
                    "id": id
                },
                function (data, status) {
                    if (data === "") {
                        $("#rowSP_" + id).remove();
                        LayTongSoLuongSP();
                        LayTongTien();
                    }
                }
            );
        }
    }

    //Cập nhật số lượng sản phẩm trong giỏ
    function CapNhatSoLuong(id) {
        var soLuongMoi = $("#quantity_" + id).val();

        $.post("CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
            {
                "ThaoTac": "CapNhatSoLuong",
                "id": id,
                "SoLuongMoi": soLuongMoi
            },
            function (data, status) {
                if (data === "") {
                    LayTongSoLuongSP();
                    LayTongTien();
                }
            }
        );
    }

    $(document).ready(
        function () {
            LayThongTinGioHang();
            LayTongSoLuongSP();
            LayTongTien();
        }
    )
</script>
