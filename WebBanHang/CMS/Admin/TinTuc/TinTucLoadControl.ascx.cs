﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin.TinTuc
{
    public partial class TinTucLoadControl : System.Web.UI.UserControl
    {
        string suc = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["suc"] != null)
            {
                suc = Request.QueryString["suc"];
            }

            switch (suc)
            {
                case "grp":
                    plTinTucControl.Controls.Add(LoadControl("DanhMucTinTuc/DanhMucTinTucLoadControl.ascx"));
                    break;
                case "lst":
                    plTinTucControl.Controls.Add(LoadControl("DanhSachTinTuc/DanhSachTinTucLoadControl.ascx"));
                    break;
                default:
                    plTinTucControl.Controls.Add(LoadControl("DanhSachTinTuc/DanhSachTinTucLoadControl.ascx"));
                    break;
            }
        }

        protected string DanhDauThaoTac(string ucName, string sucName, string manName)
        {
            string s = "";

            //Lấy giá trị queryString trong modul
            string uc = String.Empty;
            string suc = String.Empty;
            string man = String.Empty;
            if (Request.QueryString["uc"] != null)
            {
                uc = Request.QueryString["uc"];
            }
            if (Request.QueryString["suc"] != null)
            {
                suc = Request.QueryString["suc"];
            }
            if (Request.QueryString["man"] != null)
            {
                man = Request.QueryString["man"];
            }

            //So sánh queryString bằng tên uc + suc + man truyền vào thì trả về current --> Đánh dấu là menu hiện tại
            if (uc == ucName && suc == sucName && man == manName)
                s = "current";

            return s;
        }
    }
}