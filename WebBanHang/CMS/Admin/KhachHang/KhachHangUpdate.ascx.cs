﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.KhachHang
{
    public partial class KhachHangUpdate : System.Web.UI.UserControl
    {
        KhachHangDAL dal = new KhachHangDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }
            HienThiThongTin(id);            
        }

        private void HienThiThongTin(int id)
        {
            DataTable dt = dal.KhachHang_SelectById(id);

            if(dt.Rows.Count > 0)
            {
                txtTenKH.Text = dt.Rows[0]["TenKH"].ToString();
                txtDiaChi.Text = dt.Rows[0]["DiaChi"].ToString();
                txtSDT.Text = dt.Rows[0]["SDT"].ToString();
                txtEmail.Text = dt.Rows[0]["Email"].ToString();
                hdfMatKhau.Value = dt.Rows[0]["MatKhau"].ToString();
            }
        }

        protected void btnChinhSua_Click(object sender, EventArgs e)
        {
            try
            {
                string matkhau = txtMatKhau.Text.Trim() != "" ? txtMatKhau.Text.Trim() : hdfMatKhau.Value.ToString();
                dal.KhachHang_Update(id, txtTenKH.Text.Trim(), txtDiaChi.Text.Trim(), txtSDT.Text.Trim(),
                    txtEmail.Text.Trim(), MaHoa.GetMD5(matkhau));
                
                Response.Redirect("Admin.aspx?uc=cus&suc=lst");
                
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chỉnh sửa thất bại</div>";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenKH.Text = "";
            txtDiaChi.Text = "";
            txtEmail.Text = "";
            txtSDT.Text = "";
            txtMatKhau.Text = "";
        }
    }
}