﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KhachHangLoadControl.ascx.cs" Inherits="WebBanHang.CMS.Admin.KhachHang.KhachHangLoadControl" %>
<div class="container">
     <div class="colLeft">  
        <div class="head">
            Quản lý
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("cus","lst","") %>" href="Admin.aspx?uc=cus&suc=lst"title="Danh sách khách hàng">Danh sách khách hàng</a></li>
        </ul>
        <div class="head">
            Thêm mới
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("cus","lst","add") %>" href="Admin.aspx?uc=cus&suc=lst&man=add" title="Thêm mới khách hàng">Khách hàng</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="colRight">
        <asp:PlaceHolder ID="plControl" runat="server"></asp:PlaceHolder>
    </div>
</div>
