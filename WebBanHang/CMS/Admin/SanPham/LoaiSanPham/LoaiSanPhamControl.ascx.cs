﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin.SanPham.LoaiSanPham
{
    public partial class LoaiSanPhamControl : System.Web.UI.UserControl
    {
        string queryString = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["man"] != null)
            {
                queryString = Request.QueryString["man"];
            }

            switch (queryString)
            {
                case "add":
                    plControl.Controls.Add(LoadControl("LoaiSanPhamInsert.ascx"));
                    break;
                case "upd":
                    plControl.Controls.Add(LoadControl("LoaiSanPhamUpdate.ascx"));
                    break;
                case "del":
                    plControl.Controls.Add(LoadControl("LoaiSanPhamDelete.ascx"));
                    break;
                case "dis":
                    plControl.Controls.Add(LoadControl("LoaiSanPhamSelect.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("LoaiSanPhamSelect.ascx"));
                    break;
            }
        }
    }
}