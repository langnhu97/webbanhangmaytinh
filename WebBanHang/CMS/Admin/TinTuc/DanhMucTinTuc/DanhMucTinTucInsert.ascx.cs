﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TinTuc.DanhMucTinTuc
{
    public partial class DanhMucTinTucInsert : System.Web.UI.UserControl
    {
        DanhMucTinTucDAL dal = new DanhMucTinTucDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.DanhMucTinTuc_Insert(txtTenDMTT.Text.Trim(), Convert.ToInt32(txtThuTu.Text));
                lblThongBao.Text = "<div class='successNoti'>Đã tạo " + txtTenDMTT.Text + "</div>";
                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=new&suc=grp");
                }

            }
            catch (Exception)
            {
                lblThongBao.Text = "Tạo thất bại";
            }
        }

        private void ResetForm()
        {
            txtTenDMTT.Text = "";
            txtThuTu.Text = "";
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}