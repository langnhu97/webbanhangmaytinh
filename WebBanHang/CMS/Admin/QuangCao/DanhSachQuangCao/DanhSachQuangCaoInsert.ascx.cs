﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.QuangCao.DanhSachQuangCao
{
    public partial class DanhSachQuangCaoInsert : System.Web.UI.UserControl
    {
        NhomQuangCaoDAL nhomQuangCaoDAL = new NhomQuangCaoDAL();
        QuangCaoDAL quangCaoDAL = new QuangCaoDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhSachNhomQuangCao();
            }
        }

        private void LayDanhSachNhomQuangCao()
        {
            DataTable dt = nhomQuangCaoDAL.NhomQuangCao_Select();
            ddlNhomQC.DataSource = dt;
            ddlNhomQC.DataValueField = "MaNhomQC";
            ddlNhomQC.DataTextField = "TenNhomQC";
            ddlNhomQC.DataBind();
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                //Lưu ảnh 
                if (flHinhAnh.FileContent.Length > 0)
                {
                    if (flHinhAnh.FileName.EndsWith(".jpg") || flHinhAnh.FileName.EndsWith(".jpeg")
                        || flHinhAnh.FileName.EndsWith(".png") || flHinhAnh.FileName.EndsWith(".gif"))
                    {
                        flHinhAnh.SaveAs(Server.MapPath("IMG/QuangCao/") + flHinhAnh.FileName);
                    }
                }

                int maNhomQC = Convert.ToInt32(ddlNhomQC.SelectedValue);
                string tenQC = txtTenQC.Text.Trim();
                string lienKet = txtLienKet.Text.Trim();
                int thuTu = Convert.ToInt32(txtThuTu.Text.Trim());

                quangCaoDAL.QuangCao_Insert(maNhomQC, tenQC, flHinhAnh.FileName, lienKet, thuTu);
                lblThongBao.Text = "<div class='successNoti'>Đã tạo quảng cáo" + tenQC + @"</div>";
                if (ckTiepTuc.Checked)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=adv&suc=lst");
                }
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Tạo thất bại</div>";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenQC.Text = "";
            txtThuTu.Text = "";
            txtTenQC.Text = "";
            flHinhAnh.FileContent.Flush();
        }
    }
}