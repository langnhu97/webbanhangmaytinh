﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.Menu
{
    public partial class MenuSelect : System.Web.UI.UserControl
    {
        MenuDAL dal = new MenuDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = dal.Menu_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaMenu"] + @"'>
                    <td class='colId'>" + dt.Rows[i]["MaMenu"] + @"</td>
                    <td class='colName'>" + dt.Rows[i]["TenMenu"] + @"</td>
                    <td class='colNote'>" + dt.Rows[i]["LienKet"] + @"</td>
                    <td class='colOrd'>" + dt.Rows[i]["ThuTu"] + @"</td>
                    <td class='colTool'>
                        <a href = 'Admin.aspx?uc=men&suc=grp&man=upd&id=" + dt.Rows[i]["MaMenu"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaMenu"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>                         
                </tr>
                ";
            }
        }
    }
}