﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.QuangCao.NhomQuangCao
{
    public partial class NhomQuangCaoInsert : System.Web.UI.UserControl
    {
        NhomQuangCaoDAL dal = new NhomQuangCaoDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.NhomQuangCao_Insert(txtTenNhomQC.Text.Trim(), txtViTri.Text.Trim(), Convert.ToInt32(txtThuTu.Text));
                lblThongBao.Text = "<div class='successNoti'>Đã tạo " + txtTenNhomQC.Text + "</div>";
                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=adv&suc=grp");
                }

            }
            catch (Exception)
            {
                lblThongBao.Text = "Tạo thất bại";
                throw;
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenNhomQC.Text = String.Empty;
            txtViTri.Text = String.Empty;
            txtThuTu.Text = String.Empty;
            txtTenNhomQC.Focus();
        }

        protected void btnThemMoi_Click1(object sender, EventArgs e)
        {

        }

        protected void btnHuy_Click1(object sender, EventArgs e)
        {

        }
    }
}