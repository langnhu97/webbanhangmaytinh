﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.DanhMucSanPham
{
    public partial class DanhMucUpdate : System.Web.UI.UserControl
    {
        DanhMucDAL dal = new DanhMucDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            id = Convert.ToInt32(Request.QueryString["id"]);
            HienThiThongTin(id);
        }

        private void HienThiThongTin(int id)
        {
            DataTable dt = dal.DanhMuc_SelectById(id);

            txtTenDM.Text = dt.Rows[0]["TenDM"].ToString();
            txtGhiChu.Text = dt.Rows[0]["GhiChu"].ToString();
            txtSoSPHienThi.Text = dt.Rows[0]["SoSPHienThi"].ToString();
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.DanhMuc_Update(id, txtTenDM.Text.Trim(), Convert.ToInt32(txtSoSPHienThi.Text), txtGhiChu.Text.Trim());
                Response.Redirect("Admin.aspx?uc=prd&suc=cat");

            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa sửa được danh mục</div>";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenDM.Text = "";
            txtGhiChu.Text = "";
            txtSoSPHienThi.Text = "";
        }
    }
}