﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.QuangCao.NhomQuangCao
{
    public partial class NhomQuangCaoSelect : System.Web.UI.UserControl
    {
        NhomQuangCaoDAL dal = new NhomQuangCaoDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = dal.NhomQuangCao_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaNhomQC"] + @"'>
                    <td class='colId'>" + dt.Rows[i]["MaNhomQC"] + @"</td>
                    <td class='colName'>" + dt.Rows[i]["TenNhomQC"] + @"</td>
                    <td class='colAdd'>" + dt.Rows[i]["ViTri"] + @"</td>
                    <td class='colOrd'>" + dt.Rows[i]["ThuTu"] + @"</td>
                    <td class='colTool'>
                        <a href = 'Admin.aspx?uc=adv&suc=grp&man=upd&id=" + dt.Rows[i]["MaNhomQC"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaNhomQC"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>                         
                </tr>
                ";
            }
        }
    }
}