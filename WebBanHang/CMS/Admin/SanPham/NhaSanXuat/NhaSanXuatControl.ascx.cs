﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin.SanPham.NhaSanXuat
{
    public partial class NhaSanXuatControl : System.Web.UI.UserControl
    {
        string man = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["man"] != null)
            {
                man = Request.QueryString["man"];
            }

            switch (man)
            {
                case "add":
                    plControl.Controls.Add(LoadControl("NhaSanXuatInsert.ascx"));
                    break;
                case "upd":
                    plControl.Controls.Add(LoadControl("NhaSanXuatUpdate.ascx"));
                    break;
                case "del":
                    plControl.Controls.Add(LoadControl("NhaSanXuatDelete.ascx"));
                    break;                
                default:
                    plControl.Controls.Add(LoadControl("NhaSanXuatSelect.ascx"));
                    break;
            }
        }
    }
}