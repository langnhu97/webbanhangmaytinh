﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.LoaiSanPham
{
    
    public partial class LoaiSanPhamInsert : System.Web.UI.UserControl    
    {
        LoaiSanPhamDAL dal = new LoaiSanPhamDAL();

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.LoaiSanPham_Insert(txtTenLoaiSP.Text.Trim(), txtGhiChu.Text.Trim());
                lblThongBao.Text = "<div class='successNoti'>Đã tạo loại sản phẩm " + txtTenLoaiSP.Text + "</div>";
                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=prd&suc=typ");
                }

            }
            catch (Exception)
            {
                lblThongBao.Text = "Chưa thêm được loại sản phẩm";
                throw;                
            }                     
        }

        private void ResetForm()
        {
            txtTenLoaiSP.Text = String.Empty;
            txtGhiChu.Text = String.Empty;
            txtTenLoaiSP.Focus();
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}