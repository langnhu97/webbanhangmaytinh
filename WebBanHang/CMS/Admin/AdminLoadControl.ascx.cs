﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin
{
    public partial class AdminLoadControl : System.Web.UI.UserControl
    {
        string queryString = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Request.QueryString["uc"] != null)
            {
                queryString = Request.QueryString["uc"];
            }

            switch (queryString)
            {
                case "prd":
                    plAdminLoadControl.Controls.Add(LoadControl("SanPham/SanPhamLoadControl.ascx"));
                    break;
                case "cus":
                    plAdminLoadControl.Controls.Add(LoadControl("KhachHang/KhachHangLoadControl.ascx"));
                    break;                           
                case "sup":
                    plAdminLoadControl.Controls.Add(LoadControl("DaiLyCungCap/DaiLyLoadControl.ascx"));
                    break;
                case "ord":
                    plAdminLoadControl.Controls.Add(LoadControl("DonDatHang/DonDatHangLoadControl.ascx"));
                    break;
                case "adv":
                    plAdminLoadControl.Controls.Add(LoadControl("QuangCao/QuangCaoLoadControl.ascx"));
                    break;
                case "new":
                    plAdminLoadControl.Controls.Add(LoadControl("TinTuc/TinTucLoadControl.ascx"));
                    break;
                case "acc":
                    plAdminLoadControl.Controls.Add(LoadControl("TaiKhoan/TaiKhoanLoadControl.ascx"));
                    break;
                case "men":
                    plAdminLoadControl.Controls.Add(LoadControl("Menu/MenuLoadControl.ascx"));
                    break;
                default:
                    plAdminLoadControl.Controls.Add(LoadControl("SanPham/SanPhamLoadControl.ascx"));
                    break;
            }
        }
    }
}