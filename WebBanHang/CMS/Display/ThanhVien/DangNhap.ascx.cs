﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.ThanhVien
{
    public partial class DangNhap : System.Web.UI.UserControl
    {
        KhachHangDAL khachHangDAL = new KhachHangDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnDangNhap_Click(object sender, EventArgs e)
        {
            DataTable dt = khachHangDAL.KhachHang_SelectByEmail_MatKhau(txtEmail.Text, MaHoa.GetMD5(txtMatKhau.Text));
            if(dt.Rows.Count > 0)
            {
                Session["KhachHangDangNhap"] = "1";
                Session["MaKH"] = dt.Rows[0]["MaKH"];
                Session["TenKH"] = dt.Rows[0]["TenKH"];
                Session["DiaChi"] = dt.Rows[0]["DiaChi"];
                Session["SDT"] = dt.Rows[0]["SDT"];
                Session["Email"] = dt.Rows[0]["Email"];

                Response.Redirect("/Default.aspx");
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('Thông tin đăng nhập không chính xác');", true);
            }
        }
    }
}