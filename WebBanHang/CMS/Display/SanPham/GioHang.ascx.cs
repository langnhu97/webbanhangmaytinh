﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Display.SanPham
{
    public partial class GioHang : System.Web.UI.UserControl
    {
        protected string  hoTen { get; set; }
        protected string  diaChi { get; set; }
        protected string  email { get; set; }
        protected string  sdt { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["KhachHangDangNhap"] != null && Session["KhachHangDangNhap"].ToString() == "1")
            {
                hoTen = Session["TenKH"].ToString();
                diaChi =  Session["DiaChi"].ToString();
                email = Session["SDT"].ToString();
                sdt = Session["Email"].ToString();
            }
        }        
    }
}