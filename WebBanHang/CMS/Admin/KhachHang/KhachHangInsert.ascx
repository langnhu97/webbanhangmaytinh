﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="KhachHangInsert.ascx.cs" Inherits="WebBanHang.CMS.Admin.KhachHang.KhachHangInsert" %>
<div class="head">
    Thêm mới khách hàng
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">        
        <div class="attr">Tên khách hàng</div>
        <div class="val">
            <asp:TextBox ID="txtTenKH" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="***" SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenKH"></asp:RequiredFieldValidator>
        </div>        
    </div>
   <div class="field">        
        <div class="attr">Địa chỉ</div>
        <div class="val">
            <asp:TextBox ID="txtDiaChi" runat="server"></asp:TextBox>
        </div>        
    </div>    

    <div class="field">        
        <div class="attr">Số điện thoại</div>
        <div class="val">
            <asp:TextBox ID="txtSDT" class="txt" runat="server"></asp:TextBox>
        </div>        
    </div>
       <div class="field">        
        <div class="attr">Email</div>
        <div class="val">
            <asp:TextBox ID="txtEmail" class="txt" runat="server"></asp:TextBox>
        </div>        
    </div>
    
    <div class="field">
        <div class="attr">Mật khẩu</div>
        <div class="val"><asp:TextBox ID="txtMatKhau" TextMode="Password" runat="server"></asp:TextBox></div>
    </div>
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:CheckBox ID="ckTiepTuc" Text="Tiếp tục thêm mới sau khi tạo" runat="server" />
        </div>
    </div>
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn btnThemMoi" ID="btnThemMoi" runat="server" Text="Thêm mới" OnClick="btnThemMoi_Click"/>
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click"/>
        </div>
    </div>
</div>