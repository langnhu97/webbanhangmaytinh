﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebBanHang.Default" %>

<%@ Register Src="~/CMS/Display/DisplayLoadControl.ascx" TagPrefix="uc1" TagName="DisplayLoadControl" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Website bán hàng trungtran.vn</title>
    <!-- Link -->
    <link href="Libs/fontawesome/fontawesome/css/all.min.css" rel="stylesheet" />
    <link href="CMS/Display/CSS/index.css" rel="stylesheet" />
    <link href="Libs/bootstrap.min.css" rel="stylesheet" />
    <script src="Libs/bootstrap.min.js"></script>
    <script src="Libs/jquery-3.6.4.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <!-- Header  -->

        <header class="header fixed-top">
            <nav class="navbar navbar-expand-lg w-100 px-0 " style="background-color: #fff">
                <div class="container px-0">
                    <div class="row py-2 w-100 mx-0">
                        <div class="col-md-2 px-0 py-2">
                            <asp:Literal ID="ltrLoGo" runat="server"></asp:Literal>
                        </div>

                        <!-- Menu -->
                        <div class="col-md-5 py-3">
                            <div>
                                <button style="background-color: #fff" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                                    <i class="fa fa-bars" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="collapse navbar-collapse menu-nav" id="navbarToggler">
                                <ul class="navbar-nav ">
                                    <asp:Literal ID="ltrMenu" runat="server"></asp:Literal>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 input-group py-2 px-0" onsubmit="javascript:TimKiemSP()">
                            <input class="form-control py-2 border-info" type="search" placeholder="Từ khóa tìm kiếm" id="search-input" />
                            <span class="input-group-append">
                                <asp:LinkButton ID="btnSearch" class="btn btn-outline-secondary border-info text-white" runat="server" href="javascript:TimKiemSP()" style="background-color: #2E4AE8;">
                                    <i class="fa fa-search"></i>
                                </asp:LinkButton>
                            </span>
                            <%--<div class="input-group">
                                <div class="form-outline" onsubmit="javascript:TimKiemSP()">
                                    <input type="search" id="search-input" class="form-control py-2" onsubmit="javascript:TimKiemSP()"/>                                   
                                <%--</div>
                                <asp:LinkButton ID="LinkButton1" class="btn btn-primary" type="submit" href="javascript:TimKiemSP()" runat="server">
                                    <i class="fas fa-search"></i>
                                </asp:LinkButton>                                
                            </div>--%>
                        </div>
                        <div class="col-md-1 px-0 text-right py-3">
                            <a href="/Default.aspx?uc=prd&suc=car" style="color: #2E4AE8">
                                <i class="fa-solid fa-cart-shopping" style="font-size: 25px"></i>
                                <span id="SLSanPhamGioHang" style="color: #000">0</span>
                            </a>
                        </div>
                        <div class="col-md-1 text-right py-1">
                            <ul class="navbar-nav" style="font-size: 25px">
                                <!-- Dropdown -->
                                <li class="nav-item dropdown">
                                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                                        <i class="fa-regular fa-user"></i>
                                    </a>
                                    <div class="dropdown-menu">
                                        <asp:PlaceHolder ID="plChuaDangNhap" runat="server">
                                            <a class="dropdown-item" href="Default.aspx?uc=mem&suc=log">Đăng nhập</a>
                                            <a class="dropdown-item" href="Default.aspx?uc=mem&suc=reg">Đăng ký</a>
                                        </asp:PlaceHolder>
                                        <asp:PlaceHolder ID="plDaDangNhap" runat="server" Visible="false">
                                            <a class="dropdown-item">
                                                <asp:Literal ID="ltrTenDangNhapKH" runat="server"></asp:Literal></a>
                                            <asp:LinkButton class="dropdown-item" ID="btnDangXuat" runat="server" CausesValidation="false" OnClick="btnDangXuat_Click">Đăng xuất</asp:LinkButton>
                                        </asp:PlaceHolder>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <div class="spacer">
            &nbsp;
        </div>
        <!-- Menu -->

        <!-- Trang Chính -->
        <section class="main">
            <div class="container">

                <!-- Phần nội dung hiển thị -->
                <div class="row px-0 d-flex">
                    <!-- Cột trái -->
                    <div class="colLeft col-md-3 pl-0 pr-3 float-md-left">

                        <!-- Phần danh mục sản phẩm -->
                        <asp:PlaceHolder ID="plDanhMucSanPham" runat="server">
                            <div class="product-category px-0 pb-0 mb-3">
                                <p class="mx-0 my-0 py-3 text-center">DANH MỤC SẢN PHẨM</p>
                                <ul class="border px-0 py-0 mb-0">
                                    <asp:Literal ID="ltrDanhMucSanPham" runat="server"></asp:Literal>
                                </ul>
                            </div>
                        </asp:PlaceHolder>
                        <!-- Phần danh mục tin tức -->
                        <asp:PlaceHolder ID="plDanhMucTinTuc" runat="server" Visible="false">
                            <div class="product-category px-0 pb-0 mb-3">
                                <p class="mx-0 my-0 py-3 text-center">DANH MỤC TIN TỨC</p>
                                <ul class="product-category border px-0 py-0 mb-0">
                                    <asp:Literal ID="ltrDanhMucTinTuc" runat="server"></asp:Literal>
                                </ul>
                            </div>
                        </asp:PlaceHolder>

                        <!-- Phần tư vấn khách hàng -->
                        <div class="support px-0 py-0 mb-3">
                            <ul class="px-0 py-0 mb-0">
                                <li>
                                    <img class="img-fluid" style="width: 100%;" src="IMG/Logo/tuvan3.png" /></li>
                                <li class="py-2 support-attr" style="font-size: 18px; border-bottom: 1px solid #fff;">HỖ TRỢ TRỰC TUYẾN
                                </li>
                                <li class="support-info p-3">
                                    <b>Huyền Trang - 039.2584.416</b>
                                </li>
                                <li class="support-info p-3">
                                    <b>Số hotline - 1900.6903</b>
                                </li>
                                <li class="support-info p-3 mb-0">
                                    <b>Thời gian làm việc - 24/24</b>
                                </li>
                            </ul>
                        </div>

                        <!-- Phần thông tin thanh toán -->
                        <div class="transaction px-0 mb-3 pb-0">
                            <ul class="px-0 pb-0">
                                <li class="transaction-head py-2 text-center">THÔNG TIN GIAO DỊCH</li>

                                <li class="transaction-info px-3 py-3 mb-0">
                                    <p class="transaction-img">
                                        <img class="img-fluid" src="IMG/logo/logo-mbbanksd.png" />
                                    </p>
                                    <p class="transaction-attr">Chi nhánh Long Biên - Hà Nội</p>
                                    <p class="transaction-val">STK: 0400119101997</p>
                                    <p class="transaction-val">Chủ TK: Nguyễn Đức Cường</p>
                                </li>
                                <li class="transaction-info px-3 py-3 mb-0">
                                    <p class="transaction-img">
                                        <img class="img-fluid" src="IMG/logo/Logo_Vietinbank.png" />
                                    </p>
                                    <p class="transaction-attr">Chi nhánh Long Biên - Hà Nội</p>
                                    <p class="transaction-val">STK: 100877498061</p>
                                    <p class="transaction-val">Chủ TK: Nguyễn Đức Cường</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!-- Cột phải -->
                    <div class="colRight col-md-9 p-3">
                        <uc1:DisplayLoadControl runat="server" ID="DisplayLoadControl" />
                    </div>
                </div>
            </div>
        </section>

        <!-- Phần Footer -->
        <section class="footer mt-3">
            <div class="container">
                <div class="row footer-content px-0">
                    <div class="col-md-4 py-3">
                        <div class="py-2">
                            <img src="IMG/Logo/logo_footter.png" class="img-fluid w-75" alt="#" />
                        </div>
                        <div class="py-2 text-white font-weight-bold">
                            THEO DÕI CHÚNG TÔI
                        </div>
                        <div class="py-2 text-white font-weight-bold">
                            <img src="IMG/Logo/face.png" class="img-fluid" alt="#" />
                            <img src="IMG/Logo/tiktok.png" class="img-fluid" alt="#" />
                        </div>
                        <div class="py-2">
                            <a href="http://online.gov.vn/(X(1)S(u3se3rdsz51hpj1ws3a3iv4t))/Home/WebDetails/50028?AspxAutoDetectCookieSupport=1" target="_blank" class="congthuong" title="Đã đăng ký với Bộ Công Thương">
                                <img src="https://trungtran.vn/templates/default/images/bct.svg" width="139" height="53" alt="images" class="img-responsive">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 py-3 pr-5 text-justify">
                        <div class="py-2 text-white font-weight-bold">
                            THÔNG TIN LIÊN HỆ
                        </div>
                        <div class="py-2 text-white">
                            Địa chỉ tên miền: trungtran.somee.com
                        </div>
                        <div class="py-2 text-white">
                            Tên Doanh nghiệp: Công ty cổ phần thương mại dịch vụ Trung Trần
                        </div>
                        <div class="py-2 text-white">
                            Trụ sở Doanh nghiệp: số 65 phố Hàm Tử Quan, phường Phúc Tân, quận Hoàn Kiếm, tp Hà Nội, Việt Nam
                        </div>
                    </div>
                    <div class="col-md-4 py-3">
                        <iframe class="img-fluid rounded w-100 h-100" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.002861830294!2d105.85715051482062!3d21.032571493024285!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abc2331bf80f%3A0x943d41f1da170be2!2zUXXDoW4gU2F5!5e0!3m2!1svi!2s!4v1681118685444!5m2!1svi!2s" width="600" height="450" style="border: 0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                    </div>
                </div>
                <hr />
                <div class="row copy-right font-weight-bold text-white text-center d-block py-3">
                    Copyright &copy 2023 by Nguyễn Đức Cường. All Rights Reserved.                   
                </div>
        </section>
        <div>
        </div>
        <!-- Code JS -->

        <script src="Libs/jquery-3.4.1.slim.min.js"></script>
        <script src="Libs/bootstrap.bundle.min.js"></script>
        <script src="Libs/jquery-3.6.4.min.js"></script>
        <script type="text/javascript">
            function LaySoLuongSP() {
                $.post("CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
                    {
                        "ThaoTac": "LayTongSoLuongSP",
                    },
                    function (data, status) {
                        if (data != null) {
                            $("#SLSanPhamGioHang").html(data);
                        }
                    }
                );
            }

            function TimKiemSP() {
                var key = $("#search-input").val();
                window.location.href = "/Default.aspx?uc=prd&suc=search&key=" + key, true;
                return false;
            }

            $(document).ready(function () {
                LaySoLuongSP();
            });
        </script>
    </form>
</body>
</html>
