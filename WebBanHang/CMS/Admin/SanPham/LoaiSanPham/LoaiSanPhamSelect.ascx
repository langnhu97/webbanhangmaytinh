﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoaiSanPhamSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.SanPham.LoaiSanPham.LoaiSanPhamSelect" %>
<div class="head">
    Các loại sản phẩm đã tạo
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=prd&suc=typ&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colName">Tên Danh Mục</th>
            <th class="colNote">Ghi Chú</th>
            <th class="colTool">Công cụ</th>            
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>
         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc muốn xóa danh mục này?")) {
            $.post("CMS/Admin/SanPham/LoaiSanPham/AJAX/LoaiSanPham.aspx",
                {
                    "man": "del",
                    "id": id
                },
                function (data, status) {
                    if (data == "1") //Nếu thực hiện thành công data -> 1, ngược lại data ->0
                    {
                        $("#rowId_" + id).slideUp();
                    } else {
                        alert("Xóa thất bại");
                    }
                }
            );
        }        
    }
</script>