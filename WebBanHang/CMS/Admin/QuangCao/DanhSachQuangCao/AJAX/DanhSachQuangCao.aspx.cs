﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.QuangCao.DanhSachQuangCao.AJAX
{
    public partial class DanhSachQuangCao : System.Web.UI.Page
    {
        QuangCaoDAL dal = new QuangCaoDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            string man = "";
            if (Request.Params["man"] != null)
            {
                man = Request.Params["man"];
            }

            switch (man)
            {
                case "del":
                    XoaDanhMuc();
                    break;
                default:
                    break;
            }
        }

        private void XoaDanhMuc()
        {
            //Lấy id
            if (Request.Params["id"] != null)
            {
                id = Convert.ToInt32(Request.Params["id"]);
            }

            //Code xóa trả về "1"-Thành công, "0"- thất bại
            try
            {
                dal.QuangCao_Delete(id);
                Response.Write("1");
            }
            catch (Exception)
            {
                Response.Write("0");
            }
        }
    }
}