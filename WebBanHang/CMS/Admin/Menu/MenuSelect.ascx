﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MenuSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.Menu.MenuSelect" %>
<div class="head">
    Danh sách menu
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=men&suc=lst&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colName">Tên Menu</th>
            <th class="colNote">Liên kết</th>
            <th class="colOrd">Thứ tự</th>
            <th class="colTool">Công cụ</th>
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc muốn xóa danh mục này?")) {           
            $.post("/CMS/Admin/Menu/AJAX/Menu.aspx",
                {
                    "man": "del",
                    "id": id
                },
                function (data, status) {
                    if (data == "1") //Nếu thực hiện thành công data -> 1, ngược lại data ->0
                    {
                        $("#rowId_" + id).slideUp();
                    } else {
                        alert("Xóa thất bại");
                    }
                }
            );
        }        
    }
</script>