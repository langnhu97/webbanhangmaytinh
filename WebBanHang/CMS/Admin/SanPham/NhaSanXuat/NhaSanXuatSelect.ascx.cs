﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.NhaSanXuat
{
    public partial class NhaSanXuatSelect : System.Web.UI.UserControl
    {
        NhaSanXuatDAL dal = new NhaSanXuatDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhMuc();
            }
        }

        private void LayDanhMuc()
        {
            DataTable dt = new DataTable();
            dt = dal.NhaSanXuat_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaNSX"] + @"'>
                     <td class='colId'>" + dt.Rows[i]["MaNSX"] + @"</td>   
                     <td class='colName'>" + dt.Rows[i]["TenNSX"] + @"</td>       
                     <td class='colNote'>" + dt.Rows[i]["GhiChu"] + @"</td>       
                     <td class='colTool'>
                         <a href = 'Admin.aspx?uc=prd&suc=man&man=upd&id=" + dt.Rows[i]["MaNSX"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaNSX"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>                
                </tr>
            ";

            }
        }
    }
}