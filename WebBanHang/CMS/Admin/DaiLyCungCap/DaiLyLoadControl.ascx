﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DaiLyLoadControl.ascx.cs" Inherits="WebBanHang.CMS.Admin.DaiLyCungCap.DaiLyLoadControl" %>
<div class="container">
     <div class="colLeft">  
        <div class="head">
            Quản lý
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("sup","lst","") %>" href="Admin.aspx?uc=sup&suc=lst"title="Danh sách đại lý">Danh sách đại lý</a></li>
        </ul>
        <div class="head">
            Thêm mới
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("sup","lst","add") %>" href="Admin.aspx?uc=sup&suc=lst&man=add" title="Thêm mới đại lý">Đại lý</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="colRight">
        <asp:PlaceHolder ID="plControl" runat="server"></asp:PlaceHolder>
    </div>
</div>
