﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DanhSachQuangCaoSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.QuangCao.DanhSachQuangCao.DanhSachQuangCaoSelect" %>
<div class="head">
    <div class="fl" style="padding-right: 30px;">Danh sách quảng cáo</div>
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=adv&suc=lst&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>    
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colGroup">Nhóm quảng cáo</th>   
            <th class="colName">Tên quảng cáo</th>                   
            <th class="colImg">Hình ảnh</th>            
            <th class="colLink">Liên kết</th>  
            <th class="colOrd">Thứ tự</th>            
            <th class="colTool">Công cụ</th>
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc xóa bỏ mục này?")) {
            
            $.post("/CMS/Admin/QuangCao/DanhSachQuangCao/AJAX/DanhSachQuangCao.aspx",
                {
                    "man": "del",
                    "id" : id
                },
                function (data, status) {
                    if (data == "1") {
                        $("#rowId_" + id).slideUp();
                    } else {
                        alert("Xóa thất bại");
                    }
                }

            );
        }
    }

</script>
