﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.DanhMucSanPham.Ajax
{
    public partial class DanhMuc : System.Web.UI.Page
    {
        DanhMucDAL dal = new DanhMucDAL();
        private string man =""; // manipulate:  Đánh dấu thao tác(được gửi qua Request của phương thức Post-Ajax)
        protected void Page_Load(object sender, EventArgs e)
        {
            //Code kiểm tra đăng nhập -> Sau đó thực hiện các thao tác ở dưới
            if (Session["DangNhap"] != null && Session["DangNhap"].ToString() == "1")
            {

            }
            else
            {
                Response.Redirect("Login.aspx");
            }


            if (Request.Params["man"] != null)
            {
                man = Request.Params["man"];
            }

            switch (man)
            {
                case "del":
                    XoaDanhMuc();
                    break;

            }
        }

        private void XoaDanhMuc()
        {
            string MaDM = "";
            if (Request.Params["MaDM"] != null)
            {
                MaDM = Request.Params["MaDM"];
            }

            //Thực hiện code xóa
            //Trả về 1-thành công. 0-Không thành công
            try
            {
                dal.DanhMuc_Delete(Convert.ToInt32(MaDM));
                Response.Write("1");
            }
            catch (Exception)
            {
                Response.Write("0");
            }            
        }
    }
}