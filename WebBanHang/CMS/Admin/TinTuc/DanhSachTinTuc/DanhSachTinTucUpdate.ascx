﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DanhSachTinTucUpdate.ascx.cs" Inherits="WebBanHang.CMS.Admin.TinTuc.TinTucUpdate" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET"  TagPrefix="CKEditor" %>
<div class="head">  
    Chỉnh sửa tin tức
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">
        <div class="attr">Danh mục tin tức</div>
        <div class="val">
            <asp:DropDownList CssClass="w250" ID="ddlDMTT" runat="server"></asp:DropDownList></div>
        </div>
    </div>
    <div class="field">
        <div class="attr">Tiêu đề</div>
        <div class="val">
            <asp:TextBox ID="txtTieuDe" runat="server"></asp:TextBox></div>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="***" SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTieuDe"></asp:RequiredFieldValidator>
    </div>
    <div class="field">
        <div class="attr">Mô tả</div>
        <div class="val">
            <asp:TextBox ID="txtMoTa" TextMode="MultiLine" CssClass="w250" runat="server"></asp:TextBox></div>
    </div>
    <div class="field">
        <div class="attr">Lượt xem</div>
        <div class="val">
            <asp:TextBox ID="txtLuotXem" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtLuotXem"
                SetFocusOnError="true" Display="Dynamic" ForeColor="red" ValidationExpression="(\d)*" ErrorMessage="Phải nhập số"></asp:RegularExpressionValidator>
        </div>
    </div>
    <div class="field">
        <div class="attr">Ngày đăng</div>
        <div class="val">
            <asp:TextBox ID="txtNgayDang" TextMode="Date" runat="server"></asp:TextBox>
        </div>
    </div>

    <div class="field">
        <div class="attr">Hình ảnh</div>
        <div class="val">
            <div>
                <asp:Literal ID="ltrAnh" runat="server"></asp:Literal>
                <asp:HiddenField ID="hdfAnh" runat="server" />
            </div>
            <asp:FileUpload ID="flHinhAnh" runat="server" /></div>
    </div>
    <div class="field">
        <div class="attr">Chi tiết</div>
        <div class="val">
            <%--<asp:TextBox ID="txtChiTiet" runat="server"></asp:TextBox></div>--%>
             <CKEditor:CKEditorControl  ID="txtChiTiet" runat="server" Text='<%# Bind("content") %>' ></CKEditor:CKEditorControl>
    </div>
   
    <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn" ID="btnChinhSua" runat="server" Text="Chỉnh sửa" OnClick="btnChinhSua_Click" />
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click" />
        </div>
    </div>
</div>
