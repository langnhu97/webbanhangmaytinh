﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NhaSanXuatSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.SanPham.NhaSanXuat.NhaSanXuatSelect" %>

<div class="head">
    Các nhà sản xuất đã tạo
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=prd&suc=man&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colName">Tên Danh Mục</th>
            <th class="colNote">Ghi Chú</th>
            <th class="colTool">Công cụ</th>            
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>
         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc xóa danh mục này?")) {

            $.post("CMS/Admin/SanPham/NhaSanXuat/AJAX/NhaSanXuat.aspx",
                {
                    "man": "del",
                    "id":  id
                },
                function (data, status) {
                    if (data == "1") //Nếu thực hiện thành công data -> 1, ngược lại data ->0
                    {
                        $("#rowId_" + id).slideUp();
                        alert("Xóa thành công");
                    } else {
                        alert("Xóa thất bại");
                    }
                }
            );
        }
    }
</script>