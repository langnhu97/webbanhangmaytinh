﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NhaSanXuatInsert.ascx.cs" Inherits="WebBanHang.CMS.Admin.SanPham.NhaSanXuat.NhaSanXuatInsert" %>
<div class="head">
    Thêm mới nhà sản xuất
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">        
        <div class="attr">Tên nhà sản xuất</div>
        <div class="val">
            <asp:TextBox ID="txtTenNSX" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="***" 
                SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenNSX"></asp:RequiredFieldValidator>
        </div>        
    </div>    
    <div class="field">
        <div class="attr">Ghi chú</div>
        <div class="val"><asp:TextBox ID="txtGhiChu" class="txt" runat="server"></asp:TextBox></div>
    </div>   
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:CheckBox ID="ckTiepTuc" Text="Tiếp tục thêm mới sau khi tạo nhà sản xuất này" runat="server" />
        </div>
    </div>
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn btnThemMoi" ID="btnThemMoi" runat="server" Text="Thêm mới" OnClick="btnThemMoi_Click" />
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click"/>
        </div>
    </div>
</div>