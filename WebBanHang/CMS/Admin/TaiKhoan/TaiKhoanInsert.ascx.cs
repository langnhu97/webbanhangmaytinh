﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TaiKhoan
{
    public partial class TaiKhoanInsert : System.Web.UI.UserControl
    {
        TaiKhoanDAL _taikhoanDAL = new TaiKhoanDAL();
        QuyenDangNhapDAL _quyenDangNhapDAL = new QuyenDangNhapDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhSachQuyen();
            }
        }

        private void LayDanhSachQuyen()
        {
            DataTable dt = _quyenDangNhapDAL.QuyenDangNhap_Select();
            ddlQuyen.DataSource = dt;
            ddlQuyen.DataValueField = "MaQuyen";
            ddlQuyen.DataTextField = "TenQuyen";
            ddlQuyen.DataBind();                        
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            DateTime ngaySinh = txtNgaySinh.Text != "" ? DateTime.Parse(txtNgaySinh.Text) : DateTime.Now;

            try
            {
                _taikhoanDAL.TaiKhoan_Insert(txtTenDangNhap.Text.Trim(), MaHoa.GetMD5(txtMatKhau.Text.Trim()),
                        txtEmailDK.Text.Trim(), txtDiaChiDK.Text.Trim(), txtHoTen.Text.Trim(),
                        ngaySinh, Convert.ToInt32(ddlQuyen.SelectedValue));
                lblThongBao.Text = "<div class='successNoti'>Đã tạo tài khoản " + txtTenDangNhap.Text.Trim() + "</div>";

                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=acc&suc=lst");
                }
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa tạo được nhân viên </div>";
            }
        }

        private void ResetForm()
        {
            txtTenDangNhap.Text = "";
            txtMatKhau.Text = "";
            txtEmailDK.Text = "";
            txtDiaChiDK.Text = "";
            txtHoTen.Text = "";
            txtNgaySinh.Text = "";
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}