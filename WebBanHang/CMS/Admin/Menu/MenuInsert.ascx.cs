﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.Menu
{
    public partial class MenuInsert : System.Web.UI.UserControl
    {
        MenuDAL dal = new MenuDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.Menu_Insert(txtTenMenu.Text.Trim(), txtLienKet.Text.Trim(), Convert.ToInt32(txtThuTu.Text));
                lblThongBao.Text = "<div class='successNoti'>Đã tạo " + txtTenMenu.Text + "</div>";
                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=men&suc=lst");
                }

            }
            catch (Exception)
            {
                lblThongBao.Text = "Tạo thất bại";
                throw;
            }
        }

        private void ResetForm()
        {
            txtTenMenu.Text = "";
            txtLienKet.Text = "";
            txtThuTu.Text = "";
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}