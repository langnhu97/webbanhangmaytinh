﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.NhaSanXuat.AJAX
{
    public partial class NhaSanXuat : System.Web.UI.Page
    {
        NhaSanXuatDAL dal = new NhaSanXuatDAL();
        string man = ""; //Đánh dấu thao tác (manipulate) được truyền dưới dạng tham số qua XMLHttpRequest

        protected void Page_Load(object sender, EventArgs e)
        {
            //Code kiểm tra đăng nhập
            if (Session["DangNhap"] != null && Session["DangNhap"].ToString() == "1")
            {

            }
            else
            {
                Response.Redirect("Login.aspx");
            }

            if (Request.Params["man"] != null)
            {
                man = Request.Params["man"];
            }

            switch (man)
            {
                case "del":
                    XoaDanhMuc();
                    break;
            }
        }

        private void XoaDanhMuc()
        {
            string id = "";
            if (Request.Params["id"] != null)
            {
                id = Request.Params["id"];
            }

            //Thực hiện code xóa và trả về 1 - thành công, 0 - thất bại
            try
            {
                dal.NhaSanXuat_Delete(Convert.ToInt32(id));
                Response.Write("1");
            }
            catch (Exception)
            {
                Response.Write("0");
            }   
        }
    }
}