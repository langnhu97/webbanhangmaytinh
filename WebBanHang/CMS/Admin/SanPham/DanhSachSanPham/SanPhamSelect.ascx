﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SanPhamSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.SanPham.DanhSachSanPham.SanPhamSelect" %>
<div class="head">
    <div class="fl" style="padding-right: 30px;">Các sản phẩm đã tạo</div>
    <div class="fl">
        <asp:TextBox ID="txtTimKiem" CssClass="txtTimKiem" runat="server" style="width: 400px" OnTextChanged="txtTimKiem_TextChanged" placeholder="Tên sản phẩm cần tìm"></asp:TextBox>
        <asp:Button ID="btnTimKiem" CssClass="btnTimKiem" runat="server" Type="Submit" Text="Tìm kiếm" OnClick="Button1_Click"/>
    </div>
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=prd&suc=lst&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
    
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colName">Tên sản phẩm</th>
<%--            <th class="colCat">Loại sản phẩm</th>            
            <th class="colSup">Nhà sản xuất</th>
            <th class="colDM">Danh mục</th>            
            <th class="colDis">Khuyến mãi</th>   --%>         
            <th class="colGua">Bảo hành</th>            
            <th class="colQua">Số lượng</th>            
            <th class="colPar">Thông số</th>            
            <th class="colCos">Giá bán</th>            
            <th class="colImg">Hình ảnh</th>            
            <th class="colTool">Công cụ</th>
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc muốn xóa sản phẩm này?")) {
            $.post("CMS/Admin/SanPham/DanhSachSanPham/AJAX/SanPham.aspx",
                {
                    "man": "del",
                    "id": id
                },
                function (data, status) {
                    if (data == "1") //Nếu thực hiện thành công data -> 1, ngược lại data ->0
                    {
                        $("#rowId_" + id).slideUp();
                    } else {
                        alert("Xóa thất bại");
                    }
                }
            );
        }        
    }
</script>