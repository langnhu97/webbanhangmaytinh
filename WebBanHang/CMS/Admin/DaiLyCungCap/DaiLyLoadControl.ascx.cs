﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin.DaiLyCungCap
{
    public partial class DaiLyLoadControl : System.Web.UI.UserControl
    {
        string suc = "";
        string man = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["suc"] != null)
            {
                suc = Request.QueryString["suc"];
            }
            if (Request.QueryString["man"] != null)
            {
                man = Request.QueryString["man"];
            }            

            switch (man)
            {
                case "add":
                    plControl.Controls.Add(LoadControl("DaiLyInsert.ascx"));
                    break;
                case "upd":
                    plControl.Controls.Add(LoadControl("DaiLyUpdate.ascx"));
                    break;
                case "del":
                    plControl.Controls.Add(LoadControl("DaiLyDelete.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("DaiLySelect.ascx"));
                    break;
            }
        }

        protected string DanhDauThaoTac(string ucName, string sucName, string manName)
        {
            string s = "";

            //Lấy giá trị queryString trong modul
            string uc = String.Empty;
            string suc = String.Empty;
            string man = String.Empty;
            if (Request.QueryString["uc"] != null)
            {
                uc = Request.QueryString["uc"];
            }
            if (Request.QueryString["suc"] != null)
            {
                suc = Request.QueryString["suc"];
            }
            if (Request.QueryString["man"] != null)
            {
                man = Request.QueryString["man"];
            }

            //So sánh queryString bằng tên uc + suc + man truyền vào thì trả về current --> Đánh dấu là menu hiện tại
            if (uc == ucName && suc == sucName && man == manName)
                s = "current";

            return s;
        }
    }
}