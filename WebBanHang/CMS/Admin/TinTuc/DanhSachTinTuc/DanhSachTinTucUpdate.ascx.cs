﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TinTuc
{
    public partial class TinTucUpdate : System.Web.UI.UserControl
    {
        DanhMucTinTucDAL danhMucTinTucDAL = new DanhMucTinTucDAL();
        TinTucDAL tinTucDAL = new TinTucDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhMucTinTuc();
            }

            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }
            HienThiThongTin(id);
        }

        private void HienThiThongTin(int id)
        {
            DataTable dt = tinTucDAL.TinTuc_SelectById(id);
            if(dt.Rows.Count > 0)
            {
                txtTieuDe.Text = dt.Rows[0]["TieuDe"].ToString();
                ddlDMTT.SelectedValue = dt.Rows[0]["MaDMTT"].ToString();
                txtMoTa.Text = dt.Rows[0]["MoTa"].ToString();
                txtLuotXem.Text = dt.Rows[0]["LuotXem"].ToString();
                txtNgayDang.Text = DateTime.Parse(dt.Rows[0]["NgayDang"].ToString()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
                ltrAnh.Text += @"<img class='img-load' src='/IMG/TinTuc/" + dt.Rows[0]["AnhDaiDien"] + @"'>";
                hdfAnh.Value = dt.Rows[0]["AnhDaiDien"].ToString();
                txtChiTiet.Text = dt.Rows[0]["ChiTiet"].ToString();
            }
        }

        private void LayDanhMucTinTuc()
        {
            DataTable dt = danhMucTinTucDAL.DanhMucTinTuc_Select();
            ddlDMTT.DataSource = dt;
            ddlDMTT.DataValueField = "MaDMTT";
            ddlDMTT.DataTextField = "TenDMTT";
            ddlDMTT.DataBind();
        }

        protected void btnChinhSua_Click(object sender, EventArgs e)
        {
            try
            {
                string tenAnh = "";
                if (flHinhAnh.FileContent.Length > 0)
                {
                    if (flHinhAnh.FileName.EndsWith(".jpg") || flHinhAnh.FileName.EndsWith(".png")
                        || flHinhAnh.FileName.EndsWith(".gif") || flHinhAnh.FileName.EndsWith(".jpeg")                        
                        || flHinhAnh.FileName.EndsWith(".psd") || flHinhAnh.FileName.EndsWith(".pdf"))
                    {
                        flHinhAnh.SaveAs(Server.MapPath("IMG/TinTuc/") + flHinhAnh.FileName);
                    }
                    tenAnh = flHinhAnh.FileName;
                }
                else
                {
                    tenAnh = hdfAnh.Value;
                }
                int maDMTT = Convert.ToInt32(ddlDMTT.SelectedValue) > 0 ? Convert.ToInt32(ddlDMTT.SelectedValue) : 0;
                string tieude = txtTieuDe.Text.Trim();
                string mota = txtMoTa.Text.Trim();
                int luotxem = Convert.ToInt32(txtLuotXem.Text.Trim());
                DateTime ngaydang = DateTime.Parse(txtNgayDang.Text);
                string chitiet = txtChiTiet.Text;

                tinTucDAL.TinTuc_Update(id, maDMTT, tieude, mota, luotxem, ngaydang, tenAnh, chitiet);
                Response.Redirect("Admin.aspx?uc=new&suc=lst");
            }
            catch (Exception)
            {
                lblThongBao.Text = "Chỉnh sửa thất bại";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            ddlDMTT.SelectedIndex = -1;
            txtTieuDe.Text = "";
            txtMoTa.Text = "";
            txtLuotXem.Text = "";
            txtNgayDang.Text = "";
            txtChiTiet.Text = "";
            flHinhAnh.FileContent.Flush();
        }
    }
}