﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoaiSanPhamInsert.ascx.cs" Inherits="WebBanHang.CMS.Admin.SanPham.LoaiSanPham.LoaiSanPhamInsert" %>
<div class="head">
    Thêm mới loại sản phẩm
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">
        <div class="attr">Tên loại sản phẩm</div>
        <div class="val">
            <asp:TextBox ID="txtTenLoaiSP" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="***"
                SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenLoaiSP"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="field">
        <div class="attr">Ghi chú</div>
        <div class="val">
            <asp:TextBox ID="txtGhiChu" class="txt" runat="server"></asp:TextBox>
        </div>

    </div>
    <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:CheckBox ID="ckTiepTuc" Text="Tiếp tục thêm mới sau khi tạo" runat="server" />
        </div>
    </div>
    <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn btnThemMoi" ID="btnThemMoi" runat="server" Text="Thêm mới" OnClick="btnThemMoi_Click" />
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click" />
        </div>
    </div>
</div>