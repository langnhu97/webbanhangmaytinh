﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Web.SessionState;
using System.Web.UI.HtmlControls;
using WebBanHang.App_Code;

namespace WebApplication2
{
    public partial class vpc_dr : System.Web.UI.Page
    {
        DonDatHangDAL DonDatHangDAL = new DonDatHangDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            string SECURE_SECRET = OnepayQuocTeCode.SECURE_SECRET;//Hòa: cần thay bằng mã thật từ app_code          
            string hashvalidateResult = "";
            // Khoi tao lop thu vien
            VPCRequest conn = new VPCRequest("http://onepay.vn");
            conn.SetSecureSecret(SECURE_SECRET);
            // Xu ly tham so tra ve va kiem tra chuoi du lieu ma hoa
            hashvalidateResult = conn.Process3PartyResponse(Request.QueryString);

            // Lay gia tri tham so tra ve tu cong thanh toan
            String vpc_TxnResponseCode = conn.GetResultField("vpc_TxnResponseCode", "Unknown");
            string amount = conn.GetResultField("vpc_Amount", "Unknown");
            string localed = conn.GetResultField("vpc_Locale", "Unknown");
            string command = conn.GetResultField("vpc_Command", "Unknown");
            string version = conn.GetResultField("vpc_Version", "Unknown");
            string cardType = conn.GetResultField("vpc_Card", "Unknown");
            string orderInfo = conn.GetResultField("vpc_OrderInfo", "Unknown");
            string merchantID = conn.GetResultField("vpc_Merchant", "Unknown");
            string authorizeID = conn.GetResultField("vpc_AuthorizeId", "Unknown");
            string merchTxnRef = conn.GetResultField("vpc_MerchTxnRef", "Unknown");
            string transactionNo = conn.GetResultField("vpc_TransactionNo", "Unknown");
            string acqResponseCode = conn.GetResultField("vpc_AcqResponseCode", "Unknown");
            string txnResponseCode = vpc_TxnResponseCode;
            string message = conn.GetResultField("vpc_Message", "Unknown");

            string mathanhtoantructuyen = merchTxnRef;

            // Sua lai ham check chuoi ma hoa du lieu            
            if (hashvalidateResult == "CORRECTED" && txnResponseCode.Trim() == "0")
            {
                //vpc_Result.Text = "Transaction was paid successful";

                #region Cập nhật vào db theo mã đơn hàng

                DataTable dt = DonDatHangDAL.DonDatHang_SelectByMaThanhToan(mathanhtoantructuyen);
                //Nếu tồn tại đơn hàng này --> cập nhật lại trạng thái
                if (dt.Rows.Count > 0)
                {
                    string tinhTrangDonHang = "1";//1: có nghĩa là thành toán thành công
                    DonDatHangDAL.DonDatHang_Update(Convert.ToInt32(dt.Rows[0]["MaHD"]), Convert.ToDateTime(dt.Rows[0]["NgayDatHang"].ToString()),
                        Convert.ToSingle(dt.Rows[0]["TongTien"]), tinhTrangDonHang, Convert.ToInt32(dt.Rows[0]["MaKH"]),
                        dt.Rows[0]["TenKH"].ToString(), dt.Rows[0]["sdtKH"].ToString(), dt.Rows[0]["EmailKH"].ToString());
                }
                #endregion

                Response.Write("<div class='result'>Đã thanh toán thành công</div>");
            }
            else if (hashvalidateResult == "INVALIDATED" && txnResponseCode.Trim() == "0")
            {
                #region Cập nhật vào db theo mã đơn hàng

                DataTable dt = DonDatHangDAL.DonDatHang_SelectByMaThanhToan(mathanhtoantructuyen);
                //Nếu tồn tại đơn hàng này --> cập nhật lại trạng thái
                if (dt.Rows.Count > 0)
                {
                    string tinhTrangDonHang = "0";//1: có nghĩa là thành toán chưa thành công
                    DonDatHangDAL.DonDatHang_Update(Convert.ToInt32(dt.Rows[0]["MaHD"]), Convert.ToDateTime(dt.Rows[0]["NgayDatHang"].ToString()),
                        Convert.ToSingle(dt.Rows[0]["TongTien"]), tinhTrangDonHang, Convert.ToInt32(dt.Rows[0]["MaKH"]),
                        dt.Rows[0]["TenKH"].ToString(), dt.Rows[0]["sdtKH"].ToString(), dt.Rows[0]["EmailKH"].ToString());
                }
                #endregion

                //vpc_Result.Text = "Transaction is pending";
                Response.Write("Error description : " + message + "<br/>");
                Response.Write("<div class='result'>Thanh toán đang chờ</div>");
            }
            else
            {
                #region Cập nhật vào db theo mã đơn hàng

                DataTable dt = DonDatHangDAL.DonDatHang_SelectByMaThanhToan(mathanhtoantructuyen);
                //Nếu tồn tại đơn hàng này --> cập nhật lại trạng thái
                if (dt.Rows.Count > 0)
                {
                    string tinhTrangDonHang = "0";//1: có nghĩa là thành toán chưa thành công
                    DonDatHangDAL.DonDatHang_Update(Convert.ToInt32(dt.Rows[0]["MaHD"]), Convert.ToDateTime(dt.Rows[0]["NgayDatHang"].ToString()),
                        Convert.ToSingle(dt.Rows[0]["TongTien"]), tinhTrangDonHang, Convert.ToInt32(dt.Rows[0]["MaKH"]),
                        dt.Rows[0]["TenKH"].ToString(), dt.Rows[0]["sdtKH"].ToString(), dt.Rows[0]["EmailKH"].ToString());
                }
                #endregion

                //vpc_Result.Text = "Transaction was not paid successful";
                Response.Write("Error description : " + message + "<br/>");
                Response.Write("<div class='result'>Thanh toán không thành công</div>");
            }

        }
        /*
private static string null2unknown(string sInput)
{
    return sInput+" ";

}
public static string ToHexa(byte[] data)
{
    StringBuilder sb = new StringBuilder();
    for (int i = 0; i < data.Length; i++)
        sb.AppendFormat("{0:X2}", data[i]);
    return sb.ToString();
}
public static string DoMD5(string SData)
{
    System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
   System.Text.Encoding encode = System.Text.Encoding.GetEncoding("ISO-8859-1");
   byte[] result1 = md5.ComputeHash(encode.GetBytes(SData));
    string sResult2 = ToHexa(result1);
 //   string sResult2 = HexEncoding.ToString(byteArray)
   return sResult2;

   // System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
    //System.Text.UTF8Encoding encode = new System.Text.UTF8Encoding();
  //  byte[] result1 = md5.ComputeHash(encode.GetBytes(SData));
  //  string sResult2 = ToHexa(result1);
  //  return sResult2;
}
*/
    }
}