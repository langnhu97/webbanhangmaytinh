﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.DanhSachSanPham
{
    public partial class SanPhamSelect : System.Web.UI.UserControl
    {
        SanPhamDAL sanPhamDAL = new SanPhamDAL();
        NhaSanXuatDAL nhaSanXuatDAL = new NhaSanXuatDAL();
        LoaiSanPhamDAL loaiSanPhamDAL = new LoaiSanPhamDAL();
        DanhMucDAL danhMucDAL = new DanhMucDAL();
        KhuyenMaiDAL khuyenMaiDAL = new KhuyenMaiDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhMuc();
            }
        }

        private void LayDanhMuc()
        {
            DataTable dt = new DataTable();
            dt = sanPhamDAL.SanPham_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {              

                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaSP"] + @"'>
                     <td class='colId'>" + dt.Rows[i]["MaSP"] + @"</td>   
                     <td class='colName'>" + dt.Rows[i]["TenSP"] + @"</td>     
                     <td class='colGua'>" + dt.Rows[i]["BaoHanh"] + @"</td>       
                     <td class='colQua'>" + dt.Rows[i]["SoLuong"] + @"</td>       
                     <td class='colPar'>" + dt.Rows[i]["ThongSo"] + @"</td>       
                     <td class='colCos'>" + dt.Rows[i]["GiaBan"] + @"</td>       
                     <td class='colImg'>
                            <img class='img-display' src='/IMG/SanPham/" + dt.Rows[i]["HinhAnh"] + @"'>"
                            + @" <img class='img-hover' src = '/IMG/SanPham/" + dt.Rows[i]["HinhAnh"] + @"' > "
                    + @"</td>       
                     <td class='colTool'>
                         <a href = 'Admin.aspx?uc=prd&suc=lst&man=upd&id=" + dt.Rows[i]["MaSP"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaSP"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>
                </tr>
            ";
            }
        }
        protected void txtTimKiem_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt = sanPhamDAL.SanPham_SelectByName(txtTimKiem.Text.Trim());

            ltrDanhMuc.Text = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaSP"] + @"'>
                     <td class='colId'>" + dt.Rows[i]["MaSP"] + @"</td>   
                     <td class='colName'>" + dt.Rows[i]["TenSP"] + @"</td>    
                     <td class='colGua'>" + dt.Rows[i]["BaoHanh"] + @"</td>       
                     <td class='colQua'>" + dt.Rows[i]["SoLuong"] + @"</td>       
                     <td class='colPar'>" + dt.Rows[i]["ThongSo"] + @"</td>       
                     <td class='colCos'>" + dt.Rows[i]["GiaBan"] + @"</td>       
                     <td class='colImg'>
                            <img class='img-display' src='/IMG/SanPham/" + dt.Rows[i]["HinhAnh"] + @"'>"
                            + @" <img class='img-hover' src = '/IMG/SanPham/" + dt.Rows[i]["HinhAnh"] + @"' > "
                    + @"</td>       
                     <td class='colTool'>
                         <a href = 'Admin.aspx?uc=adv&suc=lst&man=upd&id=" + dt.Rows[i]["MaSP"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaSP"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>
                </tr>
            ";
            }
        }
    }
}