﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TaiKhoan
{
    public partial class TaiKhoanUpdate : System.Web.UI.UserControl
    {
        TaiKhoanDAL _taiKhoanDAL = new TaiKhoanDAL();
        QuyenDangNhapDAL _quyenDangNhapDAL = new QuyenDangNhapDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhSachQuyen();
            }
            HienThiThongTin();
        }

        private void HienThiThongTin()
        {
            string tendangnhap = "";
            if (Request.QueryString["id"] != null)
            {
                tendangnhap = Request.QueryString["id"].ToString();
            }

            DataTable dt = _taiKhoanDAL.TaiKhoan_SelectById(tendangnhap);
            txtTenDangNhap.Text = dt.Rows[0]["TenDangNhap"].ToString();
            txtMatKhau.Text = dt.Rows[0]["MatKhau"].ToString();
            hdfMatKhau.Value = dt.Rows[0]["MatKhau"].ToString();
            txtEmailDK.Text = dt.Rows[0]["EmailDK"].ToString();
            txtDiaChiDK.Text = dt.Rows[0]["DiaChiDK"].ToString();
            txtHoTen.Text = dt.Rows[0]["HoTen"].ToString();
            txtNgaySinh.Text = DateTime.Parse(dt.Rows[0]["NgaySinh"].ToString()).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            ddlQuyen.SelectedValue = dt.Rows[0]["MaQuyen"].ToString();
        }

        private void LayDanhSachQuyen()
        {
            DataTable dt = _quyenDangNhapDAL.QuyenDangNhap_Select();
            ddlQuyen.DataSource = dt;
            ddlQuyen.DataValueField = "MaQuyen";
            ddlQuyen.DataTextField = "TenQuyen";
            ddlQuyen.DataBind();
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                string matkhau = txtMatKhau.Text != "" ? MaHoa.GetMD5(txtMatKhau.Text.Trim()) : hdfMatKhau.Value;
                _taiKhoanDAL.TaiKhoan_Update(txtTenDangNhap.Text.Trim(), matkhau, txtEmailDK.Text.Trim(),
                    txtDiaChiDK.Text.Trim(), txtHoTen.Text.Trim(), DateTime.Parse(txtNgaySinh.Text), Convert.ToInt32(ddlQuyen.SelectedValue));
                Response.Redirect("Admin.aspx?uc=acc&suc=lst");
            }
            catch (Exception)
            {

                lblThongBao.Text = "<div class='successNoti'>Chưa sửa được tài khoản</div>";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenDangNhap.Text = "";
            txtMatKhau.Text = "";
            txtEmailDK.Text = "";
            txtDiaChiDK.Text = "";
            txtHoTen.Text = "";
            txtNgaySinh.Text = "";
        }
    }
}