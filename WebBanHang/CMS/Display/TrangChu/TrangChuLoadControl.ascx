﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TrangChuLoadControl.ascx.cs" Inherits="WebBanHang.CMS.Display.TrangChu.TrangChuLoadControl" %>
<!-- Slide quảng cáo trượt ngang -->
                <div id="demo" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner slide-item">
                        <asp:Literal ID="ltrSlide" runat="server"></asp:Literal>
                        <%--<div class="carousel-item active">
                            <img src="pic/Logo/quangcao.jpeg" class="img-fluid" alt="Los Angeles">
                        </div>
                        <div class="carousel-item">
                            <img src="pic/Logo/quangcao4.png" class="img-fluid" alt="Chicago">
                        </div>
                        <div class="carousel-item">
                            <img src="pic/Logo/quangcao.jpeg" class="img-fluid" alt="New York">
                        </div>--%>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>

                <!-- Danh mục sản phẩm -->
                <div class="product-list mt-4">
                    <asp:Literal ID="ltrDanhMucSanPham" runat="server"></asp:Literal>
                    
            </div>