﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class NhanVienDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Thuc hien chinh sua mot nhan vien

        public void NhanVien_Update(int MaNV,
                                        string TenNV,
                                        DateTime NgaySinh,
                                        string DiaChi,
                                        string SDT,
                                        string TenDangNhap,
                                        string MatKhau)
        {
            SqlCommand cmd = new SqlCommand("sp_NhanVienUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNV", MaNV);
            cmd.Parameters.AddWithValue("@TenNV", TenNV);
            cmd.Parameters.AddWithValue("@NgaySinh", NgaySinh);
            cmd.Parameters.AddWithValue("@DiaChi", DiaChi);
            cmd.Parameters.AddWithValue("@SDT", SDT);
            cmd.Parameters.AddWithValue("@TenDangNhap", TenDangNhap);
            cmd.Parameters.AddWithValue("@MatKhau", MatKhau);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion


        #region Thuc hien them mot nhan vien

        public void NhanVien_Insert(string TenNV,
                                        DateTime NgaySinh,
                                        string DiaChi,
                                        string SDT,
                                        string TenDangNhap,
                                        string MatKhau)
        {
            SqlCommand cmd = new SqlCommand("sp_NhanVienInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenNV", TenNV);
            cmd.Parameters.AddWithValue("@NgaySinh", NgaySinh);
            cmd.Parameters.AddWithValue("@DiaChi", DiaChi);
            cmd.Parameters.AddWithValue("@SDT", SDT);
            cmd.Parameters.AddWithValue("@TenDangNhap", TenDangNhap);
            cmd.Parameters.AddWithValue("@MatKhau", MatKhau);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion


        #region Thuc hien xoa 1 nhan vien

        public void NhanVien_Delete(int MaNV)
        {
            SqlCommand cmd = new SqlCommand("sp_NhanVienDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNV", MaNV);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách NhanVien
        public DataTable NhaSanXuat_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM NhanVien";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một NhanVien theo MaNV
        public DataTable NhanVien_SelectById(int MaNV)
        {
            SqlCommand cmd = new SqlCommand("sp_NhanVienSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNV", MaNV);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}