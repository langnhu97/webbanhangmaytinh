﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class ChucNangDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Update một ChucNang

        public void ChucNang_Update(int MaNV,
                                    bool QuanLySanPham,
                                    bool QuanLyKhuyenMai,
                                    bool QuanLyNhaCungCap,
                                    bool QuanLyTaiKhoan,
                                    bool QuanLyPhieuNhap,
                                    bool QuanLyDonHang,
                                    bool ThongKe)
        {
            SqlCommand cmd = new SqlCommand("sp_ChucNangUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNV", MaNV);
            cmd.Parameters.AddWithValue("@QuanLySanPham", QuanLySanPham);
            cmd.Parameters.AddWithValue("@QuanLyKhuyenMai", QuanLyKhuyenMai);
            cmd.Parameters.AddWithValue("@QuanLyNhaCungCap", QuanLyNhaCungCap);
            cmd.Parameters.AddWithValue("@QuanLyTaiKhoan", QuanLyTaiKhoan);
            cmd.Parameters.AddWithValue("@QuanLyPhieuNhap", QuanLyPhieuNhap);
            cmd.Parameters.AddWithValue("@QuanLyDonHang", QuanLyDonHang);
            cmd.Parameters.AddWithValue("@ThongKe", ThongKe);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một ChucNang

        public void ChucNang_Insert(int MaNV,
                                    bool QuanLySanPham,
                                    bool QuanLyKhuyenMai,
                                    bool QuanLyNhaCungCap,
                                    bool QuanLyTaiKhoan,
                                    bool QuanLyPhieuNhap,
                                    bool QuanLyDonHang,
                                    bool ThongKe)
        {
            SqlCommand cmd = new SqlCommand("sp_ChucNangInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNV", MaNV);
            cmd.Parameters.AddWithValue("@QuanLySanPham", QuanLySanPham);
            cmd.Parameters.AddWithValue("@QuanLyKhuyenMai", QuanLyKhuyenMai);
            cmd.Parameters.AddWithValue("@QuanLyNhaCungCap", QuanLyNhaCungCap);
            cmd.Parameters.AddWithValue("@QuanLyTaiKhoan", QuanLyTaiKhoan);
            cmd.Parameters.AddWithValue("@QuanLyPhieuNhap", QuanLyPhieuNhap);
            cmd.Parameters.AddWithValue("@QuanLyDonHang", QuanLyDonHang);
            cmd.Parameters.AddWithValue("@ThongKe", ThongKe);
            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 ChucNang

        public void ChucNang_Delete(int MaNV)
        {
            SqlCommand cmd = new SqlCommand("sp_ChucNangDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNV", MaNV);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách ChucNang
        public DataTable ChucNang_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM ChucNang";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion
    }
}