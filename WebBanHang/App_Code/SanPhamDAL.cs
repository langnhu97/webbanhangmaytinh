﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class SanPhamDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Thuc hien them mot san pham

        public void SanPham_Insert(string TenSP,
                                        int MaLoaiSP,
                                        int MaNSX,                                        
                                        int MaDM,
                                        int MaKM,
                                        string BaoHanh,
                                        int SoLuong,
                                        string ThongSo,
                                        float GiaBan,
                                        string HinhAnh,
                                        string GhiChu)
        {
            SqlCommand cmd = new SqlCommand("sp_SanPhamInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenSP", TenSP);
            cmd.Parameters.AddWithValue("@MaLoaiSP", MaLoaiSP);
            cmd.Parameters.AddWithValue("@MaNSX", MaNSX);
            cmd.Parameters.AddWithValue("@MaDM", MaDM);
            cmd.Parameters.AddWithValue("@MaKM", MaKM);
            cmd.Parameters.AddWithValue("@BaoHanh", BaoHanh);
            cmd.Parameters.AddWithValue("@SoLuong", SoLuong);
            cmd.Parameters.AddWithValue("@ThongSo", ThongSo);
            cmd.Parameters.AddWithValue("@GiaBan", GiaBan);
            cmd.Parameters.AddWithValue("@HinhAnh", HinhAnh);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);            

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thuc hien chinh sua mot san pham
    
        public void SanPham_Update(int MaSP,
                                        string TenSP,
                                        int MaLoaiSP,
                                        int MaNSX,
                                        int MaDM,
                                        int MaKM,
                                        string BaoHanh,
                                        int SoLuong,
                                        string ThongSo,
                                        float GiaBan,
                                        string HinhAnh,
                                        string GhiChu)
        {
            SqlCommand cmd = new SqlCommand("sp_SanPhamUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaSP", MaSP);
            cmd.Parameters.AddWithValue("@TenSP", TenSP);
            cmd.Parameters.AddWithValue("@MaLoaiSP", MaLoaiSP);
            cmd.Parameters.AddWithValue("@MaNSX", MaNSX);
            cmd.Parameters.AddWithValue("@MaDM", MaDM);
            cmd.Parameters.AddWithValue("@MaKM", MaKM);
            cmd.Parameters.AddWithValue("@BaoHanh", BaoHanh);
            cmd.Parameters.AddWithValue("@SoLuong", SoLuong);
            cmd.Parameters.AddWithValue("@ThongSo", ThongSo);
            cmd.Parameters.AddWithValue("@GiaBan", GiaBan);
            cmd.Parameters.AddWithValue("@HinhAnh", HinhAnh);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thuc hien xoa 1 san pham

        public void SanPham_Delete(int MaSP)
        {
            SqlCommand cmd = new SqlCommand("sp_SanPhamDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaSP", MaSP);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị toàn bộ danh sách sản phẩm
        public DataTable SanPham_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM SanPham";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị danh sách sản phẩm theo danh mục với số lượng chỉ định
        public DataTable SanPham_SelectTopByDanhMuc(int MaDM, int SoSPHienThi)
        {
            DataTable dt = new DataTable();
            string cmd = $"SELECT top {SoSPHienThi} * FROM SanPham where MaDM = {MaDM}";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion


        #region Hiển thị danh sách sản phẩm theo MaDM
        public DataTable SanPham_SelectByDanhMuc(int MaDM)
        {
            SqlCommand cmd = new SqlCommand("sp_SanPhamSelectByMaDM");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDM", MaDM);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

        #region Hiển thị một SanPham theo MaSP
        public DataTable SanPham_SelectById(int MaSP)
        {
            SqlCommand cmd = new SqlCommand("sp_SanPhamSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaSP", MaSP);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

        #region Hiển thị  SanPham theo TenSP
        public DataTable SanPham_SelectByName(string TenSP)
        {
            string cmd = $"SELECT * FROM SanPham WHERE TenSP LIKE N'%{TenSP}%'";
            return _sqldb.GetDataTable(cmd);
        }
        #endregion
    }
}