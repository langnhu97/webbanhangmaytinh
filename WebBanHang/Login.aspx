﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebBanHang.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="CMS/Admin/CSS/cssLogin.css" rel="stylesheet" />
    <link href="Libs/bootstrap.min.css" rel="stylesheet" />
    <title>Trang đăng nhập quản trị</title>
</head>
<body>
    <form id="form1" class="container col-md-3 pt-5" runat="server" defaultbutton="btnDangNhap">
        <div class="row form-outline md-4 font-weight-bold">
            <label class="form-label" for="txtTenDangNhap">Tên đăng nhập</label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtTenDangNhap" runat="server" ErrorMessage="***" CssClass=" text-right text-primary"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtTenDangNhap" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

        <!-- Password input -->
        <div class="row form-outline md-4 font-weight-bold">
            <label class="form-label" for="txtMatKhau">Mật khẩu</label>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtMatKhau" runat="server" ErrorMessage="***" CssClass="text-primary"></asp:RequiredFieldValidator>
            <asp:TextBox ID="txtMatKhau" TextMode="Password" runat="server" CssClass="form-control"></asp:TextBox>
        </div>

        <div class="row md-4 py-2  font-weight-bold">
            <div class="col d-flex">
                <!-- Checkbox -->
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" value="" id="ckNhoMatKhau"  />
                    <label class="form-check-label" for="ckNhoMatKhau">Nhớ mật khẩu </label>
                </div>
            </div>
        </div>

        <!-- Submit button -->
        <div class="row">
            <asp:LinkButton ID="btnDangNhap" class="btn btn-primary btn-block mb-4"  runat="server" OnClick="btnDangNhap_Click">Đăng nhập</asp:LinkButton>
            <asp:Label ID="lblThongBao" class="form-label text-white font-weight-bold" runat="server"></asp:Label>
        </div>
    </form>
</body>
</html>