﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication2;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.SanPham.AJAX
{
    public partial class XuLyGioHang : System.Web.UI.Page
    {
        GioHangDAL gioHangDAL = new GioHangDAL();
        SanPhamDAL sanPhamDAL = new SanPhamDAL();
        KhachHangDAL khachHangDAL = new KhachHangDAL();
        DonDatHangDAL donDatHangDAL = new DonDatHangDAL();
        ChiTietDonHangDAL chiTietDonHangDAL = new ChiTietDonHangDAL();
        public string ThaoTac { get; set; }
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["ThaoTac"] != null)
            {
                ThaoTac = Request.Params["ThaoTac"];
            }

            switch (ThaoTac)
            {
                case "XuLyGioHang":
                    ThemGioHang();
                    break;
                case "LayThongTinGioHang":
                    LayThongTinGioHang();
                    break;
                case "LayTongSoLuongSP":
                    LayTongSoLuongSP();
                    break;
                case "LayTongTien":
                    LayTongTien();
                    break;
                case "XoaSanPhamTrongGio":
                    XoaSanPhamTrongGio();
                    break;
                case "CapNhatSoLuong":
                    CapNhatSoLuong();
                    break;
                case "GuiDonHang":
                    GuiDonHang();
                    break;
                default:
                    break;
            }
        }

        private void GuiDonHang()
        {
            string ketQua = "";

            //Lấy thông tin khách hàng gửi lên
            string hoTen = Request.Params["HoTen"];
            string diaChi = Request.Params["DiaChi"];
            string soDienThoai = Request.Params["SoDienThoai"];
            string emailKH = Request.Params["EmailKH"];
            string phuongThucThanhToan = Request.Params["phuongThucThanhToan"];

            if (Session["GioHang"] != null)
            {
                //Gán thông tin giỏ hàng vào Datatable
                DataTable dtGioHang = (DataTable)Session["GioHang"];

                double tongTien = 0;
                //Lặp DataTable giỏ hàng để lấy ra tổng tiền
                for (int i = 0; i < dtGioHang.Rows.Count; i++)
                {
                    tongTien += Convert.ToInt32(dtGioHang.Rows[i]["SoLuong"]) * Convert.ToDouble(dtGioHang.Rows[i]["GiaBan"]);
                }
                //Kiểm tra thông tin khách hàng
                string maKH = XuLyThongTinKhachHang(hoTen, diaChi, soDienThoai, emailKH);

                //Lấy ngày giờ hiện tại trả về dạng số để làm mã thanh toán trực tuyến
                string mathanhtoantructuyen = DateTime.Now.Ticks.ToString();

                //Thêm thông tin vào bảng Đơn đặt hàng và lấy ra mã đơn đặt hàng
                donDatHangDAL.DonDatHang_Insert(DateTime.Now, float.Parse(tongTien.ToString()), mathanhtoantructuyen, Convert.ToInt32(maKH), hoTen, soDienThoai, emailKH);
                DataTable dtDonDatHang = donDatHangDAL.DonDatHang_SelectDesc();
                string maHD = dtDonDatHang.Rows[0]["MaHD"].ToString();

                //Đọc giỏ hàng và thêm từng sản phẩm vào bảng chi tiết đơn hàng
                for (int i = 0; i < dtGioHang.Rows.Count; i++)
                {
                    chiTietDonHangDAL.ChiTietDonHang_Insert(Convert.ToInt32(maHD), Convert.ToInt32(dtGioHang.Rows[i]["MaSP"]),
                        Convert.ToInt32(dtGioHang.Rows[i]["SoLuong"]), Convert.ToSingle(dtGioHang.Rows[i]["GiaBan"]));
                }

                //Xóa thông tin giỏ hàng
                Session["GioHang"] = null;

                #region Xử lý tương ứng cho các hình thức thanh toán

                switch (phuongThucThanhToan)
                {
                    case "ChuyenKhoan":
                        break;

                    case "Onepay":
                        #region Chuyển sang trang Onepay
                        string SECURE_SECRET = OnepayCode.SECURE_SECRET;//  cần thanh bằng mã thật cấu hình trong app_code
                                                                        // Khoi tao lop thu vien va gan gia tri cac tham so gui sang cong thanh toan
                        VPCRequest conn = new VPCRequest(OnepayCode.VPCRequest);//  Cần thay bằng cổng thật cấu hình trong app_code
                        conn.SetSecureSecret(SECURE_SECRET);
                        // Add the Digital Order Fields for the functionality you wish to use
                        // Core Transaction Fields
                        conn.AddDigitalOrderField("Title", "onepay paygate");
                        conn.AddDigitalOrderField("vpc_Locale", "vn");//Chon ngon ngu hien thi tren cong thanh toan (vn/en)
                        conn.AddDigitalOrderField("vpc_Version", "2");
                        conn.AddDigitalOrderField("vpc_Command", "pay");
                        conn.AddDigitalOrderField("vpc_Merchant", OnepayCode.Merchant);//  cần thanh bằng mã thật cấu hình trong app_code
                        conn.AddDigitalOrderField("vpc_AccessCode", OnepayCode.AccessCode);//  cần thanh bằng mã thật cấu hình trong app_code
                        conn.AddDigitalOrderField("vpc_MerchTxnRef", mathanhtoantructuyen);//  mã thanh toán
                        conn.AddDigitalOrderField("vpc_OrderInfo", mathanhtoantructuyen);//  thông tin đơn hàng
                        conn.AddDigitalOrderField("vpc_Amount", (tongTien * 100).ToString());//  chi phí cần nhân 100 theo yêu cầu của onepay
                        conn.AddDigitalOrderField("vpc_Currency", "VND");
                        conn.AddDigitalOrderField("vpc_ReturnURL", OnepayCode.ReturnURL);//  địa chỉ nhận kết quả trả về
                                                                                         // Thong tin them ve khach hang. De trong neu khong co thong tin
                        conn.AddDigitalOrderField("vpc_SHIP_Street01", "");
                        conn.AddDigitalOrderField("vpc_SHIP_Provice", "");
                        conn.AddDigitalOrderField("vpc_SHIP_City", "");
                        conn.AddDigitalOrderField("vpc_SHIP_Country", "");
                        conn.AddDigitalOrderField("vpc_Customer_Phone", "");
                        conn.AddDigitalOrderField("vpc_Customer_Email", "");
                        conn.AddDigitalOrderField("vpc_Customer_Id", "");
                        // Dia chi IP cua khach hang
                        conn.AddDigitalOrderField("vpc_TicketNo", Request.UserHostAddress);
                        // Chuyen huong trinh duyet sang cong thanh toan
                        String url = conn.Create3PartyQueryString();
                        #endregion

                        ketQua = url;

                        break;
                    case "OnepayQuocTe":
                        string SECURE_SECRET1 = OnepayQuocTeCode.SECURE_SECRET;//  cần thanh bằng mã thật cấu hình trong app_code; 
                                                                               // Khoi tao lop thu vien va gan gia tri cac tham so gui sang cong thanh toan
                        VPCRequest conn1 = new VPCRequest(OnepayQuocTeCode.VPCRequest);//  Cần thay bằng cổng thật
                        conn1.SetSecureSecret(SECURE_SECRET1);
                        // Add the Digital Order Fields for the functionality you wish to use
                        // Core Transaction Fields
                        conn1.AddDigitalOrderField("AgainLink", "http://onepay.vn");
                        conn1.AddDigitalOrderField("Title", "onepay paygate");
                        conn1.AddDigitalOrderField("vpc_Locale", "en");//Chon ngon ngu hien thi tren cong thanh toan (vn/en)
                        conn1.AddDigitalOrderField("vpc_Version", "2");
                        conn1.AddDigitalOrderField("vpc_Command", "pay");
                        conn1.AddDigitalOrderField("vpc_Merchant", OnepayQuocTeCode.Merchant);//  cần thanh bằng mã thật cấu hình trong app_code
                        conn1.AddDigitalOrderField("vpc_AccessCode", OnepayQuocTeCode.AccessCode);//  cần thanh bằng mã thật cấu hình trong app_code
                        conn1.AddDigitalOrderField("vpc_MerchTxnRef", mathanhtoantructuyen);//  mã thanh toán
                        conn1.AddDigitalOrderField("vpc_OrderInfo", mathanhtoantructuyen);//  mã thanh toán
                        conn1.AddDigitalOrderField("vpc_Amount", (tongTien * 100).ToString());//  chi phí cần nhân 100 theo yêu cầu của onepay

                        conn1.AddDigitalOrderField("vpc_ReturnURL", OnepayQuocTeCode.ReturnURL);//  địa chỉ nhận kết quả trả về
                                                                                                // Thong tin them ve khach hang. De trong neu khong co thong tin
                        //conn1.AddDigitalOrderField("vpc_SHIP_Street01", "");
                        //conn1.AddDigitalOrderField("vpc_SHIP_Provice", "");
                        //conn1.AddDigitalOrderField("vpc_SHIP_City", "");
                        //conn1.AddDigitalOrderField("vpc_SHIP_Country", "");
                        conn1.AddDigitalOrderField("vpc_Customer_Phone", "");
                        conn1.AddDigitalOrderField("vpc_Customer_Email", "");
                        conn1.AddDigitalOrderField("vpc_Customer_Id", "");
                        // Dia chi IP cua khach hang
                        conn1.AddDigitalOrderField("vpc_TicketNo", Request.UserHostAddress);
                        // Chuyen huong trinh duyet sang cong thanh toan
                        String url1 = conn1.Create3PartyQueryString();
                        ketQua = url1;

                        break;
                }
                #endregion

            }
            else
            {
                ketQua = "Giỏ hàng đã hết hạn. Quý khách vui lòng đặt hàng lại!";
            }
            Response.Write(ketQua);
        }

        private string XuLyThongTinKhachHang(string hoTen, string diaChi, string soDienThoai, string emailKH)
        {
            //Kiểm tra khách hàng đã từng đăng ký tài khoản chưa
            DataTable dt = khachHangDAL.KhachHang_SelectByEmail(emailKH);
            if(dt.Rows.Count == 0)
            {
                khachHangDAL.KhachHang_Insert(hoTen, diaChi, soDienThoai, emailKH, MaHoa.GetMD5(emailKH), "");

                //Lấy thông tin khách hàng vừa tạo mới và trả về  MaKH
                dt = khachHangDAL.KhachHang_SelectByEmail(emailKH);
                return dt.Rows[0]["MaKH"].ToString();
            }
            else
            {
                return dt.Rows[0]["MaKH"].ToString();
            }
        }

        private void CapNhatSoLuong()
        {
            string idSanPham = Request.Params["id"].ToString();
            string soLuongMoi = Request.Params["SoLuongMoi"].ToString();
            if (Session["GioHang"] != null)
            {
                //Gán thông tin giỏ hàng vào Datatable
                DataTable dtGioHang = (DataTable)Session["GioHang"];

                //Lấy danh sách sản phẩm trong giở hàng --> Loại sản phẩm có MaSP == id truyền lên
                for (int i = 0; i < dtGioHang.Rows.Count; i++)
                {
                    if (dtGioHang.Rows[i]["MaSP"].ToString() == idSanPham)
                    {
                        dtGioHang.Rows[i]["SoLuong"] = soLuongMoi;
                    }
                }
                //Gán lại Datatable vào giỏ hàng
                Session["GioHang"] = dtGioHang;
            }
            Response.Write("");
        }

        private void XoaSanPhamTrongGio()
        {
            string idSanPham = Request.Params["id"].ToString();

            if (Session["GioHang"] != null)
            {
                //Gán thông tin giỏ hàng vào Datatable
                DataTable dtGioHang = (DataTable)Session["GioHang"];

                //Lấy danh sách sản phẩm trong giở hàng --> Loại sản phẩm có MaSP == id truyền lên
                for (int i = 0; i < dtGioHang.Rows.Count; i++)
                {
                    if (dtGioHang.Rows[i]["MaSP"].ToString() == idSanPham)
                    {
                        dtGioHang.Rows[i].Delete();
                    }                    
                }
                //Gán lại Datatable vào giỏ hàng
                Session["GioHang"] = dtGioHang;
            }
            Response.Write("");
        }

        private void LayTongTien()
        {
            double tongtien = 0;
            if (Session["GioHang"] != null)
            {
                //Gán thông tin giỏ hàng vào Datatable
                DataTable dtGioHang = (DataTable)Session["GioHang"];

                for (int i = 0; i < dtGioHang.Rows.Count; i++)
                {
                    tongtien += Convert.ToInt32(dtGioHang.Rows[i]["SoLuong"]) * Convert.ToDouble(dtGioHang.Rows[i]["GiaBan"]);
                }
            }

            Response.Write(tongtien);
        }

        private void LayTongSoLuongSP()
        {
            int ketqua = 0;
            if (Session["GioHang"] != null)
            {
                //Gán thông tin giỏ hàng vào Datatable
                DataTable dtGioHang = (DataTable)Session["GioHang"];

                for (int i = 0; i < dtGioHang.Rows.Count; i++)
                {
                    ketqua += Convert.ToInt32(dtGioHang.Rows[i]["SoLuong"]);
                }
            }

            Response.Write(ketqua);
        }

        private void LayThongTinGioHang()
        {
            string ketqua = "";

            ketqua += @"
            <table class='w-100'>
                <tr class='text-center row'>
                    <th class='col-md-3'>Ảnh</th>
                    <th class='col-md-4'>Tên sản phẩm</th>
                    <th class='col-md-2'>Số lượng</th>
                    <th class='col-md-2'>Đơn giá</th>
                    <th class='col-md-1'></th>
                </tr>              
    ";
            if (Session["GioHang"] != null)
            {
                //Gán thông tin giỏ hàng vào Datatable
                DataTable dtGioHang = (DataTable)Session["GioHang"];

                //Lặp để lấy thông tin giỏ hàng ra bảng
                for (int i = 0; i < dtGioHang.Rows.Count; i++)
                {
                    ketqua += @"
                    <tr class='text-center row' id='rowSP_"+ dtGioHang.Rows[i]["MaSP"] + @"'>
                        <td class='col-md-3'>
                            <img class='img-fluid' src='IMG/SanPham/" + dtGioHang.Rows[i]["HinhAnh"] + @"'/>
                        </td>
                        <td class='col-md-4'>
                            <a href='Default.aspx?uc=prd&suc=dtl&id=" + dtGioHang.Rows[i]["MaSP"] + @"' title=' "+ dtGioHang.Rows[i]["TenSP"] + @"'>
                                " + dtGioHang.Rows[i]["TenSP"] + @"
                            </a>
                        </td>
                        <td class='col-md-2'>
                            <input class='w-100' type='number' id='quantity_" + dtGioHang.Rows[i]["MaSP"] + @"' onchange='javascript:CapNhatSoLuong(" + dtGioHang.Rows[i]["MaSP"] + @")' value='" + dtGioHang.Rows[i]["SoLuong"] + @"'/>
                        </td>
                        <td class='col-md-2'> " + dtGioHang.Rows[i]["GiaBan"] + @" VNĐ</td>
                        <td class='col-md-1'><a href='javascript:XoaSPTrongGioHang(" + dtGioHang.Rows[i]["MaSP"] + @")'> Xóa </a></td>
                    </tr>   
                    ";
                }
                ketqua += @"</table >";
            }

            Response.Write(ketqua);
        }

        private void ThemGioHang()
        {
            string ketqua = "";

            //Lấy id và số lượng sản phẩm
            int id = Convert.ToInt32(Request.Params["id"].ToString());
            int soluong = Convert.ToInt32(Request.Params["soluong"].ToString());           

            //Lấy thông tin sản phẩm được thêm vào 
            DataTable dt = sanPhamDAL.SanPham_SelectById(id);
            if(dt.Rows.Count > 0)
            {
                if (Session["GioHang"] == null)
                {
                    //Trường họp chưa có giỏ hàng
                    //Khai báo DataTable để lưu thông tin sản phẩm thêm vào giỏ hàng lần đầu tiên
                    DataTable dtGioHang = new DataTable();
                    dtGioHang.Columns.Add("MaSP");
                    dtGioHang.Columns.Add("TenSP");
                    dtGioHang.Columns.Add("HinhAnh");
                    dtGioHang.Columns.Add("GiaBan");
                    dtGioHang.Columns.Add("SoLuong");

                    dtGioHang.Rows.Add(dt.Rows[0]["MaSP"], dt.Rows[0]["TenSP"], dt.Rows[0]["HinhAnh"],
                        dt.Rows[0]["GiaBan"], soluong);
                    Session["GioHang"] = dtGioHang;
                }
                else
                {
                    //Trường hợp đã có giỏ hàng
                    //Khai báo DataTable để chưa thông tin giỏ hàng
                    DataTable dtGioHang = (DataTable)Session["GioHang"];

                    //Kiểm tra sản phẩm thêm vào có trong giỏ hàng hay chưa
                    int ViTriSanPham = -1;
                    for (int i = 0; i < dtGioHang.Rows.Count ; i++)
                    {
                        if(dtGioHang.Rows[i]["MaSP"].ToString() == id.ToString())
                        {
                            //Sản phẩm đã có trong giỏ hàng
                            ViTriSanPham = i;
                            break;
                        }
                    }

                    //Xử lý khi sản phẩm chưa có trong giỏ
                    if(ViTriSanPham == -1)
                    {
                        //Thêm sản phẩm vào giỏ
                        dtGioHang.Rows.Add(dt.Rows[0]["MaSP"], dt.Rows[0]["TenSP"], dt.Rows[0]["HinhAnh"],
                        dt.Rows[0]["GiaBan"], soluong);

                        //Gán DataTable sau khi thêm sản phẩm -> giỏ hàng
                        Session["GioHang"] = dtGioHang;
                    }
                    //Xử lý khi sản phẩm đã có trong giỏ
                    else
                    {
                        //Lấy ra số lượng sản phẩm hiện tại trong giỏ hàng
                        int soLuongHienTai = Convert.ToInt32(dtGioHang.Rows[ViTriSanPham]["SoLuong"].ToString());

                        //Cộng thêm số lượng 
                        soLuongHienTai += soluong;

                        //Cập nhật số lượng sau khi cộng của sản phẩm 
                        dtGioHang.Rows[ViTriSanPham]["SoLuong"] = soLuongHienTai;

                        //Gán lại DataTable vào giỏ
                        Session["GioHang"] = dtGioHang;
                    }
                }
            }
            else
            {
                ketqua = "Không tồn tại sản phẩm này";
            }
            Response.Write(ketqua);
        }
    }
}