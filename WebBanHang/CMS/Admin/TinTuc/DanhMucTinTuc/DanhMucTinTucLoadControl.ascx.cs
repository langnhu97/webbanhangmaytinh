﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Admin.TinTuc.DanhMucTinTuc
{
    public partial class DanhMucTinTucLoadControl : System.Web.UI.UserControl
    {
        string man = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["man"] != null)
            {
                man = Request.QueryString["man"];
            }

            switch (man)
            {
                case "add":
                    plControl.Controls.Add(LoadControl("DanhMucTinTucInsert.ascx"));
                    break;
                case "upd":
                    plControl.Controls.Add(LoadControl("DanhMucTinTucUpdate.ascx"));
                    break;
                case "del":
                    plControl.Controls.Add(LoadControl("DanhMucTinTucDelete.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("DanhMucTinTucSelect.ascx"));
                    break;
            }
        }
    }
}