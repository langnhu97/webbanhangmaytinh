﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.DaiLyCungCap
{
    public partial class DaiLySelect : System.Web.UI.UserControl
    {
        DaiLyCungCapDAL dal = new DaiLyCungCapDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhSach();
            }
        }

        private void LayDanhSach()
        {
            DataTable dt = new DataTable();
            dt = dal.DaiLyCungCap_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaDL"] + @"'>
                     <td class='colId'>" + dt.Rows[i]["MaDL"] + @"</td>   
                     <td class='colName'>" + dt.Rows[i]["TenDL"] + @"</td>       
                     <td class='colAdd'>" + dt.Rows[i]["DiaChi"] + @"</td>       
                     <td class='colPhone'>" + dt.Rows[i]["SDT"] + @"</td>       
                     <td class='colTool'>
                         <a href = 'Admin.aspx?uc=sup&suc=lst&man=upd&id=" + dt.Rows[i]["MaDL"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaDL"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>                
                </tr>
            ";

            }
        }
    }
}