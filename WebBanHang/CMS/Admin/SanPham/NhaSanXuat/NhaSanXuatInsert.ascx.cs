﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.NhaSanXuat
{
    public partial class NhaSanXuatInsert : System.Web.UI.UserControl
    {
        NhaSanXuatDAL dal = new NhaSanXuatDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.NhaSanXuat_Insert(txtTenNSX.Text.Trim(), txtGhiChu.Text.Trim());
                lblThongBao.Text = "<div class='successNoti'>Đã tạo nhà sản xuất: " + txtTenNSX.Text + "</div>";
                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=prd&suc=man");
                }

            }
            catch (Exception)
            {
                lblThongBao.Text = "Chưa thêm được nhà sản xuất";
                throw;
            }
        }

        private void ResetForm()
        {
            txtTenNSX.Text = String.Empty;
            txtGhiChu.Text = String.Empty;
            txtTenNSX.Focus();
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}