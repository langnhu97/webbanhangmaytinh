﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.SanPham
{
    public partial class SanPhamLoadControl : System.Web.UI.UserControl
    {
        string sucString = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["suc"] != null)
            {
                sucString = Request.QueryString["suc"];
            }

            switch (sucString)
            {
                case "typ":
                    plSanPhamLoadControl.Controls.Add(LoadControl("LoaiSanPham/LoaiSanPhamControl.ascx"));                
                    break;
                case "lst":
                    plSanPhamLoadControl.Controls.Add(LoadControl("DanhSachSanPham/DanhSachSanPhamControl.ascx"));
                    break;
                case "cat":
                    plSanPhamLoadControl.Controls.Add(LoadControl("DanhMucSanPham/DanhMucControl.ascx"));                   
                    break;
                case "man":
                    plSanPhamLoadControl.Controls.Add(LoadControl("NhaSanXuat/NhaSanXuatControl.ascx"));                    
                    break;
                case "dis":
                    plSanPhamLoadControl.Controls.Add(LoadControl("KhuyenMai/KhuyenMaiLoadControl.ascx"));
                    break;
                default:
                    plSanPhamLoadControl.Controls.Add(LoadControl("DanhSachSanPham/DanhSachSanPhamControl.ascx"));
                    break;
            }          
        }

        protected string DanhDauThaoTac(string ucName, string sucName, string manName)
        {
            string s = "";

            //Lấy giá trị queryString trong modul
            string uc = String.Empty;
            string suc = String.Empty;
            string man = String.Empty;
            if (Request.QueryString["uc"] != null)
            {
                uc = Request.QueryString["uc"];
            }
            if (Request.QueryString["suc"] != null)
            {
                suc = Request.QueryString["suc"];
            }
            if (Request.QueryString["man"] != null)
            {
                man = Request.QueryString["man"];
            }

            //So sánh queryString bằng tên uc + suc + man truyền vào thì trả về current --> Đánh dấu là menu hiện tại
            if (uc == ucName && suc == sucName && man == manName)
                s = "current";

            return s;
        }
    }
}