﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TaiKhoan
{
    public partial class TaiKhoanDelete : System.Web.UI.UserControl
    {
        TaiKhoanDAL dal = new TaiKhoanDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = "";
            if(Request.QueryString["id"] != null)
            {
                id = Request.QueryString["id"];
            }

            XoaTaiKhoan(id);
        }

        private void XoaTaiKhoan(string id)
        {
            dal.TaiKhoan_Delete(id);
            Response.Redirect("Admin.aspx?uc=acc&suc=lst");
        }
    }
}