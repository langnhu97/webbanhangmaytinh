﻿using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.SanPham
{
    public partial class ChiTietSanPham : System.Web.UI.UserControl
    {
        protected int id { get; set; }
        SanPhamDAL sanPhamDAL = new SanPhamDAL();
        NhaSanXuatDAL nhaSanXuatDAL = new NhaSanXuatDAL();
        LoaiSanPhamDAL loaiSanPhamDAL = new LoaiSanPhamDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }

            if (!IsPostBack)
            {
                LayChiTietSanPham(id);
            }
        }

        private void LayChiTietSanPham(int id)
        {
            var culture = new CultureInfo("en-US");
            culture.NumberFormat.NumberDecimalSeparator = ",";
            culture.NumberFormat.NumberGroupSeparator = ".";

            DataTable dt = sanPhamDAL.SanPham_SelectById(id);
            if(dt.Rows.Count > 0)
            {
                ltrAnh.Text = "<img src='IMG/SanPham/" + dt.Rows[0]["HinhAnh"] +@"' class='product-detail-img img-fluid'/>";
                ltrTen.Text = dt.Rows[0]["TenSP"].ToString();
                ltrGia.Text = Convert.ToDouble(dt.Rows[0]["GiaBan"]).ToString("N").Replace(".00", "") + " VNĐ";
                ltrNSX.Text = LayNhaSanXuat(Convert.ToInt32(dt.Rows[0]["MaNSX"].ToString()));
                ltrLoaiSP.Text = LayLoaiSP(Convert.ToInt32(dt.Rows[0]["MaLoaiSP"].ToString()));
                ltrThongSo.Text = dt.Rows[0]["ThongSo"].ToString();
                ltrBaoHanh.Text = dt.Rows[0]["BaoHanh"].ToString();
                ltrMoTa.Text = dt.Rows[0]["GhiChu"].ToString();
            }
        }

        private string LayNhaSanXuat(int MaNSX)
        {
            string s = "";
            DataTable dt = nhaSanXuatDAL.NhaSanXuat_SelectById(MaNSX);
            if(dt.Rows.Count > 0)
            {
                s = dt.Rows[0]["TenNSX"].ToString();
            }
            return s;
        }

        private string LayLoaiSP(int MaLoaiSP)
        {
            string s = "";
            DataTable dt = loaiSanPhamDAL.LoaiSanPham_SelectById(MaLoaiSP);
            if (dt.Rows.Count > 0)
            {
                s = dt.Rows[0]["TenLoaiSP"].ToString();
            }
            return s;
        }
    }
}