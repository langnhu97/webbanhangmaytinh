﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.LoaiSanPham
{
    public partial class LoaiSanPhamSelect : System.Web.UI.UserControl
    {
        LoaiSanPhamDAL dal = new LoaiSanPhamDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhMuc();
            }
        }

        private void LayDanhMuc()
        {
            DataTable dt = new DataTable();
            dt = dal.LoaiSanPham_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaLoaiSP"] + @"'>
                     <td class='colId'>" + dt.Rows[i]["MaLoaiSP"] + @"</td>   
                     <td class='colName'>" + dt.Rows[i]["TenLoaiSP"] + @"</td>       
                     <td class='colNote'>" + dt.Rows[i]["GhiChu"] + @"</td>       
                     <td class='colTool'>
                         <a href = 'Admin.aspx?uc=prd&suc=typ&man=upd&id=" + dt.Rows[i]["MaLoaiSP"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaLoaiSP"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>                
                </tr>
            ";
            }
        }
    }
}