﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class KhuyenMaiDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();
        #region Update một KhuyenMai

        public void KhuyenMai_Update(int MaKM,
                                        string TenKM,
                                        string NoiDungKhuyenMai,
                                        DateTime NgayBatDau,
                                        DateTime NgayKetThuc)
        {
            SqlCommand cmd = new SqlCommand("sp_KhuyenMaiUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaKM", MaKM);
            cmd.Parameters.AddWithValue("@TenKM", TenKM);
            cmd.Parameters.AddWithValue("@NoiDungKhuyenMai", NoiDungKhuyenMai);
            cmd.Parameters.AddWithValue("@NgayBatDau", NgayBatDau);
            cmd.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một KhuyenMai

        public void KhuyenMai_Insert(string TenKM,
                                        string NoiDungKhuyenMai,
                                        DateTime NgayBatDau,
                                        DateTime NgayKetThuc)
        {
            SqlCommand cmd = new SqlCommand("sp_KhuyenMaiInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenKM", TenKM);
            cmd.Parameters.AddWithValue("@NoiDungKhuyenMai", NoiDungKhuyenMai);
            cmd.Parameters.AddWithValue("@NgayBatDau", NgayBatDau);
            cmd.Parameters.AddWithValue("@NgayKetThuc", NgayKetThuc);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 KhuyenMai

        public void KhuyenMai_Delete(int MaKM)
        {
            SqlCommand cmd = new SqlCommand("sp_KhuyenMaiDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaKM", MaKM);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách KhuyenMai
        public DataTable KhuyenMai_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM KhuyenMai";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một KhuyenMai theo MaKM
        public DataTable KhuyenMai_SelectById(int MaKM)
        {
            SqlCommand cmd = new SqlCommand("sp_KhuyenMaiSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaKM", MaKM);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}