﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.DanhSachSanPham
{
    public partial class SanPhamUpdate : System.Web.UI.UserControl
    {
        SanPhamDAL _sanphamDAL = new SanPhamDAL();
        NhaSanXuatDAL _nhaSanXuatDAL = new NhaSanXuatDAL();
        LoaiSanPhamDAL _loaiSanPhamDAL = new LoaiSanPhamDAL();
        KhuyenMaiDAL _khuyenMaiDAL = new KhuyenMaiDAL();
        DanhMucDAL _danhMucDAL = new DanhMucDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhSachNSX();
                LayDanhSachLoaiSP();
                LayDanhSachKhuyenMai();
                LayDanhMuc();
            }
            id = Convert.ToInt32(Request.QueryString["id"]);
            HienThiThongTin(id);
        }

        private void HienThiThongTin(int id)
            {
            DataTable dt = new DataTable();
            dt = _sanphamDAL.SanPham_SelectById(id);

            txtTenSP.Text = dt.Rows[0]["TenSP"].ToString();
            ddlLoaiSP.SelectedValue = dt.Rows[0]["MaLoaiSP"].ToString();
            ddlNSX.SelectedValue = dt.Rows[0]["MaNSX"].ToString();
            ddlDM.SelectedValue = dt.Rows[0]["MaDM"].ToString();
            ddlKhuyenMai.SelectedValue = dt.Rows[0]["MaKM"].ToString();
            txtBaoHanh.Text = dt.Rows[0]["BaoHanh"].ToString();
            txtSoLuong.Text = dt.Rows[0]["SoLuong"].ToString();
            txtThongSo.Text = dt.Rows[0]["ThongSo"].ToString();
            txtGiaBan.Text = dt.Rows[0]["GiaBan"].ToString();
            ltrAnh.Text += @"<img class='img-load' src='/IMG/SanPham/" + dt.Rows[0]["HinhAnh"] + @"'>";
            hdfAnh.Value = dt.Rows[0]["HinhAnh"].ToString();
            txtGhiChu.Text = dt.Rows[0]["GhiChu"].ToString();
        }

        private void LayDanhMuc()
        {
            DataTable dt = new DataTable();
            dt = _danhMucDAL.DanhMuc_Select();

            ddlDM.Items.Clear();
            ddlDM.Items.Add(new ListItem("Chọn danh mục", "0"));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlDM.Items.Add(new ListItem(dt.Rows[i]["TenDM"].ToString(), dt.Rows[i]["MaDM"].ToString()));
            }
        }

        private void LayDanhSachKhuyenMai()
        {
            DataTable dt = new DataTable();
            dt = _khuyenMaiDAL.KhuyenMai_Select();

            ddlKhuyenMai.Items.Clear();
            ddlKhuyenMai.Items.Add(new ListItem("Chọn khuyến mãi", "0"));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlKhuyenMai.Items.Add(new ListItem(dt.Rows[i]["TenKM"].ToString(), dt.Rows[i]["MaKM"].ToString()));
            }
        }

        private void LayDanhSachNSX()
        {
            DataTable dt = new DataTable();
            dt = _nhaSanXuatDAL.NhaSanXuat_Select();

            ddlNSX.Items.Clear();
            ddlNSX.Items.Add(new ListItem("Chọn nhà sản xuất", "0"));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlNSX.Items.Add(new ListItem(dt.Rows[i]["TenNSX"].ToString(), dt.Rows[i]["MaNSX"].ToString()));
            }

        }

        private void LayDanhSachLoaiSP()
        {
            DataTable dt = new DataTable();
            dt = _loaiSanPhamDAL.LoaiSanPham_Select();

            ddlLoaiSP.Items.Clear();
            ddlLoaiSP.Items.Add(new ListItem("Chọn loại sản phẩm", "0"));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlLoaiSP.Items.Add(new ListItem(dt.Rows[i]["TenLoaiSP"].ToString(), dt.Rows[i]["MaLoaiSP"].ToString()));
            }
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                string tenAnh = "";
                if (flHinhAnh.FileContent.Length > 0)
                {
                    if (flHinhAnh.FileName.EndsWith(".jpg") || flHinhAnh.FileName.EndsWith(".png")
                        || flHinhAnh.FileName.EndsWith(".gif") || flHinhAnh.FileName.EndsWith(".jpeg")
                        || flHinhAnh.FileName.EndsWith(".webp") || flHinhAnh.FileName.EndsWith(".tiff")
                        || flHinhAnh.FileName.EndsWith(".psd") || flHinhAnh.FileName.EndsWith(".pdf"))
                    {
                        flHinhAnh.SaveAs(Server.MapPath("IMG/SanPham/") + flHinhAnh.FileName);
                    }
                    tenAnh = flHinhAnh.FileName;
                }
                else
                {
                    tenAnh = hdfAnh.Value;
                }
                string tensp = txtTenSP.Text.Trim();
                int loaisp = Convert.ToInt32(ddlLoaiSP.SelectedValue) > 0 ? Convert.ToInt32(ddlLoaiSP.SelectedValue) : 0;
                int mansx = Convert.ToInt32(ddlNSX.SelectedValue) > 0 ? Convert.ToInt32(ddlNSX.SelectedValue) : 0;
                int madm = Convert.ToInt32(ddlDM.SelectedValue) > 0 ? Convert.ToInt32(ddlDM.SelectedValue) : 0;
                int makm = Convert.ToInt32(ddlKhuyenMai.SelectedValue) > 0 ? Convert.ToInt32(ddlKhuyenMai.SelectedValue) : 0;
                string baohanh = txtBaoHanh.Text.Trim();
                int soluong = txtSoLuong.Text.Trim() != "" ? Convert.ToInt32(txtSoLuong.Text.Trim()) : 0;
                string thongso = txtThongSo.Text.Trim();
                float giaban = txtGiaBan.Text.Trim() != "" ? Convert.ToSingle(txtGiaBan.Text.Replace(".", "")) : 0;
                string ghichu = txtGhiChu.Text.Trim();

                _sanphamDAL.SanPham_Update(id, tensp, loaisp, mansx, madm, makm, baohanh,
                        soluong, thongso, giaban, tenAnh, ghichu);
                Response.Redirect("Admin.aspx?uc=prd&suc=lst");
            }
            catch (Exception)
            {
                lblThongBao.Text = "Chỉnh sửa thất bại";
            }           
           
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenSP.Text = String.Empty;
            txtBaoHanh.Text = String.Empty;
            txtGhiChu.Text = String.Empty;
            txtGiaBan.Text = String.Empty;
            txtSoLuong.Text = String.Empty;
            txtThongSo.Text = String.Empty;
            ddlKhuyenMai.SelectedValue = "0";
            ddlLoaiSP.SelectedValue = "0";
            ddlNSX.SelectedValue = "0";
            flHinhAnh.FileContent.Flush();
        }
    }
}