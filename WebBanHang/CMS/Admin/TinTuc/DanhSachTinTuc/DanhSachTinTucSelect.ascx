﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DanhSachTinTucSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.TinTuc.TinTucSelect" %>
<div class="head">
    <div class="fl" style="padding-right: 30px;">Danh sách các tin tức</div>
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=new&suc=lst&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colGrp">Danh mục tin tức</th>
            <th class="colName">Tiêu đề</th>
            <th class="colQua">Lượt xem</th>                       
            <th class="colDate">NgayDang</th>                       
            <th class="colImg">Ảnh đại diện</th>            
            <th class="colTool">Công cụ</th>
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc muốn xóa tin này?")) {
            $.post("CMS/Admin/TinTuc/DanhSachTinTuc/AJAX/DanhSachTinTuc.aspx",
                {
                    "man": "del",
                    "id": id
                },
                function (data, status) {
                    if (data == "1") //Nếu thực hiện thành công data -> 1, ngược lại data ->0
                    {
                        $("#rowId_" + id).slideUp();
                    } else {
                        alert("Xóa thất bại");
                    }
                }
            );
        }        
    }
</script>