﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Admin.aspx.cs" Inherits="WebBanHang.Admin" %>
<%@ Register Src="~/CMS/Admin/AdminLoadControl.ascx" TagPrefix="uc1" TagName="AdminLoadControl" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Trang quản trị website</title>
    <%--<link href="Libs/bootstrap.min.js" rel="stylesheet" />
    <link href="Libs/bootstrap.min.css" rel="stylesheet" />--%>
    <link href="Libs/fontawesome/fontawesome/css/all.min.css" rel="stylesheet" />
    <link href="CMS/Admin/CSS/cssAdmin.css" rel="stylesheet" />

    <%--jquery--%>
    <script src="Libs/jquery-3.6.4.min.js"></script>

    <%--ckeditor--%>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%--Header--%>
            <div id="header">
                <div class="container">
                    <div class="logo">
                        <img style="height: 100%" src="IMG/QuangCao/logo.png" />
                    </div>
                    <div class="accountMenu">
                        <asp:Literal ID="ltrXinChao" runat="server"></asp:Literal>
                        <asp:LinkButton ID="btnDangXuat" runat="server" OnClick="btnDangXuat_Click">Đăng xuất</asp:LinkButton>
                    </div>
                </div>
            </div>

            <%--Menu chính--%>
            <div class="Menu">
                <div class="container">
                    <ul>

                        <li><a class="<%=DanhDau("prd") %>" href="Admin.aspx?uc=prd" title="Sản phẩm">Sản phẩm</a></li>
                        <li><a class="<%=DanhDau("cus") %>" href="Admin.aspx?uc=cus" title="Khách hàng">Khách hàng</a></li>
                        <li><a class="<%=DanhDau("sup") %>" href="Admin.aspx?uc=sup" title="Đại lý cung cấp">Đại lý cung cấp</a></li>
                        <li><a class="<%=DanhDau("ord") %>" href="Admin.aspx?uc=ord" title="Đơn đặt hàng">Đơn đặt hàng</a></li>
                        <li><a class="<%=DanhDau("adv") %>" href="Admin.aspx?uc=adv" title="Quảng cáo">Quảng cáo</a></li>
                        <li><a class="<%=DanhDau("new") %>" href="Admin.aspx?uc=new" title="Tin tức">Tin tức</a></li>
                        <li><a class="<%=DanhDau("acc") %>" href="Admin.aspx?uc=acc" title="Tài Khoản">Tài Khoản</a></li>
                        <li><a class="<%=DanhDau("men") %>" href="Admin.aspx?uc=men" title="Menu">Menu</a></li>
                        <%--<li><a class="<%=DanhDau("ord") %>" href="Admin.aspx?uc=ord" title="Đặt hàng">Đặt hàng</a></li>
                        <li><a class="<%=DanhDau("ord") %>" href="Admin.aspx?uc=imp" title="Nhập hàng">Nhập hàng</a></li>--%>
                        <%--<li><a class="<%=DanhDau("rpt") %>" href="Admin.aspx?uc=rpt" title="Thống kê - Báo cáo</">Thống kê - Báo cáo</a></li>--%>
                    </ul>
                </div>
            </div>

            <%--Phần nội dung trang--%>
            <uc1:AdminLoadControl runat="server" ID="AdminLoadControl" />
        </div>
    </form>
</body>
</html>