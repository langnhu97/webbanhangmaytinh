﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DaiLySelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.DaiLyCungCap.DaiLySelect" %>

<div class="head">
    Các đại lý cung cấp đã tạo
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=sup&suc=lst&man=add">Thêm mới danh mục</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId">Mã</th>
            <th class="colName">Tên Đại lý</th>
            <th class="colAdd">Địa chỉ</th>
            <th class="colPhone">SĐT</th>
            <th class="colTool">Công cụ</th>            
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>
         
    </table>
</div>

<script>
    function XoaDanhMuc(id) {
        if (confirm("Bạn có chắc muốn xóa danh mục này?")) {
            $.post("CMS/Admin/DaiLyCungCap/AJAX/DaiLyCungCap.aspx",
                {
                    "man": "del",
                    "id": id
                },
                function (data, status) {
                    if (data == "1") //data = 1: thực hiện thành công
                    {
                        $("#rowId_" + id).slideUp();
                    } else {
                        alert("Xóa thất bại");
                    }
                }
            );
        }
        
    }
</script>