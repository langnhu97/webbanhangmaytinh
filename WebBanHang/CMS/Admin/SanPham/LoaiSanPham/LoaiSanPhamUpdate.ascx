﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LoaiSanPhamUpdate.ascx.cs" Inherits="WebBanHang.CMS.Admin.SanPham.LoaiSanPham.LoaiSanPhamUpdate" %>

<div class="head">
    Chỉnh sửa loại sản phẩm
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">        
        <div class="attr">Tên loại sản phẩm</div>
        <div class="val">
            <asp:TextBox ID="txtTenLoaiSP" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="***" 
                SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenLoaiSP"></asp:RequiredFieldValidator>
        </div>        
    </div>    
    <div class="field">
        <div class="attr">Ghi chú</div>
        <div class="val">
            <asp:TextBox ID="txtGhiChu" class="txt" runat="server"></asp:TextBox>            
        </div>        
    </div>  
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn" ID="btnThemMoi" runat="server" Text="Chỉnh sửa" OnClick="btnThemMoi_Click"/>
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click"/>
        </div>
    </div>
</div>