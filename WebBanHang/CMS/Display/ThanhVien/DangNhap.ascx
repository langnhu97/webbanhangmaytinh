﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DangNhap.ascx.cs" Inherits="WebBanHang.CMS.Display.ThanhVien.DangNhap" %>
<%--<section class="vh-100 bg-image pt-5">
    <div class="mask d-flex align-items-center h-100 gradient-custom-3">
        <div class="container h-100">
            <div class="row d-flex justify-content-center align-items-center h-100">
                <div class="col-12 col-md-9 col-lg-7 col-xl-6">
                    <div class="card" style="border-radius: 15px;">
                        <div class="card-body px-5 py-4">
                            <h2 class="text-uppercase text-center mb-5">ĐĂNG NHẬP</h2>

                            <div>

                                <div class="form-outline mb-4">
                                    <asp:TextBox class="form-control form-control-lg" ID="txtEmail" type="text" runat="server" placeholder="Email"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                        ControlToValidate="txtEmail" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email sai định dạng" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                </div>
                                <div class="form-outline mb-4">
                                    <asp:TextBox class="form-control form-control-lg" TextMode="Password" ID="txtMatKhau" type="text" runat="server" placeholder="Mật khẩu"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                        ControlToValidate="txtMatKhau" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                </div>
                                <div class="d-flex justify-content-center">
                                    <asp:LinkButton class="btn btn-info btn-block btn-lg gradient-custom-4 text-body" ID="btnDangNhap" runat="server" OnClick="btnDangNhap_Click">Đăng nhập</asp:LinkButton>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>--%>
<section class="v-100 h-50 pt-5">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card" style="border-radius: 15px;">
                <div class="card" style="border-radius: 15px;">
                    <div class="card-body px-5 py-4">
                        <h2 class="text-uppercase text-center mb-5">ĐĂNG NHẬP</h2>

                        <div>

                            <div class="form-outline mb-4">
                                <asp:TextBox class="form-control form-control-lg" ID="txtEmail" type="text" runat="server" placeholder="Email"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtEmail" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email sai định dạng" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                            </div>
                            <div class="form-outline mb-4">
                                <asp:TextBox class="form-control form-control-lg" TextMode="Password" ID="txtMatKhau" type="text" runat="server" placeholder="Mật khẩu"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                                    ControlToValidate="txtMatKhau" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            </div>
                            <div class="d-flex justify-content-center">
                                <asp:LinkButton class="btn btn-info btn-block btn-lg gradient-custom-4 text-body" ID="btnDangNhap" runat="server" OnClick="btnDangNhap_Click">Đăng nhập</asp:LinkButton>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
