﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml.Linq;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.TrangChu
{
    public partial class TrangChuLoadControl : System.Web.UI.UserControl
    {
        NhomQuangCaoDAL nhomQuangCaoDAL = new NhomQuangCaoDAL();
        QuangCaoDAL quangCaoDAL = new QuangCaoDAL();
        DanhMucDAL danhMucDAL = new DanhMucDAL();
        SanPhamDAL sanPhamDAL = new SanPhamDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ltrSlide.Text = LaySilde();

                ltrDanhMucSanPham.Text = LayDanhMucSanPham();
            }
        }

        private string LayDanhMucSanPham()
        {
            string s = "";
            DataTable dt = danhMucDAL.DanhMuc_Select();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                s += "<div>";
                s += @"
                    <div class='head-mid mt-5 mb-5' style='color:#FF5E22'>
                        " + dt.Rows[i]["TenDM"] + @"
                    </div>
                ";
                s += "<div class='row pb-3'>";
                s += LaySanPham(Convert.ToInt32(dt.Rows[i]["MaDM"].ToString()), Convert.ToInt32(dt.Rows[i]["SoSPHienThi"].ToString()));
                s += "</div>";
                s += "<div style='clear:both'><div>";
                s += "</div>";
            }
            return s;
        }

        private string LaySanPham(int MaDM, int SoSPHienThi)
        {
            string s = "";
            DataTable dt = sanPhamDAL.SanPham_SelectByDanhMuc(MaDM);
            for (int i = 0; i < SoSPHienThi; i++)
            {
                if (dt.Rows[i]["MaSP"] == null) break;
                string link = "Default.aspx?uc=prd&suc=dtl&id=" + dt.Rows[i]["MaSP"];

                s += @"
                        <figure class='product-item col-md-3 float-left p-3' style='height:225px'>
                            <a href='"+ link + @"' class='w-100 h-75 d-inline-block mb-3'>
                                <img src='IMG/SanPham/" + dt.Rows[i]["HinhAnh"] + @"' class='img-fluid w-100 h-100'>
                            </a>
                            <div class='text-center'>
                                <a href='"+ link + @"' style='color:#000000' class='product-item-name w-100 h-15 text-center font-weight-bold'>
                                   " + dt.Rows[i]["TenSP"] + @"
                                </a>
                            </div>
                            <div class='text-center'  style='color:#FF5E22'>                            
                                <a href='" + link + @"' style='color:#FF5E22'  class='product-item-name w-100 h-10 text-center font-weight-bold'>
                                    " + Convert.ToDouble(dt.Rows[i]["GiaBan"]).ToString("N").Replace(".00","") + " " + @"
                                    VNĐ
                                </a>
                            </div>
                        </figure>
                ";
            }

            return s;
        }

        private string LaySilde()
        {
            string s = "";
            DataTable dt = nhomQuangCaoDAL.NhomQuangCao_SelectByViTri("slide");
            if(dt.Rows.Count > 0)
            {
                s = LayAnhSilde(Convert.ToInt32(dt.Rows[0]["MaNhomQC"].ToString()));
            }
            return s;
        }

        private string LayAnhSilde(int MaNhomQC)
        {
            string s = "";
            DataTable dt = quangCaoDAL.QuangCao_SelectByNhomQC(MaNhomQC);
           

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string isActive = ""; //active ảnh slide đầu tiên trong carousel slide
                if(i == 0)
                {
                    isActive = "active";
                }
                s += @"
                <div class='carousel-item " + isActive + @"'>
                     <img src='./IMG/QuangCao/" + dt.Rows[i]["AnhDaiDien"] + @"' class='img-fluid w-100' alt='" + dt.Rows[i]["TenQC"] + @"'>
                </div>
                ";
            }

            return s;
        }
    }
}