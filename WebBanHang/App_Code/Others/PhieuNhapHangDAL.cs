﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class PhieuNhapHangDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Update một PhieuNhapHang

        public void PhieuNhapHang_Update(int MaPN,
                                            int MaNV,
                                            int MaDL,
                                            DateTime NgayNhap,
                                            float TongTien)
        {
            SqlCommand cmd = new SqlCommand("sp_PhieuNhapHangUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaPN", MaPN);
            cmd.Parameters.AddWithValue("@MaNV", MaNV);
            cmd.Parameters.AddWithValue("@MaDL", MaDL);
            cmd.Parameters.AddWithValue("@NgayNhap", NgayNhap);
            cmd.Parameters.AddWithValue("@TongTien", TongTien);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một PhieuNhapHang

        public void PhieuNhapHang_Insert(int MaNV,
                                            int MaDL,
                                            DateTime NgayNhap,
                                            float TongTien)
        {
            SqlCommand cmd = new SqlCommand("sp_PhieuNhapHangInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNV", MaNV);
            cmd.Parameters.AddWithValue("@MaDL", MaDL);
            cmd.Parameters.AddWithValue("@NgayNhap", NgayNhap);
            cmd.Parameters.AddWithValue("@TongTien", TongTien);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 PhieuNhapHang

        public void PhieuNhapHang_Delete(int MaPN)
        {
            SqlCommand cmd = new SqlCommand("sp_PhieuNhapHangDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaPN", MaPN);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách PhieuNhapHang
        public DataTable PhieuNhapHang_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM PhieuNhapHang";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một PhieuNhapHang theo MaPN
        public DataTable PhieuNhapHang_SelectById(int MaPN)
        {
            SqlCommand cmd = new SqlCommand("sp_PhieuNhapHangSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaPN", MaPN);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}