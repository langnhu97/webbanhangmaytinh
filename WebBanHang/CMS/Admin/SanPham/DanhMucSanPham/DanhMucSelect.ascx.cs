﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.DanhMucSanPham
{
    public partial class DanhMucSelect : System.Web.UI.UserControl
    {
        DanhMucDAL dal = new DanhMucDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhMuc();
            }
        }

        private void LayDanhMuc()
        {
            DataTable dt = new DataTable();
            dt = dal.DanhMuc_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaDM"] + @"'>
                     <td class='colId'>" + dt.Rows[i]["MaDM"] + @"</td>   
                     <td class='colName'>" + dt.Rows[i]["TenDM"] + @"</td>       
                     <td class='colQua'>" + dt.Rows[i]["SoSPHienThi"] + @"</td>       
                     <td class='colNote'>" + dt.Rows[i]["GhiChu"] + @"</td>       
                     <td class='colTool'>
                         <a href = 'Admin.aspx?uc=prd&suc=cat&man=upd&id=" + dt.Rows[i]["MaDM"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaDM"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>                
                </tr>
            ";
            }
        }
    }
}