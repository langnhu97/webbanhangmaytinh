﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class GioHangDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Update một GioHang

        public void GioHang_Update(int MaKH,
                                    int MaSP,
                                    int SoLuong)
        {
            SqlCommand cmd = new SqlCommand("sp_GioHangUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaKH", MaKH);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);
            cmd.Parameters.AddWithValue("@SoLuong", SoLuong);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một GioHang

        public void GioHang_Insert(int MaKH,
                                    int MaSP,
                                    int SoLuong)
        {
            SqlCommand cmd = new SqlCommand("sp_GioHangInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaKH", MaKH);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);
            cmd.Parameters.AddWithValue("@SoLuong", SoLuong);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 GioHang

        public void GioHang_Delete(int MaKH,
                                    int MaSP)
        {
            SqlCommand cmd = new SqlCommand("sp_GioHangDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaKH", MaKH);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách GioHang
        public DataTable GioHang_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM GioHang";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion
    }
}