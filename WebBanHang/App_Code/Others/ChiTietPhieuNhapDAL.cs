﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class ChiTietPhieuNhapDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Update một ChiTietPhieuNhap

        public void ChiTietPhieuNhap_Update(int MaPN,
                                            int MaSP,
                                            DateTime NgayNhap,
                                            int SoLuong,
                                            float GiaNhap,
                                            string GhiChu,
                                            float ThanhTien)
        {
            SqlCommand cmd = new SqlCommand("sp_ChiTietPhieuNhapUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaPN", MaPN);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);
            cmd.Parameters.AddWithValue("@NgayNhap", NgayNhap);
            cmd.Parameters.AddWithValue("@SoLuong", SoLuong);
            cmd.Parameters.AddWithValue("@GiaNhap", GiaNhap);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);
            cmd.Parameters.AddWithValue("@ThanhTien", ThanhTien);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một ChiTietPhieuNhap

        public void ChiTietPhieuNhap_Insert(int MaPN,
                                            int MaSP,
                                            DateTime NgayNhap,
                                            int SoLuong,
                                            float GiaNhap,
                                            string GhiChu,
                                            float ThanhTien)
        {
            SqlCommand cmd = new SqlCommand("sp_ChiTietPhieuNhapInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaPN", MaPN);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);
            cmd.Parameters.AddWithValue("@NgayNhap", NgayNhap);
            cmd.Parameters.AddWithValue("@SoLuong", SoLuong);
            cmd.Parameters.AddWithValue("@GiaNhap", GiaNhap);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);
            cmd.Parameters.AddWithValue("@ThanhTien", ThanhTien);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 ChiTietPhieuNhap

        public void ChiTietPhieuNhap_Delete(int MaPN,
                                            int MaSP)
        {
            SqlCommand cmd = new SqlCommand("sp_ChiTietPhieuNhapDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaPN", MaPN);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách ChiTietPhieuNhap
        public DataTable ChiTietPhieuNhap_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM ChiTietPhieuNhap";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một ChiTietPhieuNhap theo MaHD và MaSP
        public DataTable ChiTietPhieuNhap_SelectById(int MaPN, int MaKH)
        {
            SqlCommand cmd = new SqlCommand("sp_ChiTietPhieuNhapSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaPN", MaPN);
            cmd.Parameters.AddWithValue("@MaKH", MaKH);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}