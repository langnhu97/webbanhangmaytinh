﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;
using System.Net.NetworkInformation;

namespace WebBanHang.App_Code
{
    public class NhaSanXuatDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();
        #region Update một NhaSanXuat

        public void NhaSanXuat_Update(int MaNSX,
                                        string TenNSX,
                                        string GhiChu)
        {
            SqlCommand cmd = new SqlCommand("sp_NhaSanXuatUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNSX", MaNSX);
            cmd.Parameters.AddWithValue("@TenNSX", TenNSX);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một NhaSanXuat

        public void NhaSanXuat_Insert(string TenNSX,
                                        string GhiChu)
        {
            SqlCommand cmd = new SqlCommand("sp_NhaSanXuatInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@TenNSX", TenNSX);
            cmd.Parameters.AddWithValue("@GhiChu", GhiChu);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 NhaSanXuat

        public void NhaSanXuat_Delete(int MaNSX)
        {
            SqlCommand cmd = new SqlCommand("sp_NhaSanXuatDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNSX", MaNSX);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách NhaSanXuat
        public DataTable NhaSanXuat_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM NhaSanXuat";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một NhaSanXuat theo MaNSX
        public DataTable NhaSanXuat_SelectById(int MaNSX)
        {
            SqlCommand cmd = new SqlCommand("sp_NhaSanXuatSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaNSX", MaNSX);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}