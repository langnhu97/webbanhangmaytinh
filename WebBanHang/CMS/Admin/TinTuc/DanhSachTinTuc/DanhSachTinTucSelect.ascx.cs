﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TinTuc
{
    public partial class TinTucSelect : System.Web.UI.UserControl
    {
        DanhMucTinTucDAL danhMucTinTucDAL = new DanhMucTinTucDAL();
        TinTucDAL tinTucDAL = new TinTucDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable dt = tinTucDAL.TinTuc_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string tenDMTT = danhMucTinTucDAL.DanhMucTinTuc_SelectById(Convert.ToInt32(dt.Rows[i]["MaDMTT"])).Rows[0]["TenDMTT"].ToString();
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaTT"] + @"'>
                    <td class='colId'>" + dt.Rows[i]["MaTT"] + @"</td>
                    <td class='colGroup'>" + tenDMTT + @"</td>   
                    <td class='colName'>" + dt.Rows[i]["TieuDe"] + @"</td>     
                    <td class='colQua'>" + dt.Rows[i]["LuotXem"] + @"</td>     
                    <td class='colDate'>" + dt.Rows[i]["NgayDang"] + @"</td>    
                        
                    <td class='colImg'>
                        <img class='img-display' src='/IMG/TinTuc/" + dt.Rows[i]["AnhDaiDien"] + @"'/>
                        <img class='img-hover' src = '/IMG/TinTuc/" + dt.Rows[i]["AnhDaiDien"] + @"'/>
                    </td>             
                    <td class='colTool'>
                        <a href='Admin.aspx?uc=new&suc=lst&man=upd&id=" + dt.Rows[i]["MaTT"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'/>
                        <a href='javascript:XoaDanhMuc(" + dt.Rows[i]["MaTT"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'/>
                </tr>
                ";
            }
        }

       
    }
}