﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.DonDatHang
{
    public partial class DonDatHangLoadControl : System.Web.UI.UserControl
    {
        DonDatHangDAL donDatHangDAL = new DonDatHangDAL();
        public string suc { get; set; }
        public string man { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["suc"] != null)
            {
                suc = Request.QueryString["suc"];
            }
            if (Request.QueryString["man"] != null)
            {
                man = Request.QueryString["man"];
            }

            switch (man)
            {
                case "add":
                    plControl.Controls.Add(LoadControl("DonDatHangInsert.ascx"));
                    break;
                case "upd":
                    plControl.Controls.Add(LoadControl("DonDatHangUpdate.ascx"));
                    break;
                case "del":
                    plControl.Controls.Add(LoadControl("DonDatHangDelete.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("DonDatHangSelect.ascx"));
                    break;
            }
        }

        protected string DanhDauThaoTac(string ucName, string sucName, string manName)
        {
            string s = "";

            //Lấy giá trị queryString trong modul
            string uc = String.Empty;
            string suc = String.Empty;
            string man = String.Empty;
            if (Request.QueryString["uc"] != null)
            {
                uc = Request.QueryString["uc"];
            }
            if (Request.QueryString["suc"] != null)
            {
                suc = Request.QueryString["suc"];
            }
            if (Request.QueryString["man"] != null)
            {
                man = Request.QueryString["man"];
            }

            //So sánh queryString bằng tên uc + suc + man truyền vào thì trả về current --> Đánh dấu là menu hiện tại
            if (uc == ucName && suc == sucName && man == manName)
                s = "current";

            return s;
        }
    }
}