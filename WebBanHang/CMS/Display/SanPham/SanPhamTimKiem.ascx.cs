﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.SanPham
{
    public partial class SanPhamTimKiem : System.Web.UI.UserControl
    {
        SanPhamDAL sanPhamDAL = new SanPhamDAL();
        public string key { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["key"] != null)
            {
                key = Request.QueryString["key"];
            }
            if (!IsPostBack)
            {
                ltrDanhMucSanPham.Text = LayDanhSachSanPhamTimKiem(key);
            }
        }
        
        private string LayDanhSachSanPhamTimKiem(string TuKhoa)
        {
            string s = "";
            DataTable dt = sanPhamDAL.SanPham_SelectByName(TuKhoa);

            s += @"<div class='head-mid'>Có " + dt.Rows.Count + @" sản phẩm tìm được với từ khóa:<span style='color:#000'> " + key + @"</span></div>";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["MaSP"] == null) break;
                string link = "Default.aspx?uc=prd&suc=dtl&id=" + dt.Rows[i]["MaSP"];

                s += @"
                        <figure class='product-item col-md-3 float-left p-3' style='height:225px'>
                            <a href='" + link + @"' class='w-100 h-75 d-inline-block mb-3'>
                                <img src='IMG/SanPham/" + dt.Rows[i]["HinhAnh"] + @"' class='img-fluid w-100 h-100'>
                            </a>
                            <div class='text-center'>
                                <a href='" + link + @"' style='color:#000000' class='product-item-name w-100 h-15 text-center font-weight-bold'>
                                   " + dt.Rows[i]["TenSP"] + @"
                                </a>
                            </div>
                            <div class='text-center'>                            
                                <a href='" + link + @"' style='color:#FF5E22'  class='product-item-name w-100 h-10 text-center font-weight-bold'>
                                    " + Convert.ToDouble(dt.Rows[i]["GiaBan"]).ToString("N").Replace(".00", "") + " " + @"
                                    VNĐ
                                </a>
                            </div>
                        </figure>
                ";
            }

            return s;
        }
    }
}