﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DaiLyUpdate.ascx.cs" Inherits="WebBanHang.CMS.Admin.DaiLyCungCap.DaiLyUpdate" %>
<div class="head">
    Chỉnh sửa Đại lý cung cấp
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">        
        <div class="attr">Tên đại lý</div>
        <div class="val">
            <asp:TextBox ID="txtTenDL" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="***" SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenDL"></asp:RequiredFieldValidator>
        </div>        
    </div>     
   <div class="field">        
        <div class="attr">Địa chỉ</div>
        <div class="val">
            <asp:TextBox ID="txtDiaChi" class="txt" runat="server"></asp:TextBox>
        </div>        
    </div>
    <div class="field">        
        <div class="attr">Số điện thoại</div>
        <div class="val">
            <asp:TextBox ID="txtSDT" class="txt" runat="server"></asp:TextBox>
        </div>        
    </div>     
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn btnThemMoi" ID="btnChinhSua" runat="server" Text="Chỉnh sửa" OnClick="btnChinhSua_Click"/>
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click"/>
        </div>
    </div>
</div>