﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaiKhoanInsert.ascx.cs" Inherits="WebBanHang.CMS.Admin.TaiKhoan.TaiKhoanInsert" %>
<div class="head">
    Thêm mới tài khoản
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">        
        <div class="attr">Tên đăng nhập</div>
        <div class="val">
            <asp:TextBox ID="txtTenDangNhap" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"  Display="Dynamic" ErrorMessage="***" SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenDangNhap"></asp:RequiredFieldValidator>
        </div>        
    </div>
   <div class="field">        
        <div class="attr">Mật khẩu</div>
        <div class="val">
            <asp:TextBox ID="txtMatKhau" TextMode="Password" CssClass="w250" runat="server"></asp:TextBox>
        </div>        
    </div>  
    <div class="field">        
        <div class="attr">Email đăng ký</div>
        <div class="val">
            <asp:TextBox ID="txtEmailDK" class="txt" runat="server"></asp:TextBox>
        </div>        
    </div>
     <div class="field">
        <div class="attr">Địa chỉ đăng ký</div>
        <div class="val"><asp:TextBox ID="txtDiaChiDK" runat="server"></asp:TextBox></div>
    </div>
    <div class="field">
        <div class="attr">Họ tên</div>
        <div class="val"><asp:TextBox ID="txtHoTen" runat="server"></asp:TextBox></div>
    </div>
    <div class="field">        
        <div class="attr">Ngày sinh</div>
        <div class="val">
            <asp:TextBox ID="txtNgaySinh" CssClass="w250" TextMode="Date" class="txt" runat="server"></asp:TextBox>
        </div>        
    </div>
    <div class="field">        
        <div class="attr">Quyền</div>
        <div class="val">
            <asp:DropDownList ID="ddlQuyen" CssClass="w250" runat="server"></asp:DropDownList>
        </div>        
    </div>
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:CheckBox ID="ckTiepTuc" Text="Tiếp tục thêm mới sau khi tạo sản phẩm này" runat="server" />
        </div>
    </div>
     <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn btnThemMoi" ID="btnThemMoi" runat="server" Text="Thêm mới" OnClick="btnThemMoi_Click"/>
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click"/>
        </div>
    </div>
</div>