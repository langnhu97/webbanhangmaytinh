﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DangKy.ascx.cs" Inherits="WebBanHang.CMS.Display.ThanhVien.DangKy" %>
<section class="v-100 h-50 pt-5">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="card-body px-5 py-4">
                <h2 class="text-uppercase text-center mb-5">TẠO TÀI KHOẢN</h2>

                <div>

                    <div class="form-outline mb-4">
                        <asp:TextBox class="form-control form-control-lg" ID="txtTenKH" type="text" runat="server" placeholder="Họ tên"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"
                            ControlToValidate="txtTenKH" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>

                    <div class="form-outline mb-4">
                        <asp:TextBox class="form-control form-control-lg" ID="txtDiaChi" type="text" runat="server" placeholder="Địa chỉ"></asp:TextBox>
                    </div>

                    <div class="form-outline mb-4">
                        <asp:TextBox class="form-control form-control-lg" ID="txtSDT" type="text" runat="server" placeholder="Số điện thoại"></asp:TextBox>
                    </div>

                    <div class="form-outline mb-4">
                        <asp:TextBox class="form-control form-control-lg" ID="txtEmail" type="text" runat="server" placeholder="Email"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"
                            ControlToValidate="txtEmail" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail" ErrorMessage="Email sai định dạng" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </div>
                    <div class="form-outline mb-4">
                        <asp:TextBox class="form-control form-control-lg" TextMode="Password" ID="txtMatKhau" type="text" runat="server" placeholder="Mật khẩu"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="*"
                            ControlToValidate="txtMatKhau" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-outline mb-4">
                        <asp:TextBox class="form-control form-control-lg" TextMode="Password" ID="txtXacNhanMatKhau" type="text" runat="server" placeholder="Xác nhận mật khẩu"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="*"
                            ControlToValidate="txtXacNhanMatKhau" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="CompareValidator1" ControlToValidate="txtXacNhanMatKhau" ControlToCompare="txtMatKhau" runat="server" ErrorMessage="Xác nhận mật khẩu không trùng khớp"></asp:CompareValidator>
                    </div>
                    <div class="d-flex justify-content-center">
                        <asp:LinkButton class="btn btn-info btn-block btn-lg gradient-custom-4 text-body" ID="btnDangKy" runat="server" OnClick="btnDangKy_Click">Đăng ký</asp:LinkButton>
                    </div>
                </div>

            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
</section>
