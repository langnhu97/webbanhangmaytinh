﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Display.TinTuc
{
    public partial class TinTucLoadControl : System.Web.UI.UserControl
    {
        string queryString = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["suc"] != null)
            {
                queryString = Request.QueryString["suc"];
            }

            switch (queryString)
            {
                case "lst":
                    plControl.Controls.Add(LoadControl("DanhSachTinTuc.ascx"));
                    break;
                case "dtl":
                    plControl.Controls.Add(LoadControl("ChiTietTinTuc.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("TrangChuTinTuc.ascx"));
                    break;
            }
        }
    }
}