﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DanhMucTinTucUpdate.ascx.cs" Inherits="WebBanHang.CMS.Admin.TinTuc.DanhMucTinTuc.DanhMucTinTucUpdate" %>
<div class="head">
    Chỉnh sửa danh mục tin tức
</div>
<div class="inputForm">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div>
        <asp:Label ID="lblThongBao" runat="server" Text=""></asp:Label>
    </div>
    <div class="field">
        <div class="attr">Tên danh mục</div>
        <div class="val">
            <asp:TextBox ID="txtTenDMTT" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="***"
                SetFocusOnError="true" ForeColor="red" ControlToValidate="txtTenDMTT"></asp:RequiredFieldValidator>
        </div>
    </div>
    <div class="field">
        <div class="attr">Thứ tự</div>
        <div class="val">
            <asp:TextBox ID="txtThuTu" class="txt" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic" ErrorMessage="***"
                SetFocusOnError="true" ForeColor="red" ControlToValidate="txtThuTu"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtThuTu"
                ValidationExpression="(\d)*" runat="server" ErrorMessage="Chỉ nhập số"
                SetFocusOnError="true" ForeColor="Red" Display="Dynamic"></asp:RegularExpressionValidator>
        </div>
    </div>    
    <div class="field">
        <div class="attr">
            &nbsp
        </div>
        <div class="val">
            <asp:Button CssClass="btn btnThemMoi" ID="btnChinhSua" runat="server" Text="Chỉnh sửa" OnClick="btnThemMoi_Click"/>
            <asp:Button CssClass="btn btnHuy" ID="btnHuy" runat="server" Text="Hủy" CausesValidation="false" OnClick="btnHuy_Click" />
        </div>
    </div>
</div>
