﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.QuangCao.NhomQuangCao
{
    public partial class NhomQuangCaoUpdate : System.Web.UI.UserControl
    {
        NhomQuangCaoDAL dal = new NhomQuangCaoDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }

            HienThiThongTin(id);
        }

        private void HienThiThongTin(int id)
        {
            DataTable dt = dal.NhomQuangCao_SelectById(id);
            if(dt.Rows.Count > 0)
            {
                txtTenNhomQC.Text = dt.Rows[0]["TenNhomQC"].ToString();
                txtViTri.Text = dt.Rows[0]["ViTri"].ToString();
                txtThuTu.Text = dt.Rows[0]["ThuTu"].ToString();
            }
        }

        protected void btnChinhSua_Click(object sender, EventArgs e)
        {
            try
            {
                dal.NhomQuangCao_Update(id, txtTenNhomQC.Text.Trim(), txtViTri.Text.Trim(), Convert.ToInt32(txtThuTu.Text.Trim()));
                Response.Redirect("Admin.aspx?uc=adv&suc=grp");
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa sửa được danh mục</div>";
            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenNhomQC.Text = "";
            txtViTri.Text = "";
            txtThuTu.Text = "";
            txtTenNhomQC.Focus();
        }
    }
}