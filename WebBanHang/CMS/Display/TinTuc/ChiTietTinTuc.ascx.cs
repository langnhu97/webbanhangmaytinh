﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.TinTuc
{
    public partial class ChiTietTinTuc : System.Web.UI.UserControl
    {
        TinTucDAL tinTucDAL = new TinTucDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }

            if (!IsPostBack)
            {
                ltrTinTuc.Text =  LayNoiDungTinTuc(id);
            }
        }

        private string LayNoiDungTinTuc(int id)
        {
            string s = "";
            CapNhatLuotXemTin(id);
            DataTable dt = tinTucDAL.TinTuc_SelectById(id);
            if(dt.Rows.Count > 0)
            {
                s += @"
                    <div class='news-detail-title row py-3'>
                        <h1>" + dt.Rows[0]["TieuDe"] + @"</h1>
                    </div>
                    <div class='news-detail-view row py-2 my-3'>
                        <div>Lượt xem <i class='fa-solid fa-eye px-2'></i>"+ dt.Rows[0]["LuotXem"] + @"</div>
                        <div class='ml-5'>Ngày cập nhật: <i class='fa-regular fa-calendar-days px-2'></i>"+ Convert.ToDateTime(dt.Rows[0]["NgayDang"]).ToString("dd/MM/yyyy") + @"</div>
                    </div>
                    <div class='news-detail-des row'>
                        <h4>" + dt.Rows[0]["MoTa"] + @"</h4>
                    </div>
                    <div class='news-detail-content row'>
                        " + dt.Rows[0]["ChiTiet"] + @"
                    </div>
                ";
            }

            return s;
        }

        private void CapNhatLuotXemTin(int id)
        {
            tinTucDAL.TinTuc_UpdateLuotXem(id);
        }
    }
}