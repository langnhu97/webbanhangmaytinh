﻿using System.Data.SqlClient;
using System.Data;

namespace WebBanHang.App_Code
{
    public class ChiTietDonHangDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();
        #region Update một ChiTietDonHang

        public void ChiTietDonHang_Update(int MaHD,
                                          int MaSP,
                                          int SoLuong,
                                          float DonGia)
        {
            SqlCommand cmd = new SqlCommand("sp_ChiTietDonHangUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaHD", MaHD);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);
            cmd.Parameters.AddWithValue("@SoLuong", SoLuong);
            cmd.Parameters.AddWithValue("@DonGia", DonGia);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thêm một ChiTietDonHang

        public void ChiTietDonHang_Insert(int MaHD,
                                                int MaSP,
                                                int SoLuong,
                                                float DonGia)
        {
            SqlCommand cmd = new SqlCommand("sp_ChiTietDonHangInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaHD", MaHD);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);
            cmd.Parameters.AddWithValue("@SoLuong", SoLuong);
            cmd.Parameters.AddWithValue("@DonGia", DonGia);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion        

        #region Thuc hien xoa 1 ChiTietDonHang

        public void ChiTietDonHang_Delete(int MaHD,
                                            int MaSP)
        {
            SqlCommand cmd = new SqlCommand("sp_ChiTietDonHangDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaHD", MaHD);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thuc hien xoa 1 ChiTietDonHang

        public void ChiTietDonHang_DeleteByMaHD(int MaHD)
        {
            SqlCommand cmd = new SqlCommand("sp_ChiTietDonHangDeleteByMaHD");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaHD", MaHD);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách ChiTietDonHang
        public DataTable ChiTietDonHang_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM ChiTietDonHang";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị một ChiTietDonHang theo MaHD và MaSP
        public DataTable ChiTietDonHang_SelectById(int MaHD, int MaSP)
        {
            SqlCommand cmd = new SqlCommand("sp_ChiTietDonHangSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaHD", MaHD);
            cmd.Parameters.AddWithValue("@MaSP", MaSP);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion
    }
}