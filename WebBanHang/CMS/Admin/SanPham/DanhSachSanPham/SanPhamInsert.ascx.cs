﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.SanPham.DanhSachSanPham
{
    public partial class SanPhamInsert : System.Web.UI.UserControl
    {
        SanPhamDAL _sanphamDAL = new SanPhamDAL();
        NhaSanXuatDAL _nhaSanXuatDAL = new NhaSanXuatDAL();
        LoaiSanPhamDAL _loaiSanPhamDAL = new LoaiSanPhamDAL();
        KhuyenMaiDAL _khuyenMaiDAL = new KhuyenMaiDAL();
        DanhMucDAL _danhMucDAL = new DanhMucDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack){
                LayDanhSachNSX();
                LayDanhSachLoaiSP();
                LayDanhSachKhuyenMai();
                LayDanhMuc();
            }
        }

        private void LayDanhMuc()
        {
            DataTable dt = new DataTable();
            dt = _danhMucDAL.DanhMuc_Select();

            ddlDM.Items.Clear();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlDM.Items.Add(new ListItem(dt.Rows[i]["TenDM"].ToString(), dt.Rows[i]["MaDM"].ToString()));
            }
        }

        private void LayDanhSachKhuyenMai()
        {
            DataTable dt = new DataTable();
            dt = _khuyenMaiDAL.KhuyenMai_Select();

            ddlKhuyenMai.Items.Clear();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlKhuyenMai.Items.Add(new ListItem(dt.Rows[i]["TenKM"].ToString(), dt.Rows[i]["MaKM"].ToString()));
            }
        }

        private void LayDanhSachNSX()
        {
            DataTable dt = new DataTable();
            dt = _nhaSanXuatDAL.NhaSanXuat_Select();

            ddlNSX.Items.Clear();
            
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlNSX.Items.Add(new ListItem(dt.Rows[i]["TenNSX"].ToString(), dt.Rows[i]["MaNSX"].ToString()));
            }

        }

        private void LayDanhSachLoaiSP()
        {
            DataTable dt = new DataTable();
            dt = _loaiSanPhamDAL.LoaiSanPham_Select();

            ddlLoaiSP.Items.Clear();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ddlLoaiSP.Items.Add(new ListItem(dt.Rows[i]["TenLoaiSP"].ToString(), dt.Rows[i]["MaLoaiSP"].ToString()));
            }
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                if (flHinhAnh.FileContent.Length > 0)
                {
                    if (flHinhAnh.FileName.EndsWith(".jpg") || flHinhAnh.FileName.EndsWith(".png")
                        || flHinhAnh.FileName.EndsWith(".gif") || flHinhAnh.FileName.EndsWith(".jpeg")
                        || flHinhAnh.FileName.EndsWith(".webp") || flHinhAnh.FileName.EndsWith(".tiff")
                        || flHinhAnh.FileName.EndsWith(".psd") || flHinhAnh.FileName.EndsWith(".pdf"))
                    {
                        flHinhAnh.SaveAs(Server.MapPath("IMG/SanPham/") + flHinhAnh.FileName);
                    }
                }

                string tensp = txtTenSP.Text.Trim();
                int loaisp = Convert.ToInt32(ddlLoaiSP.SelectedValue) > 0 ? Convert.ToInt32(ddlLoaiSP.SelectedValue) : 0;
                int mansx = Convert.ToInt32(ddlNSX.SelectedValue) > 0 ? Convert.ToInt32(ddlNSX.SelectedValue) : 0;
                int madm = Convert.ToInt32(ddlDM.SelectedValue) > 0 ? Convert.ToInt32(ddlDM.SelectedValue) : 0;
                int makm = Convert.ToInt32(ddlKhuyenMai.SelectedValue) > 0 ? Convert.ToInt32(ddlKhuyenMai.SelectedValue) : 0;
                string baohanh = txtBaoHanh.Text.Trim();
                int soluong = txtSoLuong.Text.Trim() != "" ? Convert.ToInt32(txtSoLuong.Text.Trim()) : 0;
                string thongso = txtThongSo.Text;
                float giaban = txtGiaBan.Text.Trim() !=  "" ? Convert.ToSingle(txtGiaBan.Text.Replace(".","")) : 0;
                string fileAnh = flHinhAnh.FileName;
                string ghichu = txtGhiChu.Text.Trim();

                _sanphamDAL.SanPham_Insert(tensp, loaisp, mansx, madm,  makm, baohanh,
                        soluong, thongso,     giaban, fileAnh, ghichu);
                lblThongBao.Text = "<div class='successNoti'>Đã tạo sản phẩm " + txtTenSP.Text + "</div>";
                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=prd&suc=lst");
                }
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa thêm được sản phẩm</div>";
            }
        }

        private void ResetForm()
        {
            txtTenSP.Text = String.Empty;
            txtBaoHanh.Text = String.Empty;
            txtGhiChu.Text = String.Empty;
            txtGiaBan.Text = String.Empty;
            txtSoLuong.Text = String.Empty;
            txtThongSo.Text = String.Empty;
            ddlKhuyenMai.SelectedIndex = -1;
            ddlLoaiSP.SelectedIndex = -1;
            ddlNSX.SelectedIndex = -1;
            ddlDM.SelectedIndex = -1;
            flHinhAnh.FileContent.Flush();
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}