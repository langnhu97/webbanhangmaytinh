﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChiTietSanPham.ascx.cs" Inherits="WebBanHang.CMS.Display.SanPham.ChiTietSanPham" %>
<div class="product-detail row mt-3">
        <div class="col-md-5 text-center align-middle">
            <asp:Literal ID="ltrAnh" runat="server"></asp:Literal>
        </div>
        <div class="col-md-7">
            <div class="product-detail-name">
                <asp:Literal ID="ltrTen" runat="server"></asp:Literal>
            </div>
            <div class="product-detail-cost">
                <asp:Literal ID="ltrGia" runat="server"></asp:Literal>
            </div>
            <hr />
            <div class="product-detail-sup row">
                <div class="product-detail-left col-md-4">Nhà sản xuất</div>
                <div class="product-detail-right col-md-8">
                    <asp:Literal ID="ltrNSX" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="product-detail-type row">
                <div class="product-detail-left col-md-4">Nhóm sản phẩm</div>
                <div class="product-detail-right col-md-8">
                    <asp:Literal ID="ltrLoaiSP" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="product-detail-param row">
                <div class="product-detail-left col-md-4">Thông số</div>
                <div class="product-detail-right col-md-8">
                    <asp:Literal ID="ltrThongSo" runat="server"></asp:Literal>
                </div>
            </div>
            <div class="product-detail-guar row">
                <div class="product-detail-param-left col-md-4">Bảo hành</div>
                <div class="product-detail-param-right col-md-8">
                    <asp:Literal ID="ltrBaoHanh" runat="server"></asp:Literal>
                </div>
            </div>
             <div class="product-detail-guar row">
                <div class="product-detail-param-left col-md-4">Số lượng</div>
                <div class="product-detail-param-right col-md-8">
                    <input id="quantity" type="number" name="quantity" value="1" class="w-25"/>
                </div>
            </div>
            <hr />
            <div class="row">
                <div class="col-md-6 d-flex float-left text-center">
                    <asp:LinkButton class="btn btn-info btn-block btn-lg gradient-custom-4 text-body" ID="btnGioHang" runat="server" href="javascript:VaoGioHang();">
                        <i class="fa-solid fa-cart-shopping mr-2"></i>Thêm vào giỏ
                    </asp:LinkButton>
                </div>
                <div class="col-md-6 text-center">
                    <asp:LinkButton class="btn btn-info btn-block btn-lg gradient-custom-4 text-body" ID="btnMuaNgay" runat="server" href="javascript:MuaNgay();">
                        <i class="fa-brands fa-shopify mr-3"></i>Mua ngay
                    </asp:LinkButton>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    <div class="row mx-5 my-5">
        <asp:Literal ID="ltrMoTa" runat="server"></asp:Literal>
    </div>
</div>
<script type="text/javascript">
    function VaoGioHang() {
        var id = "<%=id%>";
        var soluong = $("#quantity").val();        

        $.post("/CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
            {
                "ThaoTac": "XuLyGioHang",
                "id": id,
                "soluong": soluong
            },
            function (data, status) {
                if (data == "") //Thực hiện thêm sản phầm vào giỏ thành công 
                {
                    alert("Sản phẩm đã được thêm vào giỏ");
                    LayTongSoLuongSP();

                } else {
                    alert(data);
                }
            }
        );
    };

    function LayTongSoLuongSP() {
        $.post("CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
            {
                "ThaoTac": "LayTongSoLuongSP",
            },
            function (data, status) {
                if (data != null) {
                    $("#SLSanPhamGioHang").html(data);
                }
            }
        );
    }


    function MuaNgay() {
        var id = "<%=id%>";
        var soluong = $("#quantity").val();

        $.post("CMS/Display/SanPham/AJAX/XuLyGioHang.aspx",
            {
                "ThaoTac": "XuLyGioHang",
                "id": id,
                "soluong": soluong
            },
            function (data, status) {
                if (data == "") //Thực hiện thêm sản phầm vào giỏ thành công 
                {
                    alert("Sản phẩm đã được thêm vào giỏ");
                   location.href = 'Default.aspx?uc=prd&suc=car'
                } else {
                    alert(data);
                }
            }
        );
    }
</script>