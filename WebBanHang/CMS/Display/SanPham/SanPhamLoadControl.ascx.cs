﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebBanHang.CMS.Display.SanPham
{
    public partial class SanPhamLoadControl : System.Web.UI.UserControl
    {
        string queryString = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["suc"] != null)
            {
                queryString = Request.QueryString["suc"];
            }

            switch (queryString)
            {
                case "lst":
                    plControl.Controls.Add(LoadControl("DanhSachSanPham.ascx"));
                    break;
                case "dtl":
                    plControl.Controls.Add(LoadControl("ChiTietSanPham.ascx"));
                    break;
                case "search":
                    plControl.Controls.Add(LoadControl("SanPhamTimKiem.ascx"));
                    break;
                case "car":
                    plControl.Controls.Add(LoadControl("GioHang.ascx"));
                    break;
                default:
                    plControl.Controls.Add(LoadControl("TrangChuSanPham.ascx"));
                    break;
            }
        }
    }
}