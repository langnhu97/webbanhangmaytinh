﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.DonDatHang
{
    public partial class DonDatHangSelect : System.Web.UI.UserControl
    {
        DonDatHangDAL dondathangDAL = new DonDatHangDAL();
        KhachHangDAL khachhangDAL = new KhachHangDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LayDanhSach();
            }
        }

        private void LayDanhSach()
        {
            DataTable dt = new DataTable();
            dt = dondathangDAL.DonDatHang_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ltrDanhMuc.Text += @"
                <tr id='rowId_" + dt.Rows[i]["MaHD"] + @"'>
                     <td class='colId'>" + dt.Rows[i]["MaHD"] + @"</td>   
                     <td class='colDate'>" + dt.Rows[i]["NgayDatHang"] + @"</td>       
                     <td class='colState'>" + dt.Rows[i]["TinhTrang"] + @"</td>       
                     <td class='colName'>" + dt.Rows[i]["TenKH"] + @"</td>       
                     <td class='colPhone'>" + dt.Rows[i]["sdtKH"] + @"</td>       
                     <td class='colEmail'>" + dt.Rows[i]["emailKH"] + @"</td>       
                     <td class='colMoney'>" + dt.Rows[i]["TongTien"] + @"</td>       
                     <td class='colTool'>
                         <a href = 'Admin.aspx?uc=ord&suc=lst&man=upd&id=" + dt.Rows[i]["MaHD"] + @"' class='btnLoadEdit' title='Click để chỉnh sửa danh mục'></a>
                         <a href = 'javascript:XoaDanhMuc(" + dt.Rows[i]["MaHD"] + @")' class='btnLoadDelete' title='Click để xóa danh mục'></a>
                     </td>                
                </tr>
            ";
            }
        }
    }
}