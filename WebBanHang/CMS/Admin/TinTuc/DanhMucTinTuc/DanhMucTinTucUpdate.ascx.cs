﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.TinTuc.DanhMucTinTuc
{
    public partial class DanhMucTinTucUpdate : System.Web.UI.UserControl
    {
        DanhMucTinTucDAL dal = new DanhMucTinTucDAL();
        public int id { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["id"] != null)
            {
                id = Convert.ToInt32(Request.QueryString["id"]);
            }
            HienThiThongTin();
        }

        private void HienThiThongTin()
        {
            DataTable dt = dal.DanhMucTinTuc_SelectById(id);
            if(dt.Rows.Count > 0)
            {
                txtTenDMTT.Text = dt.Rows[0]["TenDMTT"].ToString();
                txtThuTu.Text = dt.Rows[0]["ThuTu"].ToString();
            }
        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                dal.DanhMucTinTuc_Update(id, txtTenDMTT.Text.Trim(), Convert.ToInt32(txtThuTu.Text.Trim()));
                Response.Redirect("/Admin.aspx?uc=new&suc=grp");
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa sửa được danh mục</div>";

            }
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }

        private void ResetForm()
        {
            txtTenDMTT.Text = "";
            txtThuTu.Text = "";
        }
    }
}