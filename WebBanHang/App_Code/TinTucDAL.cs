﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using System.Linq;
using System.Web;

namespace WebBanHang.App_Code
{
    public class TinTucDAL
    {
        SqlDatabase _sqldb = new SqlDatabase();

        #region Thuc hien them mot TinTuc

        public void TinTuc_Insert(  int MaDMTT,
                                    string TieuDe,
                                    string MoTa,
                                    int LuotXem,
                                    DateTime NgayDang,
                                    string AnhDaiDien,
                                    string ChiTiet)
        {
            SqlCommand cmd = new SqlCommand("sp_TinTucInsert");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDMTT", MaDMTT);
            cmd.Parameters.AddWithValue("@TieuDe", TieuDe);
            cmd.Parameters.AddWithValue("@MoTa", MoTa);
            cmd.Parameters.AddWithValue("@LuotXem", LuotXem);
            cmd.Parameters.AddWithValue("@NgayDang", NgayDang);
            cmd.Parameters.AddWithValue("@AnhDaiDien", AnhDaiDien);
            cmd.Parameters.AddWithValue("@ChiTiet", ChiTiet);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thuc hien chinh sua mot TinTuc

        public void TinTuc_Update(int MaTT,
                                    int MaDMTT,
                                    string TieuDe,
                                    string MoTa,
                                    int LuotXem,
                                    DateTime NgayDang,
                                    string AnhDaiDien,
                                    string ChiTiet)
        {
            SqlCommand cmd = new SqlCommand("sp_TinTucUpdate");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaTT", MaTT);
            cmd.Parameters.AddWithValue("@MaDMTT", MaDMTT);
            cmd.Parameters.AddWithValue("@TieuDe", TieuDe);
            cmd.Parameters.AddWithValue("@MoTa", MoTa);
            cmd.Parameters.AddWithValue("@LuotXem", LuotXem);
            cmd.Parameters.AddWithValue("@NgayDang", NgayDang);
            cmd.Parameters.AddWithValue("@AnhDaiDien", AnhDaiDien);
            cmd.Parameters.AddWithValue("@ChiTiet", ChiTiet);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Thuc hien xoa 1 TinTuc
        public void TinTuc_Delete(int MaTT)
        {
            SqlCommand cmd = new SqlCommand("sp_TinTucDelete");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaTT", MaTT);

            _sqldb.ExecuteCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách TinTuc
        public DataTable TinTuc_Select()
        {
            DataTable dt = new DataTable();
            string cmd = "SELECT * FROM TinTuc";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion

        #region Hiển thị danh sách TinTuc theo Danh Muc 
        public DataTable TinTuc_SelectByDanhMuc(int MaDMTT)
        {
            SqlCommand cmd = new SqlCommand("sp_TinTucSelectByDM");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaDMTT", MaDMTT);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

        #region Hiển thị một TinTuc theo MaTT
        public DataTable TinTuc_SelectById(int MaTT)
        {
            SqlCommand cmd = new SqlCommand("sp_TinTucSelect");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaTT", MaTT);
            return _sqldb.GetDataTableCmd(cmd);
        }
        #endregion

        #region Hiển thị danh sách TinTuc gồm 3 tin đầu theo danh mục
        public DataTable TinTuc_Select3ByDanhMuc(int MaDMTT)
        {
            DataTable dt = new DataTable();
            string cmd = $"SELECT top 3 * FROM TinTuc where MaDMTT = {MaDMTT}";
            dt = _sqldb.GetDataTable(cmd);
            return dt;
        }
        #endregion       

        #region Cập nhật lượt xem tin
        public void TinTuc_UpdateLuotXem(int MaTT)
        {
            SqlCommand cmd = new SqlCommand("sp_TinTuc_UpdateLuotXem");
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.AddWithValue("@MaTT", MaTT);
            _sqldb.ExecuteCmd(cmd);
        }
        #endregion
    }
}