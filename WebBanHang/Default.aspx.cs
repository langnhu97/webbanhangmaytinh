﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.Expressions;
using WebBanHang.App_Code;

namespace WebBanHang
{
    public partial class Default : System.Web.UI.Page
    {
        NhomQuangCaoDAL nhomQuangCaoDAL = new NhomQuangCaoDAL();
        QuangCaoDAL quangCaoDAL = new QuangCaoDAL();
        MenuDAL menuDAL = new MenuDAL();
        DanhMucDAL DanhMucDAL = new DanhMucDAL();
        DanhMucTinTucDAL danhMucTinTucDAL = new DanhMucTinTucDAL();
        public string uc { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Kiểm tra modul là tin tức hay sản phẩm để hiển thị danh mục phù hợp
            if (Request.QueryString["uc"] != null)
            {
                uc = Request.QueryString["uc"];
            }

            if(uc == "new")
            {
                plDanhMucSanPham.Visible = false;
                plDanhMucTinTuc.Visible = true;
            }
            else
            {
                plDanhMucSanPham.Visible = true;
                plDanhMucTinTuc.Visible = false;
            }
            #endregion

            if (!IsPostBack)
            {
                ltrLoGo.Text = LayLogo();
                //ltrBanner.Text = LayBanner();
                ltrMenu.Text = LayMenu();
                ltrDanhMucSanPham.Text = LayDanhMucSanPham();
                ltrDanhMucTinTuc.Text = LayDanhMucTinTuc();
            }

            ///Kiểm tra đăng nhập khách hàng
            if (Session["KhachHangDangNhap"] != null && Session["KhachHangDangNhap"].ToString() == "1")
            {
                plDaDangNhap.Visible = true;
                plChuaDangNhap.Visible = false;
                ltrTenDangNhapKH.Text = Session["TenKH"].ToString();
            }
            else
            {
                plDaDangNhap.Visible = false;
                plChuaDangNhap.Visible = true;
            }
        }

        private string LayDanhMucTinTuc()
        {
            string s = "";
            DataTable dt = danhMucTinTucDAL.DanhMucTinTuc_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                s += @"                
                <li class='product-category-item pt-3 pb-3 mb-0'><a href='/Default.aspx?uc=new&suc=lst&id=" + dt.Rows[i]["MaDMTT"] + @"' title='" + dt.Rows[i]["TenDMTT"] + @"'>" + dt.Rows[i]["TenDMTT"] + @"</a></li>
                ";
            }

            return s;
        }

        private string LayDanhMucSanPham()
        {
            string s = "";
            DataTable dt = DanhMucDAL.DanhMuc_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {                
                s += @"                
                <li class='product-category-item pt-3 pb-3 mb-0'><a href='/Default.aspx?uc=prd&suc=lst&id="+ dt.Rows[i]["MaDM"] + @"' title='" + dt.Rows[i]["TenDM"] + @"'>" + dt.Rows[i]["TenDM"] + @"</a></li>
                ";
            }

            return s;
        }

        private string LayMenu()
        {
            string s = "";
            DataTable dt = menuDAL.Menu_Select();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string lienket = dt.Rows[i]["LienKet"].ToString();
                if(lienket == "")
                {
                    lienket = "#";
                }
                s += @"                
                <li class='list-inline-item align-content-center px-1'><a href='"+ dt.Rows[i]["LienKet"] +@"' title='" + dt.Rows[i]["TenMenu"] +@"'>" + dt.Rows[i]["TenMenu"] +@"</a></li>
                ";
            }

            return s;
        }

        #region Lấy logo
        private string LayLogo()
        {
            string s = "";

            //Code lấy vị trị nhóm quảng cáo
            DataTable dt = nhomQuangCaoDAL.NhomQuangCao_SelectByViTri("logo");

            if (dt.Rows.Count > 0)
            {
                s = LayAnhLogo(Convert.ToInt32(dt.Rows[0]["MaNhomQC"].ToString()));
            }
            return s;
        }

        private string LayAnhLogo(int MaNhomQC)
        {
            string s = "";
            DataTable dt = quangCaoDAL.QuangCao_SelectByNhomQC(MaNhomQC);
            if (dt.Rows.Count > 0)
            {
                string link = dt.Rows[0]["LienKet"].ToString();
                if (link == "") link = "#";
                s += @"
                <a href='" + dt.Rows[0]["LienKet"] + @"' title='" + dt.Rows[0]["TenQC"] + @"'>
                    <img class='img-fluid w-75' src='./IMG/QuangCao/" + dt.Rows[0]["AnhDaiDien"] + @"' alt='" + dt.Rows[0]["TenQC"] + @"'>
                </a>
               ";
            }
            return s;
        }
        #endregion

        #region Lấy Banner
        private string LayBanner()
        {
            string s = "";

            //Code lấy vị trị nhóm quảng cáo
            DataTable dt = nhomQuangCaoDAL.NhomQuangCao_SelectByViTri("banner");

            if (dt.Rows.Count > 0)
            {
                s = LayAnhBanner(Convert.ToInt32(dt.Rows[0]["MaNhomQC"].ToString()));
            }
            return s;
        }

        private string LayAnhBanner(int MaNhomQC)
        {
            string s = "";
            DataTable dt = quangCaoDAL.QuangCao_SelectByNhomQC(MaNhomQC);
            if (dt.Rows.Count > 0)
            {
                string link = dt.Rows[0]["LienKet"].ToString();
                if (link == "") link = "#";
                s += @"
                <a href='" + dt.Rows[0]["LienKet"] + @"' title='" + dt.Rows[0]["TenQC"] + @"'>
                    <img src='./IMG/QuangCao/" + dt.Rows[0]["AnhDaiDien"] + @"' class='img-fluid' style='width:100%; border-radius:10px'>
                </a>
               ";
            }
            return s;
        }
        #endregion

        #region Đếm và hiển thị số sản phẩm lên icon giỏ hàng
        protected string DemSoSPTrongGio()
        {

            DataTable dt = new DataTable();
            if (Session["GioHang"] != null)
            {
                dt = (DataTable)Session["GioHang"];
            }
            int tongSoSP = 0;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    tongSoSP += Convert.ToInt32(dt.Rows[i]["SoLuong"]);
                }
            }

            return tongSoSP.ToString();
        }
        #endregion

        protected void btnDangXuat_Click(object sender, EventArgs e)
        {
            //Xóa session
            Session["KhachHangDangNhap"] = "";
            Session["MaKH"] = "";
            Session["TenKH"] = "";
            Session["DiaChi"] = "";
            Session["SDT"] = "";
            Session["Email"] = "";

            Response.Redirect("/Default.aspx");
        }
    }
}