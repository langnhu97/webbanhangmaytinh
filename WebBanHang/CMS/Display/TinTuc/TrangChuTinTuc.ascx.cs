﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Display.TinTuc
{
    public partial class TrangChuTinTuc : System.Web.UI.UserControl
    {
        TinTucDAL tintucDAL = new TinTucDAL();
        DanhMucTinTucDAL danhMucTinTucDAL = new DanhMucTinTucDAL();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                ltrDanhMucTinTuc.Text = LayDanhMucTinTuc();
            }
        }

        private string LayDanhMucTinTuc()
        {
            string s = "";
            DataTable dt = danhMucTinTucDAL.DanhMucTinTuc_Select();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                s += @"
                    <div class='news-list mx-1 my-3 px-3'>
                    
                    <div class='news-list-head row mb-3'>
                       <a href='Default.aspx?uc=new&suc=lst&id=" + dt.Rows[i]["MaDMTT"] + @"' title='" + dt.Rows[i]["TenDMTT"] + @"'>
                            <i class='fa-solid fa-rss mr-2'></i>" + dt.Rows[i]["TenDMTT"] + @"
                       </a>
                   </div>                                 
                ";

                s += LayDanhSachTin(Convert.ToInt32(dt.Rows[i]["MaDMTT"].ToString()));
                s += "</div>";
            }
            return s;
        }

        //Chỉ hiển thị 4 tin tức đầu trong danh sách
        private string LayDanhSachTin(int MaDMTT)
        {
            string s = "";
            DataTable dt = tintucDAL.TinTuc_Select3ByDanhMuc(MaDMTT);
            s += @"
            <div class='row news-list-first mb-4'>
                <div class='news-list-img col-md-4 px-0' >
                    <a href='Default.aspx?uc=new&suc=dtl&id=" + dt.Rows[0]["MaTT"] +@"' title ='" + dt.Rows[0]["TieuDe"] + @"' class='w-100 h-100 d-inline-block mb-3'>
                        <img src = 'IMG/TinTuc/" + dt.Rows[0]["AnhDaiDien"] + @"' class='img-fluid w-100 h-100'/>
                    </a>
                </div>               
            
                <div class='col-md-8 pl-5'>
                    <div class='news-list-name'>
                        <a href='Default.aspx?uc=new&suc=dtl&id=" + dt.Rows[0]["MaTT"] + @"' title='" + dt.Rows[0]["TieuDe"] + @"'>
                            " + dt.Rows[0]["TieuDe"] + @"
                        </a >
                    </div>
                    <div class='news-list-view'>                       
                        <a href='Default.aspx?uc=new&suc=dtl&id=" + dt.Rows[0]["MaTT"] + @"' title='" + dt.Rows[0]["TieuDe"] + @"'>
                            <span><i class='fa-solid fa-eye px-2'></i></i>" + dt.Rows[0]["LuotXem"] + @"</span>
                            <span><i class='fa-regular fa-calendar-days px-2'></i>(" + Convert.ToDateTime(dt.Rows[0]["NgayDang"]).ToString("dd/MM/yyyy") + @")</span>
                        </a>
                    </div>
                    <div>
                        <a href='Default.aspx?uc=new&suc=dtl&id=" + dt.Rows[0]["MaTT"] + @"' title='" + dt.Rows[0]["TieuDe"] + @"'>
                            " + dt.Rows[0]["MoTa"] + @"
                        </a>
                    </div>
                </div>
            </div>
                ";


            for (int i = 1; i < dt.Rows.Count; i++)
            {
                s += @"
                <div class='row news-list-second py-2'>
                    <a href = 'Default.aspx?uc=new&suc=dtl&id=" + dt.Rows[0]["MaTT"] + @"' title ='" + dt.Rows[0]["TieuDe"] + @"'>
                        <span> +&nbsp" + dt.Rows[i]["TieuDe"] + @"</span>
                        <span>(" + Convert.ToDateTime(dt.Rows[i]["NgayDang"]).ToString("dd/MM/yyyy") + @")</span>
                    </a>
                </div>
                <div class='clearfix'></div>
                ";
            }
            return s;
        }
    }
}