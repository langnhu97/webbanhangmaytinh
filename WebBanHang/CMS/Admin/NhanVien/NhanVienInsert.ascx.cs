﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebBanHang.App_Code;

namespace WebBanHang.CMS.Admin.NhanVien
{
    public partial class NhanVienInsert : System.Web.UI.UserControl
    {
        NhanVienDAL _dalNhanVien = new NhanVienDAL();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnThemMoi_Click(object sender, EventArgs e)
        {
            try
            {
                _dalNhanVien.NhanVien_Insert(txtTenNV.Text.Trim(), Convert.ToDateTime(txtNgaySinh.Text),
                        txtDiaChi.Text.Trim(), txtSDT.Text.Trim(), txtTenDangNhap.Text.Trim(), txtMatKhau.Text.Trim());
                lblThongBao.Text = "<div class='successNoti'>Đã tạo nhân viên " + txtTenNV.Text.Trim() + "</div>";

                if (ckTiepTuc.Checked == true)
                {
                    ResetForm();
                }
                else
                {
                    Response.Redirect("Admin.aspx?uc=sta&suc=lst&man=add");
                }
            }
            catch (Exception)
            {
                lblThongBao.Text = "<div class='successNoti'>Chưa tạo được nhân viên </div>";
            }
        }

        private void ResetForm()
        {
            txtTenNV.Text = String.Empty;
            txtTenDangNhap.Text = String.Empty;
            txtSDT.Text = String.Empty;
            txtNgaySinh.Text = String.Empty;
            txtMatKhau.Text = String.Empty;
            txtDiaChi.Text = String.Empty;
        }

        protected void btnHuy_Click(object sender, EventArgs e)
        {
            ResetForm();
        }
    }
}