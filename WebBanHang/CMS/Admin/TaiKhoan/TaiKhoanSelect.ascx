﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaiKhoanSelect.ascx.cs" Inherits="WebBanHang.CMS.Admin.TaiKhoan.TaiKhoanSelect" %>
<div class="head">
    Các tài khoản đã tạo
    <div class="fr tar" >
        <a class="btnLoadInsert" href="Admin.aspx?uc=acc&suc=lst&man=add">Thêm mới tài khoản</a>
    </div>
    <div class="cb"></div>
</div>

<div class="tbl-hienthi">
    <table class="tbl-danhmuc">
        <tr>
            <th class="colId w150">Tên đăng nhập</th>
            <th class="colEmail">Email  </th>
            <th class="colAdd">Địa chỉ</th>
            <th class="colName">Họ tên</th>
            <th class="colBirthDay">Ngày sinh</th>
            <th class="colRole">Quyền</th>
            <th class="colTool">Công cụ</th>          
        </tr>
        <asp:Literal ID="ltrDanhMuc" runat="server"></asp:Literal>
        <asp:HiddenField ID="hdfTenDN" runat="server" />
    </table>
</div>

<script>
    
</script>