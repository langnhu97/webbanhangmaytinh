﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NhanVienLoadControl.ascx.cs" Inherits="WebBanHang.CMS.Admin.NhanVien.NhanVienLoadControl" %>

<div class="container">
     <div class="colLeft">  
        <div class="head">
            Quản lý
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("sta","lst","") %>" href="Admin.aspx?uc=sta&suc=lst"title="Danh sách Sản Phẩm">Danh sách nhân viên</a></li>
        </ul>
        <div class="head">
            Thêm mới
        </div>
        <ul>
            <li><a class="<%=DanhDauThaoTac("sta","lst","add") %>" href="Admin.aspx?uc=sta&suc=lst&man=add" title="Thêm mới nhân viên">Nhân viên</a></li>
        </ul>
    </div>
</div>
<div class="container">
    <div class="colRight">
        <asp:PlaceHolder ID="plControl" runat="server"></asp:PlaceHolder>
    </div>
</div>